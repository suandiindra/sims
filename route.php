<?php
// session_start(); 
$path = "";

if(isset($_GET['go'])){
    $path = $_GET['go'];
}
if($path == "user"){
    // if($path == "Admin"){
    //     include("pages/user/listuser.php");
    // }else{
        include("pages/user/listuser.php");
    // }
}else if($path == "adduser"){
    include("pages/user/adduser.php");
}else if($path == "listsiswa"){
    if($_SESSION['role'] !== "siswa"){
        include("pages/siswa/listsiswa.php");
    }else if($_SESSION['role'] == "siswa"){
        include("pages/siswa/addsiswa.php");
    }
}else if($path == "formspp"){
    if($_SESSION['role'] !== "siswa"){
        include("pages/SPP/listbayarspp.php");
    }else{
        include("pages/SPP/formspp.php");
    }
}else if($path == "addsiswa"){
    include("pages/siswa/addsiswa.php");
}else if($path == "listbayarspp"){
    include("pages/SPP/listbayarspp.php");
}else if($path == "inputspp"){
    include("pages/SPP/formsppmanual.php");
}elseif($path == "daftarulang"){
    if($_SESSION['role'] == "siswa"){
        include("pages/daftar_ulang/formbayardaftarulang.php");
    }else{
        include("pages/daftar_ulang/listdaftarulang.php");
    }
}elseif($path == "add_daftarulang"){
    // if($_SESSION['role'] == "siswa"){
    //     include("pages/daftar_ulang/formbayardaftarulang.php");
    // }else{
        include("pages/daftar_ulang/formbayardaftarulangmanual.php");
    // }
}else if($path == "listdaftarulang"){
    include("pages/daftar_ulang/listdaftarulang.php");
}else if($path == "listppdb"){
    if($_SESSION['role'] == "siswa"){
        include("pages/ppdb/formppdb.php");
    }else{
        include("pages/ppdb/listppdb.php");
    }
}else if($path == "ly"){
    include("pages/pembayaran_LY/data.php");
}else if($path == "inputly"){
    include("pages/pembayaran_LY/form.php");
}else if($path == "formppdb"){
    include("pages/ppdb/formppdbmanual.php");
}else if($path == "ppdb"){
    include("pages/ppdb/listppdb.php");
}else if($path == "editsiswa"){
    include("pages/siswa/formeditsiswa.php");
}else if($path == "kehadiran"){
    include("pages/kehadiran/kehadiran.php");
}else if($path == "generate"){
    include("pages/kehadiran/generate.php");
}else if($path == "generate1"){
    include("pages/generator/generator.php");
}else if($path == "rekapitulasi"){
    include("pages/laporan_tu/laporan.php");
}else if($path == "absensi"){
    include("pages/kehadiran/kehadiran.php");
}else if($path == "tunggakan"){
    include("pages/dashboard/tunggakan.php");
}else{
    if($_SESSION['role'] == "siswa"){
        include("pages/kehadiran/kehadiran.php");
    }else{
        include("pages/dashboard/dashboard.php");
    }
}
?>