<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <div class="row" style="margin-top:-35px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi PPDB Siswa
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                        <table class="col-md-12" style="margin-top:10px; padding:20px">
                            <tr >
                              <input type="hidden" name="tp" value="trf-spp">
                                <td>
                                    <div class="mb-3">
                                        <label for="inputPassword5" require class="form-label">Nama Siswa</label>
                                        <br>
                                        <select name="nama" style="width:100%" id="nama" class="kodebrg" onchange="carisiswa()">
                                            <option value=""></option>
                                        <?php
                                            $del = "select * from m_siswa where isactive = 1";
                                            $res = mysqli_query($con,$del);
                                            while($rs = mysqli_fetch_array($res))
                                            {
                                        ?>
                                            <option value="<?php echo $rs['m_siswa_id'] ?>"><?php echo $rs['nama_siswa'] ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <!-- <div class="mb-3">
                                        <label for="formFile" class="form-label">NIS</label>
                                        <input class="form-control" onchange="getData()" id="nis" type="text" value= "">
                                    </div> -->
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">NIS</label>
                                        <input class="form-control" id="nis" type="text" readonly value= "">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Kelas</label>
                                        <input class="form-control" id="kelas" type="text" readonly value= "">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Nominal PPDB</label>
                                        <input class="form-control" readonly type="text" id ="ppdb" name="nominal_ppdb">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Pembayaran PPDB</label>
                                        <input class="form-control" readonly type="text" id ="bayar" name="pembayaran_ppdb">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Tunggakan PPDB</label>
                                        <input class="form-control" readonly type="text" id ="sisa" name="tunggakan_ppdb">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFileMultiple" class="form-label">Nominal Dibayarkan</label>
                                    <input class="form-control" require type="text" id="nominal" name="nominal">
                                    </div>
                                </td>
                            
                            </tr>
                            
                        </table>
                        
                        <Button class="btn btn-warning" id="btnppdbconfirm" style="margin-top:10px; margin-bottom:10px" onclick="konfirmasi(this)">
                            Konfirmasi
                        </Button>


                        <table class="table align-items-center table-flush table-hover" id="tblppdb" style="margin-top:20px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Tgl Bayar</th>
                                <th>Jumlah Bayar</th>
                                <th>Petugas</th>
                            </tr>
                            </thead>
                            <tbody id="dtlppdb">
                            </tbody>
                        </table>

                        
                    </col>
                </Row>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>

<script>
    $(".kodebrg").chosen();
    function carisiswa(){
        var nis = $("#nama").val();
        $.ajax({
            type:'POST',
            url:'pages/ppdb/actionppdb.php',
            data:{
                carisiswa : 'true',
                m_siswa_id : nis
            },
            success:function(data){
                // console.log(data);
                var objek = $.parseJSON(data);
                var obj = objek
                obj = obj.data[0]
                $(`#nis`).val(obj.nis)
                $(`#kelas`).val(obj.kelas)
                $(`#ppdb`).val(duit(obj.ppdb))
                $(`#bayar`).val(duit(obj.bayar))
                $(`#sisa`).val(duit(obj.sisa))

                let tbl = objek.dataPPDB
                console.log(tbl);
                let dom = ``

                for(let i = 0; i<tbl.length; i++){
                    dom = dom + `<tr>
                        <td>${i+1}</td>
                        <td>${tbl[i].tgl}</td>
                        <td>${duit(tbl[i].nominal)}</td>
                        <td>${tbl[i].approvedby}</td>
                    </tr>`
                }
                $(`#dtlppdb`).html(dom);
            }
        })
    }
    function getData(){
        var nis = document.getElementById("nis").value;
        console.log(nis)
        
        $.ajax({
            type:'POST',
            url:'pages/ppdb/actionppdb.php',
            data:'tp=cek&nis='+nis,
            success:function(data){
                console.log(data);
                var obj = $.parseJSON(data);
                if(data !== "0"){
                    var nama = obj['nama_siswa'];
                    var kelas = obj['kelas'];
                    var tbl = obj['table']
                    var thn = obj['thn']
                    var cost = obj['cost']
                    var ppdb = obj['ppdb'];
                    var sisa = obj['sisa'];
                    console.log(cost,"ccccccc");
                    document.getElementById("nama").value = nama
                    document.getElementById("kelas").value = kelas
                    // document.getElementById("tahunajaran").value = thn
                    // document.getElementById("bulan").value = cost
                    document.getElementById("nominal_ppdb").value = ppdb
                    document.getElementById("byr").value = cost
                    document.getElementById("tunggakan_ppdb").value = sisa
                }else{
                    document.getElementById("nama").value = ""
                    document.getElementById("kelas").value = ""
                    document.getElementById("bulan").value = ""
                }
            
            }
        }); 
    }
    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');

        // Cetak hasil	
        // console.log(ribuan);
        return ribuan
    }
    function konfirmasi(v){
        if(!confirm(`Yakin melanjutkan transaksi...?`)){
            return;
        }   
        $(`#${v.id}`).html(`Please Wait...`)
        var nis = document.getElementById("nama").value;
        var nominal = document.getElementById("nominal").value;
        
        $.ajax({
            type:'POST',
            url:'pages/ppdb/actionppdb.php',
            data:{
                bayarppdb : 'true',
                m_siswa_id : nis,
                nominal : $(`#nominal`).val(),
                kelas : $(`#kelas`).val()
            },
            success:function(data){
                console.log(data);
                // var obj = Object.keys(data)
                if(data == "500"){
                    alert("Gagal...")
                }else{
                    window.location = "./?go=listppdb"
                }
            }
        }); 
    }
    function berhasil(){
        alert("Berhasil...")
        window.location = "./pages/ppdb/printppdb.php?id="+data
        // window.location='./?go=listppdb'
    }
</script>