<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <div class="row" style="margin-top:-35px">
    <?php
        $nis = $_SESSION['nis'];
        $sel = "select COALESCE(total,0) as cost,a.nama_siswa,nis,ppdb,nama_kelas 
        ,ppdb - COALESCE(total,0)  as sisa
        from m_siswa a
        inner join m_kelas c on c.m_kelas_id = a.m_kelas_id
        left join (select m_siswa_id,sum(nominal) as total from m_transaksi where jenis_transaksi = 'PPDB'
        and isactive = 1 and kode_status = 'WT2'
        group by m_siswa_id) b on a.m_siswa_id = b.m_siswa_id
        where nis = '$nis'";

        $da = mysqli_query($con,$sel);
        $rs = mysqli_fetch_array($da);
    ?>
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi PPDB Siswa
                    <div style="float:right; margin-bottom:10px">
                     <a href="./?go=ppdb"><button class="btn btn-danger">Riwayat PPDB</button></a>
                    </div>
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                        <table class="col-md-12" style="margin-top:10px; padding:20px">
                            <tr >
                            <form action="pages/ppdb/actionppdb.php" method="POST" enctype="multipart/form-data">
                              <input type="hidden" name="tp" value="ppdb">
                                <td>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">NIS</label>
                                        <input class="form-control" readonly id="nis" name ="nis" type="text" value= "<?php echo $rs['nis'] ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">NAMA</label>
                                        <input class="form-control" id="nama" type="text" readonly value= "<?php echo $rs['nama_siswa'] ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Kelas</label>
                                        <input class="form-control" id="kelas" type="text" readonly value= "<?php echo $rs['nama_kelas'] ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Nominal PPDB</label>
                                        <input class="form-control" readonly type="number" id ="nominal_ppdb" name="nominal_ppdb" value= "<?php echo $rs['ppdb']?>" >
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Pembayaran PPDB</label>
                                        <input class="form-control" readonly type="number" id ="byr" name="pembayaran_ppdb" value= "<?php echo $rs['cost']?>" >
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Tunggakan PPDB</label>
                                        <input class="form-control" readonly type="number" id ="tunggakan_ppdb" name="tunggakan_ppdb" value= "<?php echo $rs['sisa']?>" >
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">Nominal Dibayarkan</label>
                                        <input class="form-control" require type="number" id="nominal" name="nominal">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Upload Bukti Transfer</label>
                                        <input class="form-control" require type="file" id="formFileDisabled" name="bukti">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        
                        <Button class="btn btn-warning" style="margin-top:10px; margin-bottom:10px">
                            Konfirmasi
                        </Button>
                        </form>

                        <!-- <table class="table align-items-center table-flush table-hover" id="dataTableHover" style="margin-top:20px">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Bulan</th>
                                <th>Biaya SPP</th>
                                <th>Jumlah Bayar</th>
                                <th>Tgl Bayar</th>
                            </tr>
                            </thead>
                            <tbody id="partialdo">
                            </tbody>
                        </table> -->

                        
                    </col>
                </Row>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>

<script>
    function getData(){
        var nis = document.getElementById("nis").value;
        console.log(nis)

        $.ajax({
            type:'POST',
            url:'pages/ppdb/actionppdb.php',
            data:'tp=cek&nis='+nis,
            success:function(data){
                console.log(data);
                var obj = $.parseJSON(data);
                if(data !== "0"){
                    var nama = obj['nama_siswa'];
                    var kelas = obj['kelas'];
                    var tbl = obj['table']
                    var thn = obj['thn']
                    var cost = obj['cost']
                    var ppdb = obj['ppdb'];
                    var sisa = obj['sisa'];
                    console.log(cost,"ccccccc");
                    document.getElementById("nama").value = nama
                    document.getElementById("kelas").value = kelas
                    // document.getElementById("tahunajaran").value = thn
                    // document.getElementById("bulan").value = cost
                    document.getElementById("nominal_ppdb").value = ppdb
                    document.getElementById("byr").value = cost
                    document.getElementById("tunggakan_ppdb").value = sisa
                }else{
                    document.getElementById("nama").value = ""
                    document.getElementById("kelas").value = ""
                    document.getElementById("bulan").value = ""
                }
            
            }
        }); 
    }

    function konfirmasi(){
        var nis = document.getElementById("nis").value;
        var nominal = document.getElementById("nominal").value;

        $.ajax({
            type:'POST',
            url:'pages/ppdb/actionppdb.php',
            data:'tp=ppdb_manual&nis='+nis+'&nominal='+nominal,
            success:function(data){
                console.log(data);
                // var obj = Object.keys(data)
                if(data == "200"){
                    berhasil();
                    console.log("yessss....");
                }
            }
        }); 
    }
    function berhasil(){
        alert("Berhasil...")
        window.location='./?go=listppdb'
    }
</script>