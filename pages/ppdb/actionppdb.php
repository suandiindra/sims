<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    $obj = new stdClass();

    $tipe = "";
    $id_uniq = "select uuid() as id";
    $result = mysqli_query($con,$id_uniq);
    $id_uniq = mysqli_fetch_array($result);
    $id = $id_uniq["id"];
    
    $_user = ($_SESSION["m_user_id"]);
    $_nama = $_SESSION['nama'];
    // $_kelas = ($_SESSION['kelas']);

    if(isset($_POST['carisiswa'])){
        $m_siswa_id = $_POST['m_siswa_id'];
        $sel = "select nis,nisn,nama_siswa,kelas,ppdb,coalesce(bayar,0) as bayar,coalesce(ppdb - coalesce(bayar,0),0) as sisa from m_siswa ms 
                left join (select m_siswa_id,  coalesce (sum(nominal),0) as bayar from m_transaksi mt where m_siswa_id = '$m_siswa_id'
                and jenis_transaksi = 'PPDB' and kode_status = 'WT2' group by m_siswa_id) b on b.m_siswa_id = ms.m_siswa_id 
                where ms.m_siswa_id = '$m_siswa_id'";

        $data1 = json_decode(queryJson($con,$sel),true)['data'];
        

        $sel2 = "select date_format(mt.createdate ,'%d-%M-%y %H:%i') as tgl, 
        nominal,approvedby 
        from m_transaksi mt where m_siswa_id = '$m_siswa_id'
        and jenis_transaksi = 'PPDB' and kode_status = 'WT2' order by createdate desc";

        $data2 = json_decode(queryJson($con,$sel2),true)['data'];
        $obj -> data = $data1;
        $obj -> dataPPDB = $data2;

        $data = json_encode($obj);
        echo $data;


        // echo queryJSON($con,$sel);


    }
    if(isset($_POST['edit'])){
        $key = $_POST['id'];
        $nominal = $_POST['nominal'];

        $upd = "update m_transaksi set nominal = '$nominal',approvedby = '$_nama' 
        where m_transaksi_id = '$key'";

        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }
    if(isset($_POST['hapus'])){
        $key = $_POST['id'];
        $nominal = $_POST['nominal'];

        $upd = "delete from m_transaksi
        where m_transaksi_id = '$key'";

        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }
    if(isset($_POST['bayarppdb'])){
        $m_siswa_id = $_POST['m_siswa_id'];
        $nominal = $_POST['nominal'];
        $kelas = $_POST['kelas'];

        $sel = "select * from m_siswa where m_siswa_id = '$m_siswa_id'";
        $rt = mysqli_query($con,$sel);
        $rp = mysqli_fetch_array($rt);
        $thn = $rp['tahun_ajaran'];

        $insert = "insert into m_transaksi (m_siswa_id,m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
        ,nominal,kode_status,status_transaksi,createdby,approvedby,createdate,kelas,tahun_ajaran)
        values ('$m_siswa_id','$id','PPDB','OFFLINE',1
        ,'$nominal','WT2','Approved','$m_siswa_id','$_nama',now(),'$kelas','$thn') ";

        $res = mysqli_query($con,$insert);
        if($res){
            echo $id;
        }else{
            echo "500";
        }
    }

    if(isset($_POST['tp'])){
        if($_POST['tp'] == "cek"){
            $nis = $_POST['nis'];
            $sel = "select COALESCE(total,0) as cost,a.nama_siswa,nis,ppdb,nama_kelas from m_siswa a
            inner join m_kelas c on c.m_kelas_id = a.m_kelas_id
            left join (select m_siswa_id,sum(nominal) as total from m_transaksi where jenis_transaksi = 'PPDB'
            and isactive = 1 and kode_status = 'WT2'
            group by m_siswa_id) b on a.m_siswa_id = b.m_siswa_id
            where nis = '$nis'";
            // echo $sel;

            $res = mysqli_query($con,$sel);
            if(mysqli_num_rows($res) > 0){
                $dt = mysqli_fetch_array($res);
                $id = $dt['m_siswa_id'];
                $thn = $dt['tahun_ajaran'];

                $data ->nis = $dt['nis'];
                $data ->kelas = $dt['nama_kelas'];
                $data ->nama_siswa = $dt['nama_siswa'];
                $data ->cost = $dt['cost'];
                $data ->ppdb = $dt['ppdb'];
                $data ->sisa = $dt['ppdb'] - $dt['cost'] ;
                $data ->thn = $thn;
                $data = json_encode($data);
                echo $data;
            }else{
                echo "0";
            }
        }else if($_POST['tp'] == "ppdb"){
            $nominal = $_POST['nominal'];
            $nis = $_POST['nis'];

            $cekid = "select * from m_siswa where nis = '$nis'";
            $results1 = mysqli_query($con,$cekid);
            $dt = mysqli_fetch_array($results1);
            $tahun_ajaran = $dt['tahun_ajaran'];

            $nama_file1 = $_FILES['bukti']['name'];
            $ukuran_file1 = $_FILES['bukti']['size'];
            $tipe_file1 = $_FILES['bukti']['type'];
            $tmp_file1 = $_FILES['bukti']['tmp_name'];
            $path1 = "../../asset/bukti_ppdb/".$id."/".$nama_file1;
            // echo $tahun_ajaran;

            if( is_dir($path1) === false )
            {
                mkdir("../../asset/bukti_ppdb/".$id."/");
            }
            $pathData = "asset/bukti_ppdb/".$id."/".$nama_file1;
            // echo $pathData;
            if($ukuran_file1 <= 1000000){	
                // echo $tmp_file1,$path1;	
                if(move_uploaded_file($tmp_file1, $path1)){
                    $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                    ,nominal,kode_status,status_transaksi,createdby,createdate,bukti,m_siswa_id,kelas,tahun_ajaran)
                    values ('$id','PPDB','ONLINE',1
                    ,'$nominal','WT1','Menunggu Konfirmasi','$_user',now(),'$pathData','$_user','$_kelas','$tahun_ajaran') ";
                    // echo $upd;
                    $result = mysqli_query($con,$upd);
                    if($result){
                        echo "<script>alert('Berhasil')</script>";
                        echo "<script>window.location='../../?go=ppdb'</script>";
                    }
                }else{
                    echo "gagal";
                }
            }
        }
        // echo  "d";
        // echo $_POST['tp'];
    }else if(isset($_GET['id'])){
        $id = $_GET['id'];
        $act = $_GET['act'];
        echo $id;
        if($act == "1"){
            $cek_ = "select * from m_user where m_user_id = '$_user'";
            $result = mysqli_query($con,$cek_);
            if(mysqli_num_rows($result) > 0){
                $cek2 = "select * from m_transaksi where m_transaksi_id = '$id' and kode_status = 'WT1'";
                // echo $cek2;
                $result1 = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result1) > 0){
                    $upd = "update m_transaksi set status_transaksi = 'Approved', kode_status = 'WT2',approvedby = '$_user'
                    ,approvedate = now() where m_transaksi_id = '$id'";

                    // echo $upd;
                    $result2 = mysqli_query($con,$upd);
                    if($result2){
                        echo "<script>alert('Transaksi Berhasil...')</script>";
                        echo "<script>window.location='../../?go=listppdb'</script>";
                    }
                }

            }else{
                echo "<script>alert('Tidak diizinkan lagi...')</script>";
                echo "<script>window.location='../../?go=listppdb'</script>";
            }
        }else{
            $cek_ = "select * from m_user where m_user_id = '$_user'";
            $result = mysqli_query($con,$cek_);
            if(mysqli_num_rows($result) > 0){
                $cek2 = "select * from m_transaksi where m_transaksi_id = '$id' and kode_status = 'WT1'";
                // echo $cek2;
                $result1 = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result1) > 0){
                    $upd = "update m_transaksi set status_transaksi = 'Rejected', kode_status = 'WT0',approvedby = '$_user'
                    ,approvedate = now() where m_transaksi_id = '$id'";

                    // echo $upd;
                    $result2 = mysqli_query($con,$upd);
                    if($result2){
                        echo "<script>alert('Reject Berhasil...')</script>";
                        echo "<script>window.location='../../?go=listppdb'</script>";
                    }
                }

            }else{
                echo "<script>alert('Tidak diizinkan lagi...')</script>";
                echo "<script>window.location='../../?go=listppdb'</script>";
            }
        }
    }
?>