<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Pembayaran PPDB Siswa</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">PPDB Siswa</li>
    </ol>
</div>
      <?php
      $where = " where 1=1 and jenis_transaksi = 'PPDB' ";
      $disp = "display:block";
      if($_SESSION['role'] == "siswa"){
        $id_sis = $_SESSION['m_user_id'] ;
        $where = $where." and a.m_siswa_id = '$id_sis'";
        $disp = "display:none";
      }   
        if(isset($_POST['lihat'])){
          if($_POST['randcheck']==$_SESSION['rand']){
            $kelas = $_POST['kelas'];
            $sts   = $_POST['stat'];
            // $where = " and a.m_kelas_id = '$kelas'";
            // echo strlen($sts);
            // echo strlen($kelas);
            
            if(strlen($sts) > 0){
                $where = $where." and kode_status = '$sts'";
            }

            if(strlen($kelas) > 0){
                $where = $where." and b.m_kelas_id = '$kelas'";
            }

          }
        }
      ?>
      <div class="card">
          <div class="card-body">
          <form action="" method="POST">
            <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
            ?> 
            <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" /> 
            <div class="container col-12" style="margin-top:0px">
            <div class="row" style="margin-bottom:20px">
                <div class="col-sm">
                    <label for="inputPassword5" class="form-label">Status Pembayaran</label>
                    <select class="form-control" name="stat" id = "stat">
                        <option value="">Semua</option>
                        <option value="WT1">Menunggu Konfirmasi</option>
                        <option value="WT2">Approved</option>
                        <option value="WT0">Rejected</option>
                    </select>
                </div>
                <div class="col-sm">
                    <label style="<?php echo $disp ?>" for="inputPassword5" class="form-label">kelas</label>
                    <select style="<?php echo $disp ?>" class="form-control" name="kelas" id ="kelas">
                        <option value="">Semua</option>
                    <?php
                        $del = "select * from m_kelas where isactive = 1 order by nama_kelas asc";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="col-sm" style="margin-top:32px">
                    <Button class="btn btn-success col-md-2" name="lihat" >Lihat</Button>
                </div>
            </div>
            </div>
        </form>
        <?php
            if($_SESSION['role'] != "siswa")
            {
        ?>
        <Button class="btn btn-danger col-md-1" onclick="exp()" style="float:right; margin-top:-60px; margin-right:150px" name="addspp" >Export</Button>
        <a href="./?go=formppdb">
            <Button class="btn btn-primary col-md-1" style="float:right; margin-top:-60px" id="addspp" >Input Data</Button>
        </a>
        <?php } ?>
        </div>
      </div>

<div class="row" style="margin-top:20px">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="dataTableHover" style="margin-top:20px">
                <thead class="thead-light">
                <tr>
                        <th>No.</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kelass</th>
                        <th>Nominal</th>
                        <th>Tgl Bayar</th>
                        <th>Metode bayar</th>
                        <th>Status</th>
                        <th>Petugas</th>
                        <th>Action</th>
                </tr>
                </thead>
                <tbody id="partialdo">
                    <?php
                        $sel = "select d.username as username, m_transaksi_id, bukti,nis,nama_siswa,c.nama_kelas as nama_kelas,bulan,tipe_transaksi,
                        status_transaksi,nominal,DATE_FORMAT(a.createdate, '%d-%M-%Y') tgl_bayar,kode_status 
                        ,a.approvedby,a.m_transaksi_id
                        from m_transaksi a
                        inner join m_siswa b on a.m_siswa_id = b.m_siswa_id
                        inner join m_kelas c on c.m_kelas_id = b.m_kelas_id
                        left join m_user d on d.m_user_id = a.approvedby $where order by a.createdate desc";

                        // echo $sel;
                        $result = mysqli_query($con,$sel);
                        $i = 1;
                        while($res = mysqli_fetch_array($result)){
                        $color = "";
                        if($res['kode_status'] == "WT1"){
                            $color = "background-color:#FF2D5B; color:white";
                        }else if($res['kode_status'] == "WT0"){
                            $color = "background-color:#D3C8BA; color:red";
                        }else{
                            $color = "background-color:#7CCFC9; color:white";
                        }
                    ?>
                    <tr style="<?php echo $color; ?>">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $res['nis']; ?></td>
                            <td><?php echo $res['nama_siswa']; ?></td>
                            <td><?php echo $res['nama_kelas']; ?></td>
                            <td><?php echo number_format($res['nominal']); ?></td>
                            <td><?php echo $res['tgl_bayar']; ?></td>
                            <td><?php echo $res['tipe_transaksi']; ?></td>
                            <td><?php echo $res['status_transaksi']; ?></td>
                            <td><?php echo $res['approvedby'] ?></td>
                            <td style="text-align:center">
                                <?php
                                    if($res['kode_status'] == "WT2" && $_SESSION['role'] == "Mgr Tata Usaha"){
                                ?>
                                    <button class="btn btn-danger" onclick="aksi('<?php echo $res['m_transaksi_id']; ?>','<?php echo $res['nama_siswa'] ?>','<?php echo $res['tgl_bayar'] ?>','<?php echo $res['nominal'] ?>')">Aksi</button>
                                <?php
                                    }
                                ?>
                            </td>
                    </tr>
                    <?php
                        $i += 1;
                        }
                    ?>
                </tbody>
            </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->
        <div class="modal fade" id="modalaksippdb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran PPDB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label for="formFileMultiple" class="form-label">NAMA</label>
                    <input class="form-control" id="nama" type="text" readonly >
                    <input class="form-control" id="keys" type="hidden" >
                </div>
                <div class="mb-3">
                    <label for="formFileDisabled" class="form-label">TGL BAYAR</label>
                    <input class="form-control" id="tgl" type="text" readonly>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">NOMINAL PPDB</label>
                    <input class="form-control" type="number" id ="nominal" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="hapusppdb()">Hapus</button>
                <button type="button" class="btn btn-primary" onclick="editppdb()">Edit</button>
            </div>
            </div>
        </div>
        </div>
<script>
    

    window.addEventListener('load', loadppdb(), false);
    function loadppdb(){
        let role = "<?php echo $_SESSION['role']?>"
        if(role !== "Tata Usaha"){
            $(`#addspp`).css('display','none')
        }
    }
    function confirmationDelete(anchor)
    {
        var conf = confirm('Apakah yakin melakukan proses ini ???');
        if(conf)
            window.location=anchor.attr("href");
    }

    function exp(){
        var sts = document.getElementById("stat").value
        var kelas = document.getElementById("kelas").value

        window.location="./pages/ppdb/exportppdb.php";
    }
    function editppdb(){
        console.log('woii');
        $.ajax({
            type:'POST',
            url:`pages/ppdb/actionppdb.php`,
            data: {
                edit : true,
                id : $(`#keys`).val(),
                nominal : $(`#nominal`).val()
            },
            success:function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=rekapitulasi";
                }
            }
        })
    }
    function hapusppdb(){
        console.log('woii');
        $.ajax({
            type:'POST',
            url:`pages/ppdb/actionppdb.php`,
            data: {
                hapus : true,
                id : $(`#keys`).val(),
                nominal : $(`#nominal`).val()
            },
            success:function(data){
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=rekapitulasi";
                }
            }
        })
    }
    function aksi(id,nama,tgl,nominal){
        console.log(id,nama,tgl,nominal);
        $(`#nama`).val(nama)
        $(`#keys`).val(id)
        $(`#tgl`).val(tgl)
        $(`#nominal`).val(nominal)
        $(`#modalaksippdb`).modal('show')
    }
</script>