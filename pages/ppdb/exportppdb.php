<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();

    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Data Rekap SPP.xls");

    $_user = $_SESSION["m_user_id"];
    $_nama = $_SESSION['nama'];
    $_kelas = $_SESSION['kelas'];

    $where = " where 1=1 and jenis_transaksi = 'PPDB'";
    $sts = "";
    $kelas = "";
    $sts = isset($_GET['sts']) ? $_GET['sts'] : null;
    $kelas = isset($_GET['kelas']) ? $_GET['kelas'] : null;

    if($sts){
        $where= $where." and kode_status = '$sts'";
    }
    if($kelas){
        $where= $where." and b.m_kelas_id = '$kelas'";
    }

    // echo $sts;
    
    $where == $sts ? $where + " and kode_status = '$sts'" : "";
    $where == $kelas ? $where + " and b.m_kelas_id = '$kelas'" : "";

    $sel = "select username, m_transaksi_id, bukti,nis,nama_siswa,c.nama_kelas as nama_kelas,bulan,tipe_transaksi,
            status_transaksi,nominal,DATE_FORMAT(a.createdate, '%d-%M-%Y') tgl_bayar,kode_status from m_transaksi a
            inner join m_siswa b on a.m_siswa_id = b.m_siswa_id
            inner join m_kelas c on c.m_kelas_id = b.m_kelas_id
            left join m_user d on d.m_user_id = a.approvedby $where";
    
            // echo $sel;
?>

<table border=1>
<thead class="thead-light">
    <tr>
    <th>NIS</th>
    <th>Nama Siswa</th>
    <th>Kelas</th>
    <th>Bayar</th>
    <th>Tanggal Bayar</th>
    </tr>
</thead>
<tbody>
<?php
    $result = mysqli_query($con,$sel);
    $i = 1;
    while($res = mysqli_fetch_array($result)){
?>  
    <tr>
        <td><?php echo $res['nis']; ?></td>
        <td><?php echo $res['nama_siswa']; ?></td>
        <td><?php echo $res['nama_kelas']; ?></td>
        <td><?php echo $res['nominal']; ?></td>
        <td><?php echo $res['tgl_bayar']; ?></td>
    </tr>
<?php
    }
?>