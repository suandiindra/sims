<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Generate Laporan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Generate Laporan</li>
        </ol>
    </div>
    <div class="card" style="margin-top:-20px">
         <div class="card-body">
            <div class="row" style="margin-bottom:20px">
                <div class="col col-md-12">
                    <div class="text-center">
                        <button class="btn btn-danger" id="btngenerate" onclick="generate(this)"><i class="fa fa-cog"></i>  GENERATE LAPORAN</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.onload = function(){

    }

    function generate(v){
        // console.log(v);
        $(`#${v.id}`).prop('disabled', true);
        $(`#${v.id}`).html(`Please Wait ...`)
        $.ajax({
            type:'POST',
            url:'pages/kehadiran/kehadiran_be.php',
            data:{
                generate : true
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert(`Berhasil`)
                    window.location = "./?go=generate"
                }
            }
        })
    }
</script>