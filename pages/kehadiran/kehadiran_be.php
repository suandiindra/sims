<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    if(isset($_POST['generate'])){
        $del1 = "delete from t_absen where date_format(createdate,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";
        $res = mysqli_query($con,$del1);

        $del2 = "update log_absen set isproses = null where date_format(createddate ,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";
        $res2 = mysqli_query($con,$del2);

        $sel = "insert into t_absen (t_absen_id ,m_user_id ,nama,m_role_id ,masuk,keluar ,createdate )
        select uuid(),card_id,nama,'SISWA',min(waktu) as masuk,case when TIMESTAMPDIFF(minute,min(waktu),max(waktu)) < 60 then ''
        else max(waktu) end as pulang
        ,now() 
        from log_absen
        where isproses is null
        group by card_id,nama,date_format(createddate,'%Y-%m-%d')";

        // echo $sel;
        $res = mysqli_query($con,$sel);
        if($res){
            // $upd = "update log_absen set isproses = 1 where isproses is null";
            // $res = mysqli_query($con,$upd);
            echo "200";
        }else{
            echo $sel;
        }
        
    }
    if(isset($_GET['find'])){
        $tgl1 = $_GET['tgl1'];
        $tgl2 = $_GET['tgl2'];
        $kelas = $_GET['kelas'];
        $filter = "";
        $filtertgl = " and date_format(masuk,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";
        if(strlen($tgl1) > 0){
            $filtertgl = " and date_format(masuk,'%Y-%m-%d') between '$tgl1' and '$tgl2'";
        }
        if(strlen($kelas) > 0){
            $filter = " and m_kelas_id = '$kelas' ";
        }   

        $filtersiswa = "";
        if($_SESSION['role'] == "siswa"){
            $key = $_SESSION['m_user_id'];
            $filtersiswa = " and ms.m_siswa_id = '$key'";
        }
        $sel = "select nis,date_format(b.masuk ,'%d-%m-%Y') as period,nama_siswa,ms.kelas 
        ,date_format(b.masuk ,'%H:%i') as masuk
        ,case when date_format(b.keluar,'%H:%i') = '00:00' then '' else date_format(b.keluar,'%H:%i') end as pulang
        ,case when date_format(b.masuk ,'%H:%i') > '07:15'then 'TERLAMBAT' else '' end ket
        ,ms.tahun_ajaran
        from m_siswa ms 
        left join t_absen b on b.m_user_id = ms.id_absen $filtertgl
        where 1=1 and ms.isactive = 1 $filter  $filtersiswa order by nama_siswa asc";

        // echo $sel;
        echo queryJson($con,$sel);
    }
    if(isset($_GET['export'])){
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Data Kehadiran.xls");

        $tgl1 = $_GET['tgl1'];
        $tgl2 = $_GET['tgl2'];
        $kelas = $_GET['kelas'];
        $filter = "";
        $filtertgl = " and date_format(masuk,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";
        if(strlen($tgl1) > 0){
            $filtertgl = " and date_format(masuk,'%Y-%m-%d') between '$tgl1' and '$tgl2'";
        }
        if(strlen($kelas) > 0){
            $filter = " and m_kelas_id = '$kelas' ";
        }   

        $filtersiswa = "";
        if($_SESSION['role'] == "siswa"){
            $key = $_SESSION['m_user_id'];
            $filtersiswa = " and ms.m_siswa_id = '$key'";
        }


        $sel = "select nis,date_format(b.masuk ,'%d-%m-%Y') as period,nama_siswa,ms.kelas 
        ,date_format(b.masuk ,'%H:%i') as masuk
        ,case when date_format(b.keluar,'%H:%i') = '00:00' then '' else date_format(b.keluar,'%H:%i') end as pulang
        ,case when date_format(b.masuk ,'%H:%i') > '07:15'then 'TERLAMBAT' else '' end ket
        from m_siswa ms 
        left join t_absen b on b.m_user_id = ms.id_absen $filtertgl
        where 1=1 $filter $filtersiswa order by nama_siswa asc";

        $res = mysqli_query($con,$sel);
        ?>
            <table border="1" class="table align-items-center table-flush table-hover" id="tblkehadiran">
                <thead class="thead-light">
                    <th>NIS</th>
                    <th style="width:270px">Nama</th>
                    <th style="width:50px">Kelas</th>
                    <th >Periode</th>
                    <th>Masuk</th>
                    <th>Keluar</th>
                    <th>Keterangan</th>
                </thead>
                <tbody id="dtlkehadiran">
                    <?php 
                        while($do = mysqli_fetch_array($res)){
                    ?>
                        <tr>
                            <td><?php  echo "'".$do['nis'];?></td>
                            <td><?php  echo $do['nama_siswa'];?></td>
                            <td><?php  echo $do['kelas'];?></td>
                            <td><?php  echo $do['period'] ? $do['period'] : "";?></td>
                            <td><?php  echo $do['masuk'] ? $do['masuk'] : "";?></td>
                            <td><?php  echo $do['pulang'] ? $do['pulang'] : "";?></td>
                            <td><?php  echo $do['ket'] ? $do['ket'] : "";?></td> 
                        </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
    <?php
    }
?>