<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Laporan Kehadiran</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan Kehadiran</li>
        </ol>
    </div>
    <div class="card" style="margin-top:-20px">
          <div class="card-body">
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Period</label>
                            <input type="date" class="form-control" id="tgl1">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Period</label>
                            <input type="date" class="form-control" id="tgl2">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="inputPassword5" class="form-label">kelas</label>
                        <select required class="form-control" id="kelas">
                            <option value="">ALL</option>
                        <?php
                            $del = "select * from m_kelas where isactive = 1 order by nama_kelas asc";
                            $res = mysqli_query($con,$del);
                            while($rs = mysqli_fetch_array($res))
                            {
                        ?>
                            <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-3" style="margin-top:32px">
                        <Button class="btn btn-success" onclick="load()">Lihat</Button>
                        <Button class="btn btn-primary" onclick="exp()">Export</Button>
                    </div>
                    <!-- <div class="col-md-3" style="margin-top:32px">
                    </div> -->
            </div>
          </div>
    </div>

    <div class="card" style="margin-top:10px">
        <div class="card-body">
        <div class="row" style="margin-bottom:20px">
            <div class="col-md-12">
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="tblkehadiran">
                        <thead class="thead-light">
                            <th>NIS</th>
                            <th style="width:270px">Nama</th>
                            <th style="width:50px">Kelas</th>
                            <th >Periode</th>
                            <th>Masuk</th>
                            <th>Keluar</th>
                            <th>Keterangan</th>
                        </thead>
                        <tbody id="dtlkehadiran">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    window.onload = function(){
        load()
    }
    function exp(){
        let tgl1 = $(`#tgl1`).val()
        let tgl2 = $(`#tgl2`).val()
        let kelas = $(`#kelas`).val()
        window.location="pages/kehadiran/kehadiran_be.php?export=true&tgl1="+tgl1+"&tgl2="+tgl2+"&kelas="+kelas
    }
    function load(){
        let tgl1 = $(`#tgl1`).val()
        let tgl2 = $(`#tgl2`).val()
        let kelas = $(`#kelas`).val()
        // console.log('s');
        var table = $('#tblkehadiran').DataTable();
        table.clear().destroy(); 
        $.ajax({
            type:'GET',
            url:'pages/kehadiran/kehadiran_be.php',
            data:{
                find : true,
                tgl1 : tgl1,
                tgl2 : tgl2,
                kelas : kelas
            },
            success: function(data){
                // console.log(data);
                // return;
                let obj = JSON.parse(data)
                obj = obj.data
                let dom = ``
                let jumlahHadir = 0
                for(let i = 0; i<obj.length; i++){
                    jumlahHadir = jumlahHadir + parseFloat(obj[i].masuk ? 1 : 0)
                    dom = dom + `<tr>
                        <td>${obj[i].nis}</td>
                        <td>${obj[i].nama_siswa}</td>
                        <td>${obj[i].kelas}</td>
                        <td>${obj[i].period ? obj[i].period : ''}</td>
                        <td>${obj[i].masuk ? obj[i].masuk : ''}</td>
                        <td>${obj[i].pulang ? obj[i].pulang : ''}</td>
                        <td>${obj[i].ket ? obj[i].ket : ''}</td>
                    </tr>`
                }
                console.log(jumlahHadir);
                $(`#dtlkehadiran`).html(dom)
                $('#tblkehadiran').DataTable(({ 
                        "destroy": true, //use for reinitialize datatable
                        order: [[1, 'desc']],
                }));
                // if(data == "200"){
                //     alert('Berhasil')
                //     window.location = './?go=listsiswa'
                // }
            }
        })
    }
</script>