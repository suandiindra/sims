<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");

    if(isset($_POST['generatex'])){
        $sel = "select nisn,nama_siswa,kelas,id_absen  from m_siswa ms 
        group by nisn,nama_siswa,kelas,id_absen 
        having count(*) > 1";
        $res = mysqli_query($con,$sel);
        while($do = mysqli_fetch_array($res)){
            $idx = $do['nisn'];
            $sl2 = "select * from m_siswa ms where nisn = '$idx'";
            $res2 = mysqli_query($con,$sl2);
            while($dp = mysqli_fetch_array($res2)){
                $del_id = $dp['m_siswa_id'];
                $photo = $dp['photo'];
                $createdby = $dp['createdby'];
                $id_absen = $dp['id_absen'];
                // echo $id_absen."\n";
                if(strlen($photo) == 0){
                    $del = "delete from m_siswa where m_siswa_id = '$del_id'";
                    // echo $del."\n";
                    mysqli_query($con,$del);
                }
            }
        }

    }
    if(isset($_POST['generate'])){
        // echo "asd";
        $sel = "select ms.nisn,nama_siswa,ms.biaya_spp,ms.daftar_ulang 
        ,b.jul ,b.aug 
        ,b.sept ,b.okt ,b.nov ,b.des 
        ,b.jan ,b.feb ,b.mar ,b.apr 
        ,b.mei ,b.jun ,ms.m_kelas_id,ms.m_siswa_id
        ,uuid() as trans,ms.kelas
        from m_siswa ms 
        inner join temp_tu_baru b on b.nisn = ms.nisn ";
        $res = mysqli_query($con,$sel);
        while($do = mysqli_fetch_array($res)){
            $kelas = $do['m_kelas_id'];
            $nama_kelas = $do['kelas'];
            $thn = "2022/2023";
            $id_unik_transaksi = $do['trans'];
            $m_siswa_id_ = $do['m_siswa_id'];
            $biaya_spp = $do["biaya_spp"];
            $daftar_ulang = $do["daftar_ulang"];
            $total = $do["jul"] + $do["aug"] + $do["sept"] + $do["okt"] + $do['nov'] + $do['des'] + $do['jan'] + $do['feb']+$do['mar']+$do['apr'] + $do['mei']+$do['jun'];
            $bulan_bayar = 0;
            // echo $total;
            if($total > 0){
                $bulan_bayar = $total/$biaya_spp;
                $ins = "insert into m_transaksi (m_transaksi_id,m_siswa_id,kelas,jenis_transaksi,tipe_transaksi
                ,bulan,nominal,isactive,status_transaksi,kode_status,createdate,createdby,approvedby,tahun_ajaran,harus_bayar)
                values 
                ('$id_unik_transaksi','$m_siswa_id_','$nama_kelas','BAYAR SPP','OFFLINE'
                ,$bulan_bayar,$total,1,'Approved','WT2',now(),'SISTEM','SISTEM','$thn','$biaya_spp')";

                mysqli_query($con,$ins);
            }

            $bulan = array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
            for($i = 0; $i<count($bulan); $i++){
                $seq = $i+1;
                $bln = $bulan[$i];
                $id_uniq2 = "select uuid() as id";
                $result2 = mysqli_query($con,$id_uniq2);
                $id_uniq2 = mysqli_fetch_array($result2);
                $id_uniq2 = $id_uniq2["id"];
                if($i < $bulan_bayar){
                    // echo $i." ".$bulan_bayar;
                    $spp_query =  "insert into transaksi_spp (transaksi_spp_id,transaksi_id,m_siswa_id,kelas,tahun_ajaran,seq,bulan,biaya_spp,bayar_spp,creatdate,petugas)
                    values ('$id_uniq2','$id_unik_transaksi','$m_siswa_id_','$kelas','$thn',$seq,'$bln','$biaya_spp','$biaya_spp',now(),'SISTEM')";
                    mysqli_query($con,$spp_query);
                }else{
                    $spp_query = "insert into transaksi_spp (transaksi_spp_id,m_siswa_id,kelas,tahun_ajaran,seq,bulan,biaya_spp,bayar_spp,creatdate)
                    values ('$id_uniq2','$m_siswa_id_','$kelas','$thn',$seq,'$bln','$biaya_spp','0',now())";
                    mysqli_query($con,$spp_query);
                }
            }


            //masuk DU
            $updDU = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
            ,nominal,kode_status,status_transaksi,createdby,createdate,m_siswa_id,kelas,tahun_ajaran,approvedby,harus_bayar)
            values (uuid(),'DAFTAR ULANG','OFFLINE',0
            ,0,'WT0','Waiting','SISTEM',now(),'$m_siswa_id_','$nama_kelas','$thn','SISTEM','$daftar_ulang') ";
            
            // echo $updDU."\n";
            $result = mysqli_query($con,$updDU);

            // echo "Berhasil";
        }

    }

?>