<div class="container">
    <button class="btn btn-success" id="btn" onclick="generate(this)">Generate</button>
</div>

<script>
    function generate(v){
        $(`#${v.id}`).html('Tunggu...')
        $.ajax({
            type:'POST',
            url:'pages/generator/generator_be.php',
            data:{
                generate : true
            },
            success: function(data){
                $(`#${v.id}`).html('Generate')
                console.log(data);
            }
        })
    }
</script>