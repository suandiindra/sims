<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <div class="row" style="margin-top:-35px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi Data Siswa
                    <div style="float:right; margin-bottom:10px">
                     <a href="./?go=listbayarspp"><button class="btn btn-danger">Riwayat Pembayaran SPP</button></a>
                    </div>
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                        <table class="col-md-12" style="margin-top:10px; padding:20px">
                            <tr >
                            <form action="pages/SPP/actionspp.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="tp" value="trf-spp">
                                <td>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">NIS</label>
                                        <input class="form-control" type="text" readonly value= "<?php  echo $_SESSION['nis']; ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">NAMA</label>
                                        <input class="form-control" type="text" readonly value= "<?php  echo $_SESSION['nama']; ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Kelas</label>
                                        <input class="form-control" type="text" readonly value= "<?php  echo $_SESSION['kelas']; ?>">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFile" class="form-label">Bulan Bayar</label>
                                    <input class="form-control" require type="number" name="bulan">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFileMultiple" class="form-label">Nominal</label>
                                    <input class="form-control" require type="number" name="nominal">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFileDisabled" class="form-label">Upload Bukti Transfer</label>
                                    <input class="form-control" require type="file" id="formFileDisabled" name="bukti">
                                    </div>
                                </td>
                            
                            </tr>
                            
                        </table>
                        <Button class="btn btn-warning" style="margin-top:10px">
                            Konfirmasi
                        </Button>
                        </form>
                    </col>
                </Row>
            </div>
            

            <table class="table align-items-center table-flush table-hover" id="dataTableHover" style="margin-top:20px">
                <thead class="thead-light">
                <tr>
                    <th>No</th>
                    <th>Bulan</th>
                    <th>Biaya SPP</th>
                    <th>Jumlah Bayar</th>
                    <th>Tgl Bayar</th>
                </tr>
                </thead>
                <tbody id="partialdo">
                    <?php
                        $sel = "select * from transaksi_spp where m_siswa_id = '$_user'";
                        $result = mysqli_query($con,$sel);
                        $i = 1;
                        while($res = mysqli_fetch_array($result)){
                        $color = "";
                        if($res['bayar_spp'] == "0"){
                            $color = "background-color:#BF202A; color:white";
                        }else{
                            $color = "background-color:#106DCA; color:white";
                        }
                    ?>
                    <tr style="<?php echo $color; ?>">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['bulan']; ?></td>
                        <td><?php echo number_format($res['biaya_spp']); ?></td>
                        <td><?php echo number_format($res['bayar_spp']); ?></td>
                        <td><?php echo $res['creatdate']; ?></td>
                    </tr>
                    <?php
                        $i += 1;
                        }
                    ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>



</div>