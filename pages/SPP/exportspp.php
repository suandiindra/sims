<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();

    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Data Rekap SPP.xls");

    $_user = $_SESSION["m_user_id"];
    $_nama = $_SESSION['nama'];

    $sel = "select b.nama_siswa,nama_kelas
    ,max(case when seq = 1 then bayar_spp else 0 end) as Juli
    ,max(case when seq = 2 then bayar_spp else 0 end) as Agustus
    ,max(case when seq = 3 then bayar_spp else 0 end) as September
    ,max(case when seq = 4 then bayar_spp else 0 end) as Oktober
    ,max(case when seq = 5 then bayar_spp else 0 end) as November
    ,max(case when seq = 6 then bayar_spp else 0 end) as Desember
    ,max(case when seq = 7 then bayar_spp else 0 end) as Januari
    ,max(case when seq = 8 then bayar_spp else 0 end) as Februari
    ,max(case when seq = 9 then bayar_spp else 0 end) as Maret
    ,max(case when seq = 10 then bayar_spp else 0 end) as April
    ,max(case when seq = 11 then bayar_spp else 0 end) as Mei
    ,max(case when seq = 12 then bayar_spp else 0 end) as Juni
    from transaksi_spp a
    inner join m_siswa b on a.m_siswa_id = b.m_siswa_id
    inner join m_kelas c on c.m_kelas_id = b.m_kelas_id
    and a.tahun_ajaran = b.tahun_ajaran
    group by b.nama_siswa,nama_kelas
    order by nama_kelas,nama_siswa";
?>

<table border=1>
<thead class="thead-light">
    <tr>
    <th>Nama Siswa</th>
    <th>Kelas</th>
    <th>Juli</th>
    <th>Agustus</th>
    <th>September</th>
    <th>Oktober</th>
    <th>November</th>
    <th>Desember</th>
    <th>Januari</th>
    <th>Februari</th>
    <th>Maret</th>
    <th>April</th>
    <th>Mei</th>
    <th>Juni</th>
    </tr>
</thead>
<tbody>
<?php
    $result = mysqli_query($con,$sel);
    $i = 1;
    while($res = mysqli_fetch_array($result)){
?>  
    <tr>
        <td><?php echo $res['nama_siswa']; ?></td>
        <td><?php echo $res['nama_kelas']; ?></td>
        <td><?php echo $res['Juli']; ?></td>
        <td><?php echo $res['Agustus']; ?></td>
        <td><?php echo $res['September']; ?></td>
        <td><?php echo $res['Oktober']; ?></td>
        <td><?php echo $res['November']; ?></td>
        <td><?php echo $res['Desember']; ?></td>
        <td><?php echo $res['Januari']; ?></td>
        <td><?php echo $res['Februari']; ?></td>
        <td><?php echo $res['Maret']; ?></td>
        <td><?php echo $res['April']; ?></td>
        <td><?php echo $res['Mei']; ?></td>
        <td><?php echo $res['Juni']; ?></td>
    </tr>
<?php
    }
?>