<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    $obj = new stdClass();
    $tipe = "";
    $id_uniq = "select uuid() as id";
    $result = mysqli_query($con,$id_uniq);
    $id_uniq = mysqli_fetch_array($result);
    $id = $id_uniq["id"];

    $_user = isset($_SESSION["m_user_id"]) ? $_SESSION["m_user_id"] :"";
    $_nama = isset($_SESSION['nama']) ? $_SESSION['nama'] : "";
    $_kelas = isset($_SESSION['kelas']) ? $_SESSION['nama'] : "";
    if(isset($_POST['cari'])){
        $id = $_POST['id'];
        $tahun = $_POST['tahun'];
        $sel = "select * from m_siswa where m_siswa_id = '$id'";
        $data1 = json_decode(queryJson($con,$sel),true)['data'];

        $sel2 = "select ts.*,date_format(creatdate,'%Y %M %d %H:%i') tgl,b.nama_kelas from transaksi_spp ts 
        inner join m_kelas b on b.m_kelas_id = ts.kelas
        where m_siswa_id = '$id' and tahun_ajaran = '$tahun' 
        order by seq asc";
        // echo $sel2;
        // return;
        $data2 = json_decode(queryJson($con,$sel2),true)['data'];

        $obj -> dataSiswa = $data1;
        $obj -> dataSPP = $data2;

        $data = json_encode($obj);
        echo $data;
    }
    if(isset($_POST['findOne'])){
        $id = $_POST['id'];
        $sel2 = "select username, m_transaksi_id, bukti,nis,nama_siswa,c.nama_kelas as nama_kelas,bulan,tipe_transaksi,
        status_transaksi,nominal,DATE_FORMAT(a.createdate, '%d-%M-%Y') tgl_bayar,kode_status 
        ,a.approvedby
        from m_transaksi a
        inner join m_siswa b on a.m_siswa_id = b.m_siswa_id
        inner join m_kelas c on c.m_kelas_id = b.m_kelas_id
        left join m_user d on d.m_user_id = a.approvedby where 1=1 and m_transaksi_id = '$id'";

        echo queryJson($con,$sel2);

    }
    if(isset($_POST['hapus'])){
        $id = $_POST['id'];
        $del = "delete from m_transaksi where m_transaksi_id = '$id'";
        $re = mysqli_query($con,$del);
        if($re){
            $upd = "update transaksi_spp set transaksi_id = null, bayar_spp = 0, petugas = null
            where transaksi_id = '$id'";

            $resp = mysqli_query($con,$upd);
            if($resp){
                echo "200";
            }else{
                echo $upd;
            }
        }
    }
    if(isset($_POST['edit'])){
        $bulan = $_POST['bulan'];
        $id = $_POST['id'];

        $sel = "select * from m_transaksi where m_transaksi_id = '$id'";
        $re = mysqli_query($con,$sel);
        $dp = mysqli_fetch_array($re);
        $spp = $dp['harus_bayar'];

        $upd = "update m_transaksi set bulan = '$bulan', nominal = harus_bayar * $bulan, approvedby = '$_nama'
        ,createdate = now() where m_transaksi_id = '$id'";

        mysqli_query($con,$upd);
        // echo $upd;

        $sel = "select transaksi_spp_id from transaksi_spp ts where transaksi_id = '$id'
        order by seq desc limit $bulan";
        $to = mysqli_query($con,$sel);
        while($dp = mysqli_fetch_array($to)){
            $key = $dp['transaksi_spp_id'];

            $updx = "update transaksi_spp set transaksi_id = null, biaya_spp = null,creatdate = null,petugas = null where
            transaksi_spp_id = '$key'";

            mysqli_query($con,$upd);

        }

        echo "200";
    }
    if(isset($_POST['find'])){
        $kelas = $_POST['kelas'];
        $tgl1 = $_POST['tgl1'];
        $tgl2 = $_POST['tgl2'];

        $filter = " and jenis_transaksi = 'BAYAR SPP' ";
        if($kelas !== ""){
            $filter = $filter." and c.m_kelas_id = '$kelas' ";
        }
        if(strlen($tgl1) > 0){
            $filter = $filter." and DATE_FORMAT(a.createdate, '%Y-%m-%d') between '$tgl1' and '$tgl2' ";
        }
        $sel = "select username, m_transaksi_id, bukti,nis,nama_siswa,c.nama_kelas as nama_kelas,bulan,tipe_transaksi,
                        status_transaksi,nominal,DATE_FORMAT(a.createdate, '%d-%M-%Y') tgl_bayar,kode_status 
                        ,a.approvedby
                        from m_transaksi a
                        inner join m_siswa b on a.m_siswa_id = b.m_siswa_id
                        inner join m_kelas c on c.m_kelas_id = b.m_kelas_id
                        left join m_user d on d.m_user_id = a.approvedby where 1=1 $filter";

        echo queryJson($con,$sel);
        // echo $sel;

    }
    if(isset($_POST['tp'])){
        $bulan = $_POST['bulan'];
        $nominal = $_POST['nominal'];
        $nama_file1 = $_FILES['bukti']['name'];
        $ukuran_file1 = $_FILES['bukti']['size'];
        $tipe_file1 = $_FILES['bukti']['type'];
        $tmp_file1 = $_FILES['bukti']['tmp_name'];
        $path1 = "../../asset/bukti_transfer/".$id."/".$nama_file1;
        if( is_dir($path1) === false )
        {
            mkdir("../../asset/bukti_transfer/".$id."/");
        }
        $pathData = "asset/bukti_transfer/".$id."/".$nama_file1;
        // $insert = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
        // ,nominal,kode_status,status_transaksi,createdby,createdate,bukti)
        // values (('$id','BAYAR SPP','ONLINE','$bulan'
        // ,'$nominal','WT1','Menunggu Konfirmasi','$_user',now(),'$pathData') ";

        // echo $insert;
        // echo $pathData;
        if($ukuran_file1 <= 1000000){		
            if(move_uploaded_file($tmp_file1, $path1)){
                $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                ,nominal,kode_status,status_transaksi,createdby,createdate,bukti,m_siswa_id,kelas,tahun_ajaran)
                values ('$id','BAYAR SPP','ONLINE','$bulan'
                ,'$nominal','WT1','Menunggu Konfirmasi','$_user',now(),'$pathData','$_nama','$_kelas','2020/2021') ";
                // echo $upd;
                $result = mysqli_query($con,$upd);
                if($result){
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=formspp'</script>";
                }
            }
        }
    }else if(isset($_GET['id'])){
        $id = $_GET['id'];
        $act = $_GET['act'];
        if($act == "1"){
            $cek_ = "select * from m_user where m_user_id = '$_user'";
            $result = mysqli_query($con,$cek_);
            if(mysqli_num_rows($result) > 0){
                $cek2 = "select * from m_transaksi where m_transaksi_id = '$id' and kode_status = 'WT1'";
                // echo $cek2;
                $result1 = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result1) > 0){
                    $upd = "update m_transaksi set status_transaksi = 'Approved', kode_status = 'WT2',approvedby = '$_user'
                    ,approvedate = now(),tahun_ajaran = '2020/2021' where m_transaksi_id = '$id'";

                    // echo $upd;
                    $result2 = mysqli_query($con,$upd);
                    if($result2){
                        $data = mysqli_fetch_array($result1);
                        $limit = $data['bulan'];
                        $tahun_ajaran = $data['tahun_ajaran'];
                        $siswa = $data['m_siswa_id'];
                        $sel_spp = "select * from transaksi_spp where transaksi_id is null and m_siswa_id = '$siswa' and tahun_ajaran = '$tahun_ajaran'
                        order by seq limit $limit";
                        // echo $sel_spp;
                        $result3 = mysqli_query($con,$sel_spp);
                        while($dt = mysqli_fetch_array($result3)){
                            $id_trans = $dt['transaksi_spp_id'];
                            $updta_spp = "update transaksi_spp set transaksi_id = '$id',bayar_spp = biaya_spp,petugas = '$_user'
                            ,creatdate = now() where transaksi_spp_id = '$id_trans'";

                            // echo $updta_spp."</br>";
                            $result1 = mysqli_query($con,$updta_spp);
                        }
                        echo "<script>alert('Transaksi Berhasil...')</script>";
                        echo "<script>window.location='../../?go=formspp'</script>";
                    }
                }else{
                    echo "<script>alert('Tidak diizinkan lagi...')</script>";
                    echo "<script>window.location='../../?go=formspp'</script>";
                }
            }else{
                echo "<script>alert('Tidak diizinkan')</script>";
                echo "<script>window.location='../../?go='</script>";
            }
        }else if($act == "0"){
                
            $cek_ = "select * from m_user where m_user_id = '$_user'";
            $result = mysqli_query($con,$cek_);
            if(mysqli_num_rows($result) > 0){
                $cek2 = "select * from m_transaksi where m_transaksi_id = '$id' and kode_status = 'WT1'";
                // echo $cek2;
                $result1 = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result1) > 0){
                    // echo "okeeeeeeeee";
                    $upd = "update m_transaksi set status_transaksi = 'Rejected', kode_status = 'WT0',approvedby = '$_user'
                    ,approvedate = now(),tahun_ajaran = '2020/2021' where m_transaksi_id = '$id'";

                    // echo $upd;
                    $result2 = mysqli_query($con,$upd);
                    if($result2){
                        echo "<script>alert('Reject Berhasil...')</script>";
                        echo "<script>window.location='../../?go=formspp'</script>";
                    }
                }else{
                    echo "<script>alert('Tidak diizinkan lagi...')</script>";
                    echo "<script>window.location='../../?go=formspp'</script>";
                }
            }else{
                echo "<script>alert('Tidak diizinkan lagi...')</script>";
                echo "<script>window.location='../../?go=formspp'</script>";
            }
        }
    }
    if(isset($_POST['bayar_spp'])){
            $nis = $_POST['siswa_id'];
            $bulan = $_POST['bulan'];
            $nominal = $_POST['nominal'];
            $tahun_ajaran = $_POST['tahun_ajaran'];
            $kelas = $_POST['kelas'];

            
            $total = $nominal * $bulan;

           
            $selo = "select uuid() as idx";
            $trx = mysqli_query($con,$selo);
            $dp = mysqli_fetch_array($trx);
            $id_transaksi = $dp['idx'];
            $insert = "insert into m_transaksi (m_siswa_id,m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
            ,nominal,kode_status,status_transaksi,createdby,approvedby,createdate,kelas,tahun_ajaran,harus_bayar)
            values ('$nis','$id_transaksi','BAYAR SPP','OFFLINE','$bulan'
            ,'$total','WT2','Approved','$nis','$_nama',now(),'$kelas','$tahun_ajaran','$nominal') ";


            // echo $insert;
            // echo $nominal."  -  ".$total; 
            // exit;
            $results = mysqli_query($con,$insert);
            if($results){
                $sel_spp = "select * from transaksi_spp where transaksi_id is null and m_siswa_id = '$nis'
                and tahun_ajaran = '$tahun_ajaran'
                order by seq limit $bulan";
                $result3 = mysqli_query($con,$sel_spp);
                try {
                    while($dt = mysqli_fetch_array($result3)){
                        $id_trans = $dt['transaksi_spp_id'];
                        $updta_spp = "update transaksi_spp set transaksi_id = '$id_transaksi',bayar_spp = biaya_spp,petugas = '$_user'
                        ,creatdate = now() where transaksi_spp_id = '$id_trans'";
    
                        $result1 = mysqli_query($con,$updta_spp);
                    }
                    echo "200";
                } catch (\Throwable $th) {
                    echo "500";
                }
            }
    }
    // cek
    if(isset($_POST['tp'])){
        if($_POST['tp'] == "cek"){
            $nis = $_POST['nis'];
            $sel  = "select * from m_siswa a inner join m_kelas b on a.m_kelas_id = b.m_kelas_id where a.nis = '$nis'";
            $res = mysqli_query($con,$sel);
            if(mysqli_num_rows($res) > 0){
                $dt = mysqli_fetch_array($res);
                $id = $dt['m_siswa_id'];
                $thn = $dt['tahun_ajaran'];
                $sel = "select *,DATE_FORMAT(creatdate, '%d-%M-%Y')crt from transaksi_spp where m_siswa_id = '$id' and tahun_ajaran = '2020/2021'";
                $results = mysqli_query($con,$sel);
                $out = "";
                $i = 1;
                while($rsx = mysqli_fetch_array($results)){
                    if($rsx['bayar_spp'] == "0"){
                        $color = "background-color:#BF202A; color:white";
                    }else{
                        $color = "background-color:#106DCA; color:#ffff";
                    }

                    $out = $out. "<tr style= 'color:white;".$color."' >
                        <td>". $i."</td>
                        <td>".$rsx['bulan']."</td>
                        <td>". number_format($rsx['biaya_spp'])."</td>
                        <td>". number_format($rsx['bayar_spp'])." </td>
                        <td>". $rsx['crt']."</td>
                    </tr>";
                $i += 1;
                }
                $data ->nis        = $dt['nis'];
                $data ->kelas      = $dt['nama_kelas'];
                $data ->nama_siswa = $dt['nama_siswa'];
                $data ->nama_kelas = $dt['nama_kelas'];
                $data ->spp         = $dt['biaya_spp'];
                $data ->table = $out;
                $data = json_encode($data);
                echo $data;
            }else{
                echo "0";
            }
           
        }else if($_POST['tp'] == "spp_manual"){
            // echo "pxpxp";
            $nis = $_POST['nis'];
            $bulan = $_POST['bulan'];
            $nominal = $_POST['nominal'];
            $tahun_ajaran = "2020/2021";
            // echo $nominal;
            $cekid = "select * from m_siswa where nis = '$nis'";
            $results1 = mysqli_query($con,$cekid);
            $dt = mysqli_fetch_array($results1);
            $id_siswa = $dt['m_siswa_id'];

            $insert = "insert into m_transaksi (m_siswa_id,m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
            ,nominal,kode_status,status_transaksi,createdby,approvedby,createdate)
            values ('$id_siswa','$id','BAYAR SPP','OFFLINE','$bulan'
            ,'$nominal','WT2','Approved','$id_siswa','$_nama',now()) ";

            // echo $insert;
            $results = mysqli_query($con,$insert);
            if($results){
                $sel_spp = "select * from transaksi_spp where transaksi_id is null and m_siswa_id = '$id_siswa'
                and tahun_ajaran = '$tahun_ajaran'
                order by seq limit $bulan";
                // echo $sel_spp;
                $result3 = mysqli_query($con,$sel_spp);
                try {
                    while($dt = mysqli_fetch_array($result3)){
                        $id_trans = $dt['transaksi_spp_id'];
                        $updta_spp = "update transaksi_spp set transaksi_id = '$id',bayar_spp = biaya_spp,petugas = '$_user'
                        ,creatdate = now() where transaksi_spp_id = '$id_trans'";
    
                        // echo $updta_spp."</br>";
                        $result1 = mysqli_query($con,$updta_spp);
                    }
                    echo $id;
                } catch (\Throwable $th) {
                    //throw $th;
                    echo "500";
                }
                
            }
        }
    }

?>