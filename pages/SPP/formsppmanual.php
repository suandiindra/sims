<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <div class="row" style="margin-top:-35px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi Data Siswa
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                        <table class="col-md-12" style="margin-top:10px; padding:20px">
                            <tr >
                              <input type="hidden" name="tp" value="trf-spp">
                                <td>
                                    <!-- <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">NAMA</label>
                                        <input class="form-control" id="nama" type="text" readonly value= "">
                                    </div> -->
                                    <div class="mb-3">
                                        <label for="inputPassword5" require class="form-label">Nama Siswa</label>
                                        <br>
                                        <select style="width:100%" id="nama" class="kodebrg">
                                            <option value=""></option>
                                        <?php
                                            $del = "select * from m_siswa where isactive = 1";
                                            $res = mysqli_query($con,$del);
                                            while($rs = mysqli_fetch_array($res))
                                            {
                                        ?>
                                            <option value="<?php echo $rs['m_siswa_id'] ?>"><?php echo $rs['nama_siswa'] ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Tahun Ajaran</label>
                                        <select name="" class="form-control" id="tahun_ajaran"  onchange="cariSpp(this)">
                                            <option value=""></option>
                                            <?php
                                                $del = "select * from tahun t order by isactive desc";
                                                $res = mysqli_query($con,$del);
                                                while($rs = mysqli_fetch_array($res))
                                                {
                                            ?>
                                                <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">NIS</label>
                                        <input readonly class="form-control" id="nis" type="text" value= "">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">NISN</label>
                                        <input readonly class="form-control" id="nisn" type="text" value= "">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Kelas</label>
                                        <input class="form-control" id="kelas" type="text" readonly value= "">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">Nominal</label>
                                        <input readonly class="form-control" require type="number" id="nominal" name="nominal">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Bulan Bayar</label>
                                        <input class="form-control" require type="number" id ="bulan" name="bulan">
                                    </div>
                                    
                                </td>
                            
                            </tr>
                            
                        </table>
                        <Button class="btn btn-warning" style="margin-top:10px" onclick="konfirmasi(this)" id="btnkonfirmasi">
                            Konfirmasi
                        </Button>
                    </col>
                </Row>
            </div>
            

            <table class="table align-items-center table-flush table-hover" id="dataTableHover" style="margin-top:20px">
                <thead class="thead-light">
                <tr>
                    <th>No</th>
                    <th>Bulan</th>
                    <th>Biaya SPP</th>
                    <th>Jumlah Bayar</th>
                    <th>Tgl Bayar</th>
                </tr>
                </thead>
                <tbody id="partialdo">
                </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>

<script>
    function cariSpp(v){
        let tahun = v.value
        let id = $(`#nama`).val();
        $.ajax({
            type:'POST',
            url:'pages/SPP/actionspp.php',
            data:{
                cari : true,
                id : id,
                tahun : tahun
            },
            success:function(data){
                // console.log(data);
                // return;
                let obj = JSON.parse(data)
                let datasiswa = obj.dataSiswa[0]
                $(`#nis`).val(datasiswa.nis)
                $(`#nisn`).val(datasiswa.nisn)
                
                $(`#nominal`).val("")
                $(`#kelas`).val("")

                let spp = obj.dataSPP
                let dom =  ``
                for(let x = 0; x<spp.length; x++){
                    $(`#kelas`).val(spp[0].nama_kelas)
                    $(`#nominal`).val(duit(spp[0].biaya_spp))

                    let cls = ``
                    if(spp[x].bayar_spp > 0){
                        cls = ` bg-success text-white `
                    }
                    dom = dom + `<tr class="${cls}">
                        <td>${x+1}</td>
                        <td>${spp[x].bulan}</td>
                        <td>${duit(spp[x].biaya_spp)}</td>
                        <td>${duit(spp[x].bayar_spp)}</td>
                        <td>${spp[x].bayar_spp > 0 ? spp[x].tgl:""}</td>
                    </tr>`
                }
                $('#partialdo').html(dom);
                
            }
        })


    }
    $(".kodebrg").chosen();

    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');
        return ribuan
    }
    function getData(){
        var nis = document.getElementById("nis").value;
        // console.log(nis)
        $.ajax({
            type:'POST',
            url:'pages/SPP/actionspp.php',
            data:'tp=cek&nis='+nis,
            success:function(data){
                console.log(data);
                // var obj = Object.keys(data)
                var obj = $.parseJSON(data);
                if(data !== "0"){
                    var nama = obj['nama_siswa'];
                    var kelas = obj['kelas'];
                    var tbl = obj['table']

                    document.getElementById("nama").value = nama
                    document.getElementById("kelas").value = kelas
                    $('#partialdo').html(tbl);
                }
            
            }
        }); 
    }

    function konfirmasi(v){
        $(`#${v.id}`).html('Please Wait...');
        $(`#${v.id}`).attr('disabled','disabled');

        var nis = document.getElementById("nis").value;
        let id = $(`#nama`).val();
        var bulan = document.getElementById("bulan").value;
        var nominal = document.getElementById("nominal").value;
        if(nominal.length == 0){
            alert('Belum bisa..')
            window.location = "./?go=formspp"
            return
        }
        nominal = nominal.replace(/\./g,"")
        
        if(nominal > 0){
            $.ajax({
            type:'POST',
            url:'pages/SPP/actionspp.php',
            data : {
                bayar_spp : true,
                siswa_id : id,
                tahun_ajaran : $(`#tahun_ajaran`).val(),
                bulan : bulan,
                nominal : nominal,
                kelas : $(`#kelas`).val()
            },
            success:function(data){
                console.log(data);
                // return;
                    // var obj = Object.keys(data)
                    if(data == "500"){
                        alert("Gagal...")
                        window.location = "./?go=formspp"
                    }else{
                        alert(`Berhasil`)
                        window.location = "./?go=formspp"
                    }
                }
            }); 
        }
    }
</script>