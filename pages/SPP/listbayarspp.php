<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Pembayaran SPP Siswa</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data Pembayaran SPP Siswa</li>
    </ol>
</div>
      <div class="card">
          <div class="card-body">
            <div class="container col-12" style="margin-top:0px">
            <div class="row" style="margin-bottom:20px">
                <div class="col col-md-3">
                    <label for="inputPassword5" class="form-label">Tgl Awal</label>
                    <input type="date" name="" id="tgl1" class="form-control">
                </div>
                <div class="col col-md-3">
                    <label for="inputPassword5" class="form-label">Tgl Akhir</label>
                    <input type="date" name="" id="tgl2" class="form-control">
                </div>
                <div class="col col-md-3">
                    <label style="<?php echo $disp ?>" for="inputPassword5" class="form-label">kelas</label>
                    <select style="<?php echo $disp ?>" class="form-control" id="kelas">
                        <option value="">Semua</option>
                    <?php
                        $del = "select * from m_kelas where isactive = 1 order by nama_kelas asc";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="col col-md-3" style="margin-top:32px">
                    <Button class="btn btn-success" name="lihat" onclick="lihat()">Lihat</Button>
                    <?php
                        if($_SESSION['role'] !== "siswa")
                        {
                    ?>
                            <a href="./?go=inputspp">
                                <Button class="btn btn-warning" style="float:right" id="addspp" >Input SPP</Button>
                            </a>
                            <Button class="btn btn-primary" onclick="exp()" style="float:right" id="exportspp" >Export</Button>
                    <?php } ?>
                </div>
            </div>
            </div>
        </div>
      </div>
<div class="row" style="margin-top:10px">
    <div class="col">
        <p><b><h4 id="total">Total Uang Masuk :</h4></b></p>
    </div>
</div>
<div class="row" style="margin-top:10px">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="tblspp" style="margin-top:20px">
                <thead class="thead-light">
                <tr>
                        <th>Aksi</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Biaya Bulan SPP</th>
                        <th>Nominal</th>
                        <th>Tgl Bayar</th>
                        <th>Metode bayar</th>
                        <!-- <th>Bukti</th> -->
                        <th>Status</th>
                        <th>Petugas</th>
                        <!-- <th>Action</th> -->
                </tr>
                </thead>
                <tbody id="sppdetail">
                </tbody>
            </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->
        <div class="modal fade" id="mdlspp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran SPP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label for="formFileMultiple" class="form-label">NAMA</label>
                    <input class="form-control" id="nama" type="text" readonly >
                    <input class="form-control" id="keys" type="hidden" >
                </div>
                <div class="mb-3">
                    <label for="formFileDisabled" class="form-label">TGL BAYAR</label>
                    <input class="form-control" id="tgl" type="text" readonly>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Bulan Bayar</label>
                    <input class="form-control" type="number" id ="bulan_bayar" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="hapusSpp()">Hapus</button>
                <button type="button" class="btn btn-primary" onclick="editSpp()">Edit</button>
            </div>
            </div>
        </div>
    </div>
<script>

window.addEventListener('load', lihat, false);
function lihat(){
    let role = "<?php echo $_SESSION['role']?>"
    if(role !== "Tata Usaha"){
        $(`#addspp`).css('display','none')
    }
    $.ajax({
        type:'POST',
        url:'pages/SPP/actionspp.php',
        data:{
            find:true,
            kelas : $(`#kelas`).val(),
            tgl1 : $(`#tgl1`).val(),
            tgl2 : $(`#tgl2`).val()
        },
        success: function(data){
            // console.log(data);
            // return
            let obj = JSON.parse(data)
            var table = $('#tblspp').DataTable();
            table.clear().destroy(); 
            obj = obj.data
            let dom = ``
            let uang = 0
            for(let i = 0; i < obj.length; i++){
                dom = dom + `<tr>
                    <td><Button class="btn-${role !== "Mgr Tata Usaha" ? "secondary" : "primary"}" ${role !== "Mgr Tata Usaha" ? "disabled" : "enabled"}  onclick="aksi('${obj[i].m_transaksi_id}')">Action</button></td>
                    <td>${obj[i].nis}</td>
                    <td>${obj[i].nama_siswa}</td>
                    <td>${obj[i].nama_kelas}</td>
                    <td>${duit(obj[i].nominal / obj[i].bulan)}</td>
                    <td>${duit(obj[i].nominal)}</td>
                    <td>${obj[i].tgl_bayar}</td>
                    <td>${obj[i].tipe_transaksi}</td>
                    <td>${obj[i].status_transaksi}</td>
                    <td>${obj[i].approvedby}</td>
                </tr>`
                uang = uang + parseFloat(obj[i].nominal)
            }
            $(`#sppdetail`).html(dom)
            $('#tblspp').DataTable(({ 
                "destroy": true, //use for reinitialize datatable
            }));
            $(`#total`).html(`Total Uang masuk : ${duit(uang)}`)
            // console.log(dom);
        }
    })
}
var bayar_bulan  = 0;
function duit(v){
    var 	bilangan = v;
    var	reverse = bilangan.toString().split('').reverse().join(''),
        ribuan 	= reverse.match(/\d{1,3}/g);
        ribuan	= ribuan.join('.').split('').reverse().join('');
    return ribuan
}
function aksi(id){
    console.log(id);
    $.ajax({
        type:'POST',
        url:'pages/SPP/actionspp.php',
        data:{
            findOne:true,
            id : id
        },
        success: function(data){
            let obj = JSON.parse(data)
            obj = obj.data[0]
            console.log(obj);
            let nilai = parseFloat(obj.nominal) / parseFloat(obj.bulan)
            $(`#nama`).val(obj.nama_siswa)
            $(`#keys`).val(obj.m_transaksi_id)
            $(`#tgl`).val(obj.tgl_bayar)
            $(`#bulan_bayar`).val(obj.bulan)
            bayar_bulan = obj.bulan
            $(`#mdlspp`).modal('show')
        }
    })
}
function confirmationDelete(anchor){
    var conf = confirm('Apakah yakin melakukan proses ini ???');
    if(conf)
        window.location=anchor.attr("href");
}
function hapusSpp(){
    if(!confirm('Yahin Melanjutkan transaksi ini.. ?')){
        return
    }
    let id = $(`#keys`).val() 
    let bulan_bayar = $(`#bulan_bayar`).val() 
    $.ajax({
        type:'POST',
        url:'pages/SPP/actionspp.php',
        data:{
            hapus:true,
            id : id
        },
        success: function(data){
            console.log(data);
            if(data == "200"){
                alert('Berhasil')
                window.location = "./?go=formspp"
            }
        }
    })
}
function editSpp(){
    let nom_bulan = $(`#bulan_bayar`).val()
    if(nom_bulan == 0){
        alert(`Tidak boleh 0, Jika salah silahkan hapus saja`)
        return
    }
    if(nom_bulan > bayar_bulan){
        alert(`Tidak boleh lebih dari sebelumnya, Jika ingin lebih , lakukan transaksi baru saja..`)
        return
    }
    if(!confirm('Yahin Melanjutkan transaksi ini.. ?')){
        return
    }
    let id = $(`#keys`).val() 
    $.ajax({
        type:'POST',
        url:'pages/SPP/actionspp.php',
        data:{
            edit:true,
            id : id,
            bulan : nom_bulan
        },
        success: function(data){
            console.log(data);
            if(data == "200"){
                alert('Berhasil')
                window.location = "./?go=formspp"
            }
        }
    })
}
function exp(){
    window.location="./pages/SPP/exportspp.php";
}
</script>