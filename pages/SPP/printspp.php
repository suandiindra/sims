<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<style>
    @print { 
        @page :footer { 
            display: none
        } 
    
        @page :header { 
            display: none
        } 
    }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body onload="window.print()">
<div class="d-flex flex-row justify-content-center align-items-center" style="height: 70px;">
    <div class="p-2">
        <h4>VIRTUAL CHECK MATERIAL</h4>
    </div>
</div>
<?php
    include("../../utility/config.php");
    $id = $_GET['id'];
    
    $res = mysqli_query($con,$sel);
    $dt = mysqli_fetch_array($res);
?>
<div style="float:right">
    FRM-WH-001 : Rev 04
</div>
<b style="margin-top:-120px"> PT. CHEMICO SURABAYA</b>
<div class="d-flex flex-row align-items-left" >
    <img src="../../img/chemico.jpeg" style="width:130px;margin-top:0px; margin-left:20px;height:100px" alt="">
</div>
<div>
<center><span >Number : <?php echo $dt['visual_check_no'] ?></span></center>
</div>
<br>
<!-- <hr> -->
<div class="form-row">
    <div class="col col-md-3">Date : <u> <?php echo $dt['tgl_gr']."__" ?> </u></div>
    <div class="col">Time : <u> <?php echo $dt['tgl_gr']."__" ?></u></div>
</div>
<div class="form-row">
    <div class="col">
        <table>
            <tr>
                <td>Supplier Name</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><u>Indonesia</u></td>
            </tr>
            <tr>
                <td>PO Number</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><u><?php echo 12345 ?></u></td>
            </tr>
        </table>
    </div>
</div>
    <div class="form-row" style="margin-top:25px">
        <table class="table table-bordered">
        <thead>
            <tr>
            <th scope="col">No</th>
            <th scope="col">V/X</th>
            <th scope="col">Description</th>
            <th scope="col">BATCH</th>
            <th scope="col">EXPIRED</th>
            <th scope="col">NO PACK</th>
            <th scope="col">PACKING SIZE</th>
            <th scope="col">QTY(Kg)</th>
            <th scope="col">Kemassan</th>
            <th scope="col">Remarks</th>
            </tr>
        </thead>
        </table> 
    </div>
</body>