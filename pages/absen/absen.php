<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
    <!-- <link href="../../css/ruang-admin.min.css" rel="stylesheet"> -->
  </head>
  <body style="background-image: url('../../img/bgr.jpeg'); background-size: cover;">
    <div class="row justify-content-center" style="margin-top:100px;">
        <div class="card" style="width:1400px; height:700px">
            <div class="card-body">
                <div class="row">
                    <div class="col col-md-5" id="lead">
                        <div class="d-flex justify-content-center" id="utama" style="display:none">
                            <img src="" id="muka" style="width:600px; height:450px; margin-top:140px;margin-left:50px;display:none" alt="" />
                        </div> 
                        <!-- slide -->
                        <div class="d-flex justify-content-center" >
                                <div id="carouselExampleFade" style="width:600px; height:450px; margin-top:140px;margin-left:50px; display:none" class="carousel slide carousel-fade" data-bs-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                        <img src="../../img/logo/pta.jpg" style="width:600px; height:400px" class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                        <img src="../../img/logo/poto1.jpeg" style="width:600px; height:430px" class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                        <img src="../../img/logo/poto2.jpeg" style="width:600px; height:430px  " class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                        <img src="../../img/logo/poto3.jpeg" style="width:600px; height:430px  " class="d-block w-100" alt="...">
                                        </div>
                                    </div>
                                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                    </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                </div>
                        </div>
                        <!-- slide -->
                    </div>
                    <div class="col col-md-7">
                        <div id="isi" style="padding-top:20px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../../js/ruang-admin.min.js"></script>
    <!-- Page level plugins -->
    <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        let nomor = "";
        var intervalID;
        window.onload = function() {
            $('#sliding').hide();
            $.ajax({
                typxe:'GET',
                url:'../../pages/absen/absen_be.php',
                data:{
                    scan : true,
                    id : "s"
                },
                success: function(data){
                    console.log("ooo",data);
                }
            })

            reset()
            document.onkeypress = function(e){
                var key = e
                if(e.key == "Enter" || (nomor.length == 10)){
                    reset()
                    scan(nomor)
                }else{
                    nomor = nomor+e.key
                }    
            };
        }
        function scan(v){
            console.log('scan');
            try {
                nomor = "";
                $.ajax({
                    typxe:'GET',
                    url:'absen_be.php',
                    data:{
                        scan : true,
                        id : v
                    },
                    success: function(data){
                        console.log(data);
                        let obj = JSON.parse(data)
                        obj = obj.data[0]
                        console.log(obj);

                        let domz = `<div class="text-center">
                                    <b>SELAMAT DATANG</b>
                                </div>
                                <div class="text-center">
                                    <b style="font-size:40px">YAYASAN AL-HASYIM SA'ADATUL ALAM</b><br>
                                    <b style="font-size:20px">SDIT PELITA ALAM</b>
                                </div>
                                <hr>`
            
                        let dom = domz+`<div class="form-group" style="margin-top:10">
                                            <div class="text-left">
                                                <b style="font-size:21px">NAMA SISWA</b><br>
                                                <b class="text-primary" style="font-size:40px">${obj.nama_siswa}</b>
                                            </div>
                                            <div class="text-left">
                                                <b style="font-size:21px">TEMPAT LAHIR</b><br>
                                                <b class="text-primary" style="font-size:40px">${obj.tempat_lahir}</b>
                                            </div>
                                            <div class="text-left">
                                                <b style="font-size:21px">TGL LAHIR</b><br>
                                                <b class="text-primary" style="font-size:40px">${obj.tgl_lahir !== '0000-00-00'? obj.tgl_lahir : obj.tgl_lahir_string}</b>
                                            </div>
                                            <div class="text-center">
                                                
                                                <div class="row">
                                                    <div class="col col-md-6">
                                                        <div class="text-left">
                                                            <b style="font-size:21px">NIS</b><br>
                                                            <b class="text-primary" style="font-size:40px">${obj.nis}</b>
                                                        </div>
                                                    </div>
                                                    <div  class="col col-md-6">
                                                        <div class="text-left">
                                                            <b style="font-size:21px">NISN</b><br>
                                                            <b class="text-primary" style="font-size:40px">${obj.nisn}</b>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col col-md-6">
                                                    <p class="badge bg-success text-white" style="font-size:45px">${obj.jam}</p>
                                                </div>
                                                <div  class="col col-md-6">
                                                    <p class="badge bg-danger text-white" style="font-size:55px; display:${obj.telat == 1 ? 'block' : 'none'}">TERLAMBAT</p>
                                                </div>
                                            </div>
                                        </div>`
                        $(`#isi`).html(dom)
                        $(`#muka`).attr('src',`../../${obj.photo}`)
                        
                        document.getElementById('carouselExampleFade').style.display = "none"
                        document.getElementById('muka').style.display = "block"
                        intervalID = setInterval(function() {
                            reset()
                            console.log('ppp');
                        }, 10000);
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
        function reset(){
            let domz = `<div class="text-center">
                                    <b>SELAMAT DATANG</b>
                                </div>
                                <div class="text-center">
                                    <b style="font-size:35px">YAYASAN AL-HASYIM SA'ADATUL ALAM</b><br>
                                    <b style="font-size:20px">SDIT PELITA ALAM</b>
                                </div>
                                <div class="text-center">
                                    <b style="font-size:30px;" class="text-danger" id="jam"></b>
                                </div>
                                <hr>`
            let isisleads = `<div class="d-flex justify-content-center">
                                <div id="carouselExampleFade" style="width:600px; height:450px; margin-top:140px;margin-left:50px" class="carousel slide carousel-fade" data-bs-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                        <img src="../../img/logo/pta.jpg" style="width:600px; height:450px" class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                        <img src="slide2.png" style="width:600px; height:450px" class="d-block w-100" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                        <img src="slide1.png" style="width:600px; height:450px" class="d-block w-100" alt="...">
                                        </div>
                                    </div>
                                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                    </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                </div>
                        </div>`
            
            // let dom = domz+`<div class="form-group" style="margin-top:50">
            //         <div class="text-center">
            //             <b style="font-size:21px">NAMA SISWA</b><br>
            //             <b class="text-primary" style="font-size:70px">INDRA SUANDI</b>
            //         </div>
            //         <div class="text-center">
            //             <b style="font-size:21px; margin-bottom:-10px">N  I  S</b><br>
            //             <b class="text-primary" style="font-size:70px">100098281</b>
            //         </div>
            //         <hr>
            //         <div class="row">
            //             <div class="col col-md-6">
            //                 <p class="badge bg-success text-white" style="font-size:55px">06:23</p>
            //             </div>
            //             <div  class="col col-md-6">
            //                 <p class="badge bg-danger text-white" style="font-size:55px; display:none">TERLAMBAT</p>
            //             </div>
            //         </div>
            //     </div>`
            document.getElementById('carouselExampleFade').style.display = "block"
            document.getElementById('muka').style.display = "none"

            $(`#isi`).html(domz)
            // $(`#lead`).html(isisleads)
            
            // $(`#isi`).html(domz)
            // $(`#muka`).attr('src','../../asset/siswa/53e610e5-4587-11ed-82ff-fcb3bca9bd65/messi.jpg')
            // $(`#muka`).attr('src','../../img/logo/pta.jpg')
            startTime()
            clearInterval(intervalID);
        }
        function startTime() {
            let today = new Date();
            let h = today.getHours();
            let m = today.getMinutes();
            let s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('jam').innerHTML =  h + ":" + m + ":" + s;
            setTimeout(startTime, 1000);
        }
        function checkTime(i) {
            if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }
    </script>
</html>