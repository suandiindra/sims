<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <ol class="breadcrumb btn-primary text-white">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </div>
    <div class="row" id="kesiswaanView">
        <div class="col col-md-6">
            <div class="card bg-success">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1 text-white">Jumlah Siswa</div>
                            <div class="h4 mb-0 mr-3 font-weight-bold text-white" id="siswa_all">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                            <span class="text-white" id="thn_ajaran">Tahun Ajaran </span>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-users fa-5x text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-6">
            <div class="card  bg-primary">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1 text-white">Jumlah Siswa Hadir</div>
                            <div class="h4 mb-0 mr-3 font-weight-bold text-white" id="siswa_hadir">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                            <span class="text-white" id="hari1"></span>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-5x text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="tuView" style="margin-top:20px">
        <div class="col col-md-3">
            <div class="card">
                <div class="card-header bg-warning">
                   <b class="text-white">Pembayaran SPP</b>
                   <span class="text-white float-right" id="per1"></span>
                </div>
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col col-md-3">
                            <i class="fas fa-credit-card fa-4x text-danger"></i>
                        </div>
                        <div class="col col-md-3">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-primary" id="sppsiswa">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger float-left">Siswa</span>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-primary" id="amountspp">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger">Spp Outstanding</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-3">
            <div class="card">
                <div class="card-header bg-danger">
                   <b class="text-white">Pembayaran Daftar Ulang</b>
                   <span class="text-white float-right"  id="per3"></span>
                </div>
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col col-md-3">
                            <i class="fas fa-credit-card fa-3x text-danger"></i>
                        </div>
                        <div class="col col-md-3">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-primary" id="dusiswa">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger float-left">Siswa</span>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-primary img-fluid" id="amountdu">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger">Daftar Ulang Outstanding</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-3">
            <div class="card">
                <div class="card-header bg-success">
                   <b class="text-white">Pembayaran PPDB</b>
                   <span class="text-white float-right"  id="per2"></span>
                </div>
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col col-md-3">
                            <i class="fas fa-credit-card fa-3x text-success"></i>
                        </div>
                        <div class="col col-md-3">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-primary" id="siswappdb">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger float-left">Siswa</span>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-primary img-fluid" id="amountppdb">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger">PPDB Outstanding</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-3">
            <div class="card">
                <div class="card-header bg-primary">
                   <b class="text-white">Tunggakan Sebelumnya</b>
                   <!-- <span class="text-white float-right">Oct, 2022</span> -->
                </div>
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col col-md-3">
                            <i class="fas fa-credit-card fa-3x text-primary"></i>
                        </div>
                        <div class="col col-md-3">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-danger" id="siswa_utang">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger float-left">Siswa</span>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="h4 mb-0 mr-3 font-weight-bold text-danger img-fluid" id="jumlah_utang">0</div>
                            <div class="mt-2 mb-0 text-muted text-xs">
                                <span class="text-danger">Spp Outstanding</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', loads, false);
    function loads(){
      let role = "<?php echo $_SESSION['role']; ?>"
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); 
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;
        hari = dd + '-' + mm + '-'+yyyy
        thn = yyyy+'-'+mm
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];

        // alert(monthNames[mm-1])
        $(`#per1`).html(monthNames[mm-1]+', '+yyyy.toString())
        $(`#per2`).html(monthNames[mm-1]+', '+yyyy.toString())
        $(`#per3`).html(monthNames[mm-1]+', '+yyyy.toString())

      if(role !== "Kesiswaan"){
        loadTU(thn)
        $(`#kesiswaanView`).css('display','none')
      }else if(role == "Kesiswaan"){
        loadKehadiran(today,hari)
        $(`#tuView`).css('display','none')
      }
    }
    function loadKehadiran(today,hari){
        $.ajax({
            type:'GET',
            url:'pages/kehadiran/kehadiran_be.php',
            data:{
                find : true,
                tgl1 : today,
                tgl2 : today,
                kelas : ''
            },
            success: function(data){
                let obj = JSON.parse(data)
                obj = obj.data
                console.log(obj);
                let hadir = 0
                let thn = ""
                $(`#siswa_all`).html(obj.length)
                $(`#hari1`).html(hari)
                for(let i = 0; i<obj.length; i++){
                    thn = obj[i].tahun_ajaran
                    if(obj[i].period){
                        hadir = hadir + 1
                    }
                }
                $(`#siswa_hadir`).html(hadir)
                $(`#thn_ajaran`).html(`Tahun Ajaran `+thn)


            }
        })
    }
    function loadTU(thn){
        $.ajax({
            type:'POST',
            url:'pages/laporan_tu/laporan_be.php',
            data:{
                find : true,
                kelas_id : "",
                m_siswa_id : "",
                tahun_ajaran : "",
                bulan : thn,
            },
            success: function(data){
                // console.log(data);
                let obj = JSON.parse(data)
                obj = obj.data
                // console.log(obj);

                let muridSPP = 0
                let amountSPP = 0
                let muriddu = 0
                let amountdu = 0
                let muridppdb = 0
                let amountppdb = 0
                let sisabayar = 0
                let muridsisabayar = 0
                for(let i = 0; i<obj.length;i++){
                    let sisaSpp = parseFloat(obj[i].biaya_spp) - parseFloat(obj[i].pembayaran_spp)
                    let du = parseFloat(obj[i].daftar_ulang) - parseFloat(obj[i].pembayaran_du)
                    let ppdb = parseFloat(obj[i].ppdb) - parseFloat(obj[i].pembayaran_ppdb)
                    if(sisaSpp > 0){
                        muridSPP = muridSPP + 1
                        amountSPP = amountSPP + sisaSpp
                    }
                    if(du > 0){
                        muriddu = muriddu + 1
                        amountdu = amountdu + du
                    }
                    if(ppdb > 0){
                        muridppdb = muridppdb + 1
                        amountppdb = amountppdb + ppdb
                    }
                    if(obj[i].sisa > 0){
                        sisabayar = parseFloat(sisabayar) + parseFloat(obj[i].sisa)
                        muridsisabayar = muridsisabayar + 1
                    }
                }
                $(`#sppsiswa`).html(muridSPP)
                $(`#amountspp`).html(duit(amountSPP))

                $(`#dusiswa`).html(muriddu)
                $(`#amountdu`).html(duit(amountdu))

                $(`#siswappdb`).html(muridppdb)
                $(`#amountppdb`).html(duit(amountppdb))

                $(`#siswa_utang`).html(duit(muridsisabayar))
                $(`#jumlah_utang`).html(duit(sisabayar))
                // return
            }
        })
    }
    function duit(v){
        // return v
        console.log(v);
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        // if(parseFloat(v) < 0){
        //     return `+ ${ribuan}`
        // }
        return `${ribuan}`
    }
</script>