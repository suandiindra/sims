<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Siswa</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
        </ol>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <strong>DATA SISWA</strong>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">Nama Siswa</label>
                            <input type="text" readonly  id="nama_siswa" class="form-control  " >
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">NIS</label>
                            <input type="text" readonly  id="nis" class="form-control  " >
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">Kelas</label>
                            <input type="text" readonly  id="kelas" class="form-control  " >
                        </div>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-header bg-success text-white">
                    <strong>DATA TUNGGAKAN PEMBAYARAN SISWA</strong>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">Tunggakan SPP</label>
                            <input type="text" readonly  id="spp_" class="form-control  " >
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">Tunggakan PPDB</label>
                            <input type="text" readonly  id="ppdb_" class="form-control  " >
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">Tunggakan Daftar Ulang</label>
                            <input type="text" readonly  id="du_" class="form-control  " >
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">Tunggakan Sebelumnya</label>
                            <input type="text" readonly  id="sisa_" class="form-control  " >
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', loads, false);
    function loads(){
        console.log('asdasda');
        $(`#nama_siswa`).val("<?php echo $_SESSION['nama'] ?>")
        $(`#nis`).val("<?php echo $_SESSION['nis'] ?>")
        $(`#kelas`).val("<?php echo $_SESSION['kelas'] ?>")

        $.ajax({
            type:'POST',
            url:'pages/laporan_tu/laporan_be.php',
            data:{
                find : true,
                kelas_id : "<?php echo $_SESSION['m_kelas_id'] ?>",
                m_siswa_id : "<?php echo $_SESSION['m_user_id'] ?>",
                tahun_ajaran : "",
                bulan : "",
            },
            success: function(data){
                // console.log(data);
                let objek = JSON.parse(data)
                objek = objek.data
                let obj = objek[0]
                console.log(obj);
                $(`#spp_`).val(duit(parseFloat(obj.biaya_spp) - parseFloat(obj.pembayaran_spp)))
                $(`#ppdb_`).val(duit(parseFloat(obj.ppdb) - parseFloat(obj.pembayaran_ppdb)))
                $(`#du_`).val(duit(obj.daftar_ulang - obj.pembayaran_du))
                $(`#sisa_`).val(duit(obj.sisa))
            }
        })

    function duit(v){
        // return v
        console.log(v);
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        if(parseFloat(v) < 0){
            return `+ ${ribuan}`
        }
        return `- ${ribuan}`
    }
    }
</script>