<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Laporan Kehadiran</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan Kehadiran</li>
        </ol>
    </div>
    <div class="card" style="margin-top:-20px">
          <div class="card-body">
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Sampai Bulan</label>
                            <input type="month" class="form-control" id="bulan">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="inputPassword5" class="form-label">Tahun Ajaran</label>
                        <select required class="form-control" id="tahun_ajaran">
                            <option value="">ALL</option>
                        <?php
                            $del = "select * from tahun order by isactive desc";
                            $res = mysqli_query($con,$del);
                            while($rs = mysqli_fetch_array($res))
                            {
                        ?>
                            <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="inputPassword5" class="form-label">kelas</label>
                        <select required class="form-control" id="kelas">
                            <option value="">ALL</option>
                        <?php
                            $del = "select * from m_kelas where isactive = 1 order by nama_kelas asc";
                            $res = mysqli_query($con,$del);
                            while($rs = mysqli_fetch_array($res))
                            {
                        ?>
                            <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-3" style="margin-top:32px">
                        <Button class="btn btn-success" id="btnLihat" onclick="lihat()">Lihat</Button>
                        <Button class="btn btn-primary" onclick="exp()">Export</Button>
                    </div>
                    <!-- <div class="col-md-3" style="margin-top:32px">
                    </div> -->
            </div>
          </div>
    </div>

    <div class="card" style="margin-top:10px">
        <div class="card-body">
        <div class="row" style="margin-bottom:20px">
            <div class="col-md-12">
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="tblrekap">
                        <thead class="thead-light">
                            <th>NIS</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Tahun Ajaran</th>
                            <th>SPP</th>
                            <th>Daftar Ulang</th>
                            <th>PPDB</th>
                            <th>Sisa Tahun Sebelumnya</th>
                        </thead>
                        <tbody id="dtlrekap">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', lihat, false);
    function lihat(){
        $(`#btnLihat`).html('Please Wait...')
        var table = $('#tblrekap').DataTable();
        table.clear().destroy(); 
        $.ajax({
                type:'POST',
                url:'pages/laporan_tu/laporan_be.php',
                data:{
                    find : true,
                    kelas_id : $(`#kelas`).val(),
                    m_siswa_id : "",
                    tahun_ajaran : $(`#tahun_ajaran`).val(),
                    bulan : $(`#bulan`).val(),
                },
                success: function(data){
                    console.log(data);
                    // return
                    $(`#btnLihat`).html('Lihat')
                    let obj = JSON.parse(data)
                    obj = obj.data
                    let dom = ``
                    for(let i = 0; i<obj.length; i++){
                        $(`#tahun_ajaran`).val(obj[0].tahun_ajaran)
                        // console.log(parseFloat(obj[i].daftar_ulang));
                        let spp = parseFloat(obj[i].biaya_spp) - parseFloat(obj[i].pembayaran_spp)
                        let du = parseFloat(obj[i].daftar_ulang) - parseFloat(obj[i].pembayaran_du)
                        let ppdb = parseFloat(obj[i].ppdb) - parseFloat(obj[i].pembayaran_ppdb)
                        // let ly = parseFloat(obj[i].sisa_terbayar) - parseFloat(obj[i].sisa)
                        let ly = parseFloat(obj[i].sisa) - parseFloat(obj[i].sisa_terbayar)
                        console.log(`***`,obj[i].sisa,obj[i].sisa_terbayar,ly);
                        dom = dom + `<tr>
                            <td>${obj[i].nis}</td>
                            <td>${obj[i].nama_siswa}</td>
                            <td>${obj[i].kelas}</td>
                            <td>${obj[i].tahun_ajaran}</td>
                            <td>${spp  == 0 ? '<div class="badge bg-success text-white">LUNAS</div>' : duit(spp)}</td>
                            <td>${du   == 0 ? '<div class="badge bg-success text-white">LUNAS</div></div>' : duit(du)}</td>
                            <td>${ppdb == 0 ? '<div class="badge bg-success text-white">LUNAS' : duit(ppdb)}</td>
                            <td>${ly == 0 ? '<div class="badge bg-success text-white">LUNAS' : duit(ly)}</td>
                        </tr>`
                    }
                    $(`#dtlrekap`).html(dom)
                    $('#tblrekap').DataTable(({ 
                            "destroy": true, //use for reinitialize datatable
                            order: [[2, 'asc']],
                    }));
                }
        })
    }
    function duit(v){
        // return v
        console.log(v);
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        if(parseFloat(v) < 0){
            return `+ ${ribuan}`
        }
        return `- ${ribuan}`
    }
    function exp(){
        console.log('ep');
        let tahun_ajaran = document.getElementById("tahun_ajaran").value
        let bulan = document.getElementById("bulan").value
        let kelas = document.getElementById("kelas").value


        window.location="pages/laporan_tu/laporan_be.php?export=true&tahun_ajaran="+tahun_ajaran+"&bulan="+bulan+"&kelas="+kelas
    }
</script>