<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");

    $obj = new stdClass();

    if(isset($_POST['find'])){
        $tahun_ajaran = $_POST['tahun_ajaran'];
        $m_siswa_id = $_POST['m_siswa_id'];
        $kelas_id = $_POST['kelas_id'];
        $bulan = $_POST['bulan'];

        $sel = "select *,date_format(now(),'%m') as bulan_ from tahun where isactive = 1";
        $cr = mysqli_query($con,$sel);
        $do = mysqli_fetch_array($cr);
        $tahun_ajaran_aktif = $do['tahun_ajaran'];
        $bulan_ = $do['bulan_'];

        if(strlen($bulan) > 0){
            $bulan = substr($bulan,5);
        }else{
            $bulan = $bulan_;
        }
        if((int)$bulan <= 6){
            $bulan = (int)$bulan + 6;
        }else if((int)$bulan > 6){
            $bulan = (int)$bulan - 6;
        }

        $filter = "";
        if(strlen($kelas_id) > 0){
            $filter = " and ms.m_kelas_id = '$kelas_id'";
        }

        if(strlen($m_siswa_id) > 0){
            $filter = $filter." and ms.m_siswa_id = '$m_siswa_id'";
        }
        

        if(strlen($tahun_ajaran) == 0){
            $tahun_ajaran = $tahun_ajaran_aktif;
        }

        // echo $kelas_id." - ".$tahun_ajaran." - ".$bulan;
        // exit;

        $sel = "select ms.m_siswa_id,ms.nis,ms.nama_siswa,ms.kelas
        ,case when ks is not null then coalesce(b.harus_bayar,ms.biaya_spp) * $bulan else 0 end as biaya_spp
        ,coalesce (c.harus_bayar,0) as daftar_ulang
        ,coalesce (ms.ppdb,0) as ppdb
        ,coalesce(pembayaran_spp,0) as pembayaran_spp
        ,coalesce (pembayaran_du,0) as pembayaran_du
        ,coalesce (pembayaran_ppdb,0) as pembayaran_ppdb
        ,coalesce (sisa_bayar ,0) as sisa
        ,coalesce (pembayaran_sisa ,0) as sisa_terbayar
        ,'$tahun_ajaran' as tahun_ajaran
        from m_siswa ms
        left join
	        (select m_siswa_id,tahun_ajaran,harus_bayar ,sum(nominal)as pembayaran_spp from m_transaksi mt where jenis_transaksi = 'BAYAR SPP' and kode_status = 'WT2' and isactive = 1
	        and tahun_ajaran = '$tahun_ajaran'
	        group by m_siswa_id,tahun_ajaran,harus_bayar) b on ms.m_siswa_id = b.m_siswa_id
    	left join 
    		(select m_siswa_id,tahun_ajaran,harus_bayar ,sum(case when kode_status = 'WT2' then nominal else 0 end)as pembayaran_du from m_transaksi mt where jenis_transaksi = 'DAFTAR ULANG' 
    		and isactive = 1
	        and tahun_ajaran = '$tahun_ajaran'
	        group by m_siswa_id,tahun_ajaran,harus_bayar) c on c.m_siswa_id = ms.m_siswa_id 
        left join 
    		(select m_siswa_id,harus_bayar ,sum(nominal)as pembayaran_ppdb from m_transaksi mt where jenis_transaksi = 'PPDB' 
    		and kode_status = 'WT2' and isactive = 1
	        group by m_siswa_id,harus_bayar) d on d.m_siswa_id = ms.m_siswa_id 
        left join 
        	(select m_siswa_id,harus_bayar ,sum(nominal)as pembayaran_sisa from m_transaksi mt where jenis_transaksi = 'LY' 
    		and kode_status = 'WT2' and isactive = 1
	        group by m_siswa_id,harus_bayar) e on e.m_siswa_id = ms.m_siswa_id 
        left join 
            (select tahun_ajaran as ks,m_siswa_id   from transaksi_spp ts where tahun_ajaran = '$tahun_ajaran'
            group by tahun_ajaran,m_siswa_id 
            )f on f.m_siswa_id = ms.m_siswa_id
        where ms.isactive = 1 $filter";

        // echo $sel;

        echo queryJSON($con,$sel);
    }
    if(isset($_GET['export'])){
        $tahun_ajaran   = $_GET['tahun_ajaran'];
        $kelas_id       = $_GET['kelas'];
        $bulan          = $_GET['bulan'];

        $sel = "select *,date_format(now(),'%m') as bulan_ from tahun where isactive = 1";
        $cr = mysqli_query($con,$sel);
        $do = mysqli_fetch_array($cr);
        $tahun_ajaran_aktif = $do['tahun_ajaran'];
        $bulan_ = $do['bulan_'];

        if(strlen($bulan) > 0){
            $bulan = substr($bulan,5);
        }else{
            $bulan = $bulan_;
        }
        if((int)$bulan <= 6){
            $bulan = (int)$bulan + 6;
        }else if((int)$bulan > 6){
            $bulan = (int)$bulan - 6;
        }

        $filter = "";
        if(strlen($kelas_id) > 0){
            $filter = " and ms.m_kelas_id = '$kelas_id'";
        }

        

        if(strlen($tahun_ajaran) == 0){
            $tahun_ajaran = $tahun_ajaran_aktif;
        }

        // echo $kelas_id." - ".$tahun_ajaran." - ".$bulan;
        // exit;

        $sel = "select ms.m_siswa_id,ms.nis,ms.nama_siswa,ms.kelas
        ,case when ks is not null then coalesce(b.harus_bayar,ms.biaya_spp) * $bulan else 0 end as biaya_spp
        ,coalesce (c.harus_bayar,0) as daftar_ulang
        ,coalesce (ms.ppdb,0) as ppdb
        ,coalesce(pembayaran_spp,0) as pembayaran_spp
        ,coalesce (pembayaran_du,0) as pembayaran_du
        ,coalesce (pembayaran_ppdb,0) as pembayaran_ppdb
        ,coalesce (sisa_bayar ,0) as sisa
        ,coalesce (pembayaran_sisa ,0) as sisa_terbayar
        ,'$tahun_ajaran' as tahun_ajaran
        from m_siswa ms
        left join
	        (select m_siswa_id,tahun_ajaran,harus_bayar ,sum(nominal)as pembayaran_spp from m_transaksi mt where jenis_transaksi = 'BAYAR SPP' and kode_status = 'WT2' and isactive = 1
	        and tahun_ajaran = '$tahun_ajaran'
	        group by m_siswa_id,tahun_ajaran,harus_bayar) b on ms.m_siswa_id = b.m_siswa_id
    	left join 
    		(select m_siswa_id,tahun_ajaran,harus_bayar ,sum(case when kode_status = 'WT2' then nominal else 0 end)as pembayaran_du from m_transaksi mt where jenis_transaksi = 'DAFTAR ULANG' 
    		and isactive = 1
	        and tahun_ajaran = '$tahun_ajaran'
	        group by m_siswa_id,tahun_ajaran,harus_bayar) c on c.m_siswa_id = ms.m_siswa_id 
        left join 
    		(select m_siswa_id,harus_bayar ,sum(nominal)as pembayaran_ppdb from m_transaksi mt where jenis_transaksi = 'PPDB' 
    		and kode_status = 'WT2' and isactive = 1
	        group by m_siswa_id,harus_bayar) d on d.m_siswa_id = ms.m_siswa_id 
        left join 
        	(select m_siswa_id,harus_bayar ,sum(nominal)as pembayaran_sisa from m_transaksi mt where jenis_transaksi = 'LY' 
    		and kode_status = 'WT2' and isactive = 1
	        group by m_siswa_id,harus_bayar) e on e.m_siswa_id = ms.m_siswa_id 
        left join 
            (select tahun_ajaran as ks,m_siswa_id   from transaksi_spp ts where tahun_ajaran = '$tahun_ajaran'
            group by tahun_ajaran,m_siswa_id 
            )f on f.m_siswa_id = ms.m_siswa_id
        where ms.isactive = 1 $filter";

        // echo $sel;
        // return;
        $rex = mysqli_query($con,$sel);
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Data Rekap.xls");
    ?>
        <table border="1">
            <tr>
                <td>NISN</td>
                <td>Nama Siswa</td>
                <td>Kelas</td>
                <td>Tunggakan SPP</td>
                <td>Tunggakan Daftar Ulang</td>
                <td>Tunggakan PPDB</td>
                <td>Tunggakan Sebelumnya</td>
            </tr>
    <?php
        while($do = mysqli_fetch_array($rex)){
            $spp = $do['pembayaran_spp'] - $do['biaya_spp'];
            $ppdb = $do['ppdb'] - $do['pembayaran_ppdb'];
    ?>
        <tr>
            <td><?php echo $do['nis']?></td>
            <td><?php echo $do['nama_siswa']?></td>
            <td><?php echo $do['kelas']?></td>
            <td><?php echo $spp ?></td>
            <td><?php echo $do['daftar_ulang']?></td>
            <td><?php echo $ppdb; ?></td>
            <td><?php echo $do['sisa']?></td>
        </tr>
    <?php
        }
    ?>
    </table>
<?php
    }
?>