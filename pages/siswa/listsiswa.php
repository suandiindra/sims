<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Siswa</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Daftar Siswa</li>
    </ol>
</div>
      <div class="card">
          <div class="card-body">
            <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" /> 
            <div class="container col-12" style="margin-top:0px">
            <div class="row" style="margin-bottom:20px">
                <div class="col-sm">
                    <label for="inputPassword5" class="form-label">kelas</label>
                    <select required class="form-control" id="kelas">
                        <option value=""></option>
                    <?php
                        $del = "select * from m_kelas where isactive = 1 order by nama_kelas asc";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="col-sm" style="margin-top:32px">
                    <Button class="btn btn-success col-md-2"  onclick="findsiswa()" >Lihat</Button>
                    <Button class="btn btn-danger float-right col-md-2"  onclick="expot_excel()" >Export</Button>
                </div>
            </div>
            </div>
          </div>
      </div>

<div class="row" style="margin-top:20px">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary float-left"><a href="./?go=addsiswa"> <button class="btn btn-facebook">+ Data Siswa</button></a></h6>
                  <button class="btn btn-primary float-right" onclick="upld()"><i class="fa fa-upload"></i>  Upload Siswa</button>
                </div>
                <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="tbl_siswa">
                    <thead class="thead-light">
                      <tr>
                        <th>NIS</th>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Tgl Lahir</th>
                        <th>Kelas</th>
                        <th>ID Absen</th>
                        <th>Daftar Ulang</th>
                        <th>PPDB</th>
                        <th>SPP</th>
                        <th style="width:200px">Action</th>
                      </tr>
                    </thead>
                    <tbody id="dtl_siswa">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal  -->
        <div class="modal" tabindex="-1" role="dialog" id="mdlupld">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Upload Data Siswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="mb-3">
                    <label for="formFile" class="form-label">Tahun Ajaran</label>
                    <select name="" class="form-control" id="tahun_ajaran" >
                        <option value=""></option>
                        <?php
                            $del = "select * from tahun t order by isactive desc";
                            $res = mysqli_query($con,$del);
                            while($rs = mysqli_fetch_array($res))
                            {
                        ?>
                            <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <input type="file" id="upload" class="form-control"/>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnProses" onclick="pushJson()" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    <script>
      window.addEventListener('load', findsiswa, false);
      document.getElementById('upload').addEventListener('change', handleFileSelect, false);
      var dataSiswa = [];
      let role = "<?php echo $_SESSION['role'] ?>"
      function upld(){
        $(`#mdlupld`).modal(`show`)
      }
      function expot_excel(){
        window.location="pages/siswa/actionsiswa.php?export=true"
      }
      function findsiswa(){
        console.log('asdassa');
        var table = $('#tbl_siswa').DataTable();
        // console.log(role);
        table.clear().destroy(); 
        $.ajax({
            type:'GET',
            url:'pages/siswa/actionsiswa.php',
            data:{
                find : true,
                kelas : $(`#kelas`).val()
            },
            success: function(data){
              // console.log(data);
              // return
              let obj = JSON.parse(data)
              obj = obj.data
              // console.log(obj);
              let dom = ``
              for(let i = 0; i<obj.length; i++){
                dom = dom + `<tr>
                  <td>${obj[i].nis}</td>
                  <td>${obj[i].nisn}</td>
                  <td>${obj[i].nama_siswa}</td>
                  <td>${obj[i].tgl_lahir && obj[i].tgl_lahir == '0000-00-00' ? obj[i].tgl_lahir  : ''}</td>
                  <td>${obj[i].kelas}</td>
                  <td>${obj[i].id_absen ? obj[i].id_absen : ''}</td>
                  <td>${obj[i].daftar_ulang}</td>
                  <td>${obj[i].ppdb ? obj[i].ppdb : 0}</td>
                  <td>${obj[i].biaya_spp}</td>
                  <td><a href='./?go=editsiswa&act=edit&id=${obj[i].m_siswa_id}'><button class="btn btn-primary">Edit</button></a></td>
                </tr>`
              }
              $(`#dtl_siswa`).html(dom)
              $('#tbl_siswa').DataTable(({ 
                      "destroy": true, //use for reinitialize datatable
                      order: [[2, 'asc']],
              }));
            }
          })
      }
      //excel handle
      function handleFileSelect(e) {
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onload = function(e) {
          const data = new Uint8Array(e.target.result);
          const workbook = XLSX.read(data, { type: 'array' });

          // Ambil nama sheet pertama
          const sheetName = workbook.SheetNames[0];

          // Dapatkan sheet yang sesuai dengan nama
          const sheet = workbook.Sheets[sheetName];

          // Konversi sheet menjadi bentuk JSON
          const jsonData = XLSX.utils.sheet_to_json(sheet);

          // Tampilkan data
          dataSiswa = jsonData
          console.log(dataSiswa);
        };

        reader.readAsArrayBuffer(file);
      }

      function pushJson() {
          let data = dataSiswa
          if(data.length == 0){
            alert(`Gaada data`)
            return
          }
          $("#btnProses").attr("disabled", true);
          $("#btnProses").html(`Save Change`)
          $.ajax({
              type: 'POST',
              url: 'pages/siswa/actionsiswa.php',
              data: {
                  upload: true,
                  listData: JSON.stringify(data),
                  tahun : $(`#tahun_ajaran`).val()
              },
              success: function(data) {
                console.log(data);
                if(data == "200"){
                  alert(`Berhasil`)
                  $("#btnProses").attr("disabled", false);
                  $("#btnProses").html(`Save Chane`)
                }
              }
          });
      }
    </script>
    