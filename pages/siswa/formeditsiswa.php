<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Siswa</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $nis = "";
        $alamat = "";
        $nama_kelas = "";
        $hp = "";
        $ppdb = "";
        $daftar_ulang = "";
        $biaya_spp = "";
        $m_kelas_id = "";

        $activeform = "";
        $activebtn = "";

        $tipe = "add";
        $label = "Tambahkan";
        $key = "";
        $password = "";
        $id_agent = "";
        $tahun_ajaran = "";
        if(isset($_GET['id'])){
            $activeform = "";
            $activebtn = " style='display:none' ";
            $nis = $_GET['id'];
            $key = $nis;
            $sel = "select * from m_siswa where m_siswa_id = '$nis'";
            $rs = mysqli_query($con,$sel);
            $th = mysqli_fetch_array($rs);
            $nis = $th['nis'];
            $tahun_ajaran = $th['tahun_ajaran'];
            $nama_lengkap = $th['nama_siswa'];
            $tempat_lahir = $th['tempat_lahir'];
            $tgl_lahir = $th['tgl_lahir'];
            $nama_kelas = $th['kelas'];
            $alamat = $th['alamat'];
            $ppdb = $th['ppdb'];
            $daftar_ulang = $th['daftar_ulang'];
            $biaya_spp = $th['biaya_spp'];
            $m_kelas_id = $th['m_kelas_id'];
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri siswa
            <hr>
            </div>
            <div class="card-body" style="margin-top:-20px">
            <?php
                if($_SESSION['role'] == "siswa"){
            ?>
                    <Form method="POST" action="pages/siswa/actionsiswa.php" style="margin-bottom:30px;">
                        <input type="hidden"  name="tp" value="ganti-pwd" class="form-control">
                        <div class="mb-3">
                        <label for="inputPassword5" class="form-label badge-danger">&nbsp&nbsp Password Lama &nbsp&nbsp </label>
                            <input type="text" required  id="old_pwd" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label badge-danger">&nbsp&nbsp Password Baru &nbsp&nbsp </label>
                            <input type="text" required  id="new_pwd" class="form-control" >
                        </div>

                        <button class="btn btn-success">Konfirmasi Password</button>
                    </form>
            <?php
                }
            ?>
                <input type="hidden"  name="tp" value="edt" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                <div class="row">
                    <div class="col col-md-4">
                        <div class="row">
                            <div class="col">
                                <img src="" id="poto" alt="" srcset="" style="width:250px; height:300px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md-8">
                                <input type="file"  id="filefoto" class="form-control">
                            </div>
                            <div class="col col-md-4">
                                <button class="btn btn-danger" id="updl" onclick="uploadAction()">Upload</button>
                            </div>
                        </div>  
                    </div>
                    <div class="col col-md-8">
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">* Crd ID</label>
                            <input type="text"  required  id="card_id" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">* NIS</label>
                            <input type="text"  required  id="nis" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">* NISN</label>
                            <input type="text"  required  id="nisn" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label">* Nama Siswa</label >
                            <input type="text"  required  id="nama" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" id="nts" class="form-label" style="color:red">Password  (isi jika ingin berubah)</label >
                            <input type="text"   id="password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                    <input type="text"   id="tempat_lahir" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tanggal Lahir</label>
                    <input type="date"   id="tgl_lahir" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat</label>
                    <input type="text"   id="alamat" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" require class="form-label">Tahun Ajaran</label>
                    <select  required class="form-control" id="tahun_ajaran">
                    <?php
                        $del = "select * from tahun where isactive = 1";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">kelas</label>
                    <select  required class="form-control" id="kelas">
                    <?php
                        $del = "select * from m_kelas where isactive = 1";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="mb-3" id="ppdb_form" style="display:none">
                    <label for="inputPassword5" class="form-label">Biaya PPDB</label>
                    <input type="number"   id="ppdb" class="form-control" >
                </div>
                <div class="mb-3" id="daftar_ulang_form" style="display:none">
                    <label for="inputPassword5" class="form-label">Biaya Daftar Ulang</label>
                    <input type="number"   id="daftar_ulang" class="form-control">
                </div>
                
                <div class="mb-3" id="spp_form"  style="display:none">
                    <label for="inputPassword5" class="form-label">Biaya SPP</label>
                    <input type="number" id="spp" class="form-control">
                </div>
                <div class="mb-3" >
                    <label for="inputPassword5" class="form-label">Tunggakan Sebelumnya</label>
                    <input type="number" id="tunggakan" class="form-control">
                </div>
                <Button class="btn btn-success" id="editsiswa" onclick="edit()">
                     Edit Data Siswa
                </Button>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
    // window.onload = function() {
    //     findOne()
    // }
    window.addEventListener('load', findOne, false);

    let role = "<?php echo $_SESSION['role'] ?>"
    function uploadAction(){
        var file_data1 = ``;
        if($("#filefoto").val().length > 0){
            file_data1 = $('#filefoto').prop('files')[0];  
        }else{
            alert('Pilih foto dulu...')
            return
        }
        var form_data = new FormData();      
        form_data.append('editFoto', true);
        form_data.append('id', `<?php echo $_GET['id'] ?>`);
        form_data.append('file1', file_data1);

        console.log(form_data);
        $.ajax({
            url: 'pages/siswa/actionsiswa.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(res){
                console.log(res);
                if(res == "200"){
                    alert("Proses Berhasil..")
                    findOne()
                //     window.location = "./?go=dataklaim"
                }else{
                    console.log(`salah..`);
                    alert(res)
                }
            }
        });
    }
    function edit(){
        $.ajax({
            type:'POST',
            url:'pages/siswa/actionsiswa.php',
            data:{
                tp : 'edt',
                id : `<?php echo $_GET['id'] ?>`,
                nis : $(`#nis`).val(),
                nisn : $(`#nisn`).val(),
                nama : $(`#nama`).val(),
                password : $(`#password`).val(),
                tempat_lahir : $(`#tempat_lahir`).val(),
                tgl_lahir : $(`#tgl_lahir`).val(),
                alamat : $(`#alamat`).val(),
                kelas : $(`#kelas`).val(),
                ppdb : $(`#ppdb`).val(),
                tahun_ajaran : $(`#tahun_ajaran`).val(),
                daftar_ulang : $(`#daftar_ulang`).val(),
                spp : $(`#spp`).val(),
                card_id : $(`#card_id`).val(),
                tunggakan : $(`#tunggakan`).val(),
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                    window.location = './?go=listsiswa'
                }
            }
        })
    }
    function findOne(){
        

        $.ajax({
            type:'GET',
            url:'pages/siswa/actionsiswa.php',
            data:{
                findOne : true,
                id : `<?php echo $_GET['id'] ?>`
            },
            success: function(data){
                // console.log(data);
                let obj = JSON.parse(data)
                obj = obj.data[0]

                console.log(obj);
               $(`#nis`).val(obj.nis)
               $(`#nama`).val(obj.nama_siswa)
               $(`#password`).val(obj.pwd)
               $(`#tempat_lahir`).val(obj.tempat_lahir)
               $(`#tgl_lahir`).val(obj.tgl_lahir)
               $(`#alamat`).val(obj.alamat)
               $(`#tahun_ajaran`).val(obj.tahun_ajaran)
               $(`#kelas`).val(obj.m_kelas_id)

               $(`#ppdb`).val(obj.ppdb)
               $(`#tunggakan`).val(obj.sisa_bayar)
               $(`#daftar_ulang`).val(obj.daftar_ulang)
               $(`#spp`).val(obj.biaya_spp)
               $(`#ppdb_form`).css('display','block')
               $(`#daftar_ulang_form`).css('display','block')
               $(`#spp_form`).css('display','block')

               $(`#nisn`).val(obj.nisn)
               $(`#card_id`).val(obj.id_absen)
               $('#poto').attr('src',`${obj.photo}`);
               
               if(obj.role !== "Mgr Tata Usaha"){
                    $(`#spp`).attr('disabled','disabled')
                    $(`#daftar_ulang`).attr('disabled','disabled')
                    $(`#ppdb`).attr('disabled','disabled')
                    $(`#tunggakan`).attr('disabled','disabled')
               }else{
                    $(`#nis`).attr('disabled','disabled')
                    $(`#nama`).attr('disabled','disabled')
                    $(`#password`).attr('disabled','disabled')
                    $(`#tempat_lahir`).attr('disabled','disabled')
                    $(`#tgl_lahir`).attr('disabled','disabled')
                    $(`#alamat`).attr('disabled','disabled')
                    $(`#kelas`).attr('disabled','disabled')
                    $(`#tahun_ajaran`).attr('disabled','disabled')
                    $(`#card_id`).attr('disabled','disabled')
                    $(`#poto`).attr('disabled','disabled')
                    $(`#nisn`).attr('disabled','disabled')
                    $(`#filefoto`).attr('disabled','disabled')
               }

               if("<?php echo $_SESSION['role']?>" == "siswa"){
                    // alert('sdf')
                    $(`#nis`).attr('disabled','disabled')
                    $(`#nama`).attr('disabled','disabled')
                    $(`#password`).attr('disabled','disabled')
                    $(`#tempat_lahir`).attr('disabled','disabled')
                    $(`#tgl_lahir`).attr('disabled','disabled')
                    $(`#alamat`).attr('disabled','disabled')
                    $(`#kelas`).attr('disabled','disabled')
                    $(`#tahun_ajaran`).attr('disabled','disabled')
                    $(`#card_id`).attr('disabled','disabled')
                    $(`#poto`).attr('disabled','disabled')
                    $(`#nisn`).attr('disabled','disabled')
                    $(`#filefoto`).attr('disabled','disabled')
                    $(`#editsiswa`).css(`display`,`none`)
                    $(`#updl`).css(`display`,`none`)
                    $(`#editsiswa`).html('Password')
               }
               console.log(obj.photo);
            }
        })
    }

</script>