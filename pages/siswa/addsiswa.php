<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Siswa</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Siswa</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $nis = "";
        $alamat = "";
        $nama_kelas = "";
        $hp = "";
        $ppdb = "";
        $daftar_ulang = "";
        $biaya_spp = "";
        $m_kelas_id = "";

        $activeform = "";
        $activebtn = "";
        $card_id = "";
        $tipe = "add";
        $label = "Tambahkan";
        $key = "";
        $password = "";
        $id_agent = "";
        $tahun_ajaran = "";
        $tempat_lahir = "";
        if($_SESSION['role'] == "siswa"){
            $activeform = "disabled";
            $activebtn = " style='display:none' ";
            $nis = $_SESSION['nis'];
            $nama_lengkap = $_SESSION['nama'];
            $tempat_lahir = $_SESSION['tempat_lahir'];
            $tgl_lahir = $_SESSION['tgl_lahir'];
            $nama_kelas = $_SESSION['kelas'];
            $alamat = $_SESSION['alamat'];
            $ppdb = $_SESSION['ppdb'];
            $daftar_ulang = $_SESSION['daftar_ulang'];
            $biaya_spp = $_SESSION['biaya_spp'];
            $m_kelas_id = $_SESSION['m_kelas_id'];
            
            $sel = "select * from m_siswa where nis = '$nis'";
            $rs = mysqli_query($con,$sel);
            $th = mysqli_fetch_array($rs);
            $tahun_ajaran = $th['tahun_ajaran'];
            $card_id = $th['id_absen'];

        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri siswa
            <hr>
            </div>
            <div class="card-body" style="margin-top:-20px">
            <?php
                if($_SESSION['role'] == "siswa"){
            ?>
                    <Form method="POST" action="pages/siswa/actionsiswa.php" style="margin-bottom:30px;">
                        <input type="hidden"  name="tp" value="ganti-pwd" class="form-control">
                        <div class="mb-3">
                        <label for="inputPassword5" class="form-label badge-danger">&nbsp&nbsp Password Lama &nbsp&nbsp </label>
                            <input type="text" required  name="old_pwd" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label badge-danger">&nbsp&nbsp Password Baru &nbsp&nbsp </label>
                            <input type="text" required  name="new_pwd" class="form-control" >
                        </div>

                        <button class="btn btn-success">Konfirmasi Password</button>
                    </form>
            <?php
                }
            ?>

            <Form method="POST" action="pages/siswa/actionsiswa.php" enctype="multipart/form-data">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* CARD ID</label>
                    <input type="text" <?php echo $activeform ?>  name="card_id" id="card_id" class="form-control" value= "<?php echo $card_id ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* NIS</label>
                    <input type="text" <?php echo $activeform ?> required  name="nis" class="form-control" value= "<?php echo $nis ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* NISN</label>
                    <input type="text" <?php echo $activeform ?> required  name="nisn" class="form-control" value= "<?php echo $nis ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Nama Siswa</label >
                    <input type="text" <?php echo $activeform ?> required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                    <input type="text" <?php echo $activeform ?>  name="tempat_lahir" class="form-control" value= "<?php echo $tempat_lahir ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tanggal Lahir</label>
                    <input type="date" <?php echo $activeform ?>  name="tgl_lahir" class="form-control" value= "<?php echo $tgl_lahir ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat</label>
                    <input type="text" <?php echo $activeform ?>  name="alamat" class="form-control" value= "<?php echo $alamat ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" require class="form-label">Tahun Ajaran</label>
                    <select <?php  echo $activeform ?> required class="form-control" name="tahun_ajaran">
                        <option value="<?php echo $tahun_ajaran; ?>"><?php echo $tahun_ajaran; ?></option>
                    <?php
                        $del = "select * from tahun where isactive = 1";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">kelas</label>
                    <select <?php  echo $activeform ?> required class="form-control" name="kelas">
                        <option value="<?php echo $m_kelas_id; ?>"><?php echo $nama_kelas; ?></option>
                    <?php
                        $del = "select * from m_kelas where isactive = 1";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['m_kelas_id'] ?>"><?php echo $rs['nama_kelas'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Foto Siswa</label>
                    <input type="file" name="photo" class="form-control" >
                </div>
                <div class="mb-3" >
                    <label for="inputPassword5" class="form-label">Biaya PPDB</label>
                    <input type="number" <?php echo $activeform ?>  name="ppdb" class="form-control" value= "<?php echo $ppdb ?>">
                </div>
                <div class="mb-3" >
                    <label for="inputPassword5" class="form-label">Biaya Daftar Ulang</label>
                    <input type="number" <?php echo $activeform ?>  name="daftar_ulang" class="form-control" value= "<?php echo $daftar_ulang ?>">
                </div>
                
                <div class="mb-3" >
                    <label for="inputPassword5" class="form-label">Biaya SPP</label>
                    <input type="number" <?php echo $activeform ?>  name="spp" class="form-control" value= "<?php echo $biaya_spp ?>">
                </div>
                <div class="mb-3" >
                    <label for="inputPassword5" class="form-label">Sisa Tunggakan Sebelumnya</label>
                    <input type="number" <?php echo $activeform ?>  name="sisa" class="form-control" value= "<?php echo $biaya_spp ?>">
                </div>
                <Button class="btn btn-success" <?php echo $activebtn; ?>>
                        <?php echo $label; ?>
                </Button>
                
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
    window.addEventListener('load', loads, false);

    function loads(){
        $("#card_id").focus();
    }
</script>