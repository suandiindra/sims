<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    $tipe = "";
    $id_uniq = "select uuid() as id";
    $result = mysqli_query($con,$id_uniq);
    $id_uniq = mysqli_fetch_array($result);
    $id_uniq = $id_uniq["id"];
    $role = $_SESSION['role'];

    // echo "asd";
    $_user = $_SESSION["m_user_id"];
    $_nama = $_SESSION['nama'];

    if(isset($_GET['export'])){
        $sel = "select m_siswa_id,nis,nisn,nama_siswa,kelas,tahun_ajaran ,biaya_spp,daftar_ulang,ppdb
        ,sisa_bayar, coalesce(is_lulus,'N') as finish
        from m_siswa ms where isactive  = 1
        order by nama_siswa asc";
        $res = mysqli_query($con,$sel);
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Data Siswa.xls");
    ?>
        <Table border="1">
            <tr>
                <td>Key_id</td>
                <td>NIS</td>
                <td>NISN</td>
                <td>Nama Siswa</td>
                <td>Kelas</td>
                <td>Tahun Ajaran</td>
                <td>Biaya SPP</td>
                <td>Biaya DU</td>
                <td>PPDB</td>
                <td>Tunggakan Sebelumnya</td>
                <td>Lulus</td>
            </tr>
       
    <?php
        while($do = mysqli_fetch_array($res)){
    ?>
        <tr>
            <td><?php echo $do['m_siswa_id'] ?></td>
            <td><?php echo "'".$do['nis'] ?></td>
            <td><?php echo "'".$do['nisn'] ?></td>
            <td><?php echo $do['nama_siswa'] ?></td>
            <td><?php echo $do['kelas'] ?></td>
            <td><?php echo $do['tahun_ajaran'] ?></td>
            <td><?php echo $do['biaya_spp'] ?></td>
            <td><?php echo $do['daftar_ulang'] ?></td>
            <td><?php echo $do['ppdb'] ?></td>
            <td><?php echo $do['sisa_bayar'] ?></td>
            <td><?php echo $do['finish'] ?></td>
        </tr>
    <?php
        }
    ?>
        </table>
    <?php
    }
    if(isset($_POST['editFoto'])){
        $id = $_POST['id'];
        if(strlen($_FILES['file1']['name']) > 0){
            $nama_file1 = $_FILES['file1']['name'];
            $ukuran_file1 = $_FILES['file1']['size'];
            $tipe_file1 = $_FILES['file1']['type'];
            $tmp_file1 = $_FILES['file1']['tmp_name'];
            $path1 = "../../asset/siswa/".$id;
            if( is_dir($path1) === false )
            {
                mkdir("../../asset/siswa/".$id."/");
            }
            $pathData = "asset/siswa/".$id."/".$nama_file1;

            // echo $pathData;
            if($ukuran_file1 <= 1000000){		
                if(move_uploaded_file($tmp_file1, $path1."/".$nama_file1)){
                    $upd = "update m_siswa set photo = '$pathData' where m_siswa_id = '$id'";
                    $res = mysqli_query($con,$upd);
                    if($res){
                        echo "200";
                    }else{
                        echo $upd;
                    }
                }
            }
        }
    }
    if(isset($_GET['find'])){
        $kelas = $_GET['kelas'];
        $filter = "";
        if(strlen($kelas) > 0){
            $filter = " and m_kelas_id = '$kelas' ";
        }

        $sel = "select * from m_siswa where isactive = 1 and is_lulus is null $filter  
        order by kelas ,nama_siswa ";
        echo queryJson($con,$sel);
    }
    if(isset($_POST['tp'])){
        $tipe = $_POST['tp'];
        if($tipe == "add"){
            $nis = $_POST['nis'];
            $nama = $_POST['nama'];
            $tempat_lahir = $_POST['tempat_lahir'];
            $tgl_lahir = $_POST['tgl_lahir'];
            $kelas = $_POST['kelas'];
            $ppdb = $_POST['ppdb'];
            $card_id = $_POST['card_id'];
            $daftar_ulang = $_POST['daftar_ulang'];
            $spp = $_POST['spp'];
            $alamat = $_POST['alamat'];
            $thn = $_POST['tahun_ajaran'];
            $nisn = $_POST['nisn'];
            $sisa = $_POST['sisa'];
            $pathData = "";
            // file disini

            // cek dulu disini

            $selCek = "select * from m_siswa where nisn = '$nisn' or id_absen = '$card_id'";
            $res = mysqli_query($con,$selCek);
            if(mysqli_num_rows($res) > 0){
                echo "<script>alert('kemungkinan NISN atau Kartu sudah pernah di daftarkan sebelumnya, perhatikan saat Klik Tambahkan (Jangan 2 kali)')</script>";
                echo "<script>window.location='../../?go=addsiswa'</script>";
                exit;
            }

            $sel = "select * from m_kelas where m_kelas_id = '$kelas'";
            $ro = mysqli_query($con,$sel);
            $dp = mysqli_fetch_array($ro);
            $kls = $dp['nama_kelas'];

            if(strlen($_FILES['photo']['name']) > 0){
                $nama_file1 = $_FILES['photo']['name'];
                $ukuran_file1 = $_FILES['photo']['size'];
                $tipe_file1 = $_FILES['photo']['type'];
                $tmp_file1 = $_FILES['photo']['tmp_name'];
                $path1 = "../../asset/siswa/".$id_uniq;
                if( is_dir($path1) === false )
                {
                    mkdir("../../asset/siswa/".$id_uniq."/");
                }
                $pathData = "asset/siswa/".$id_uniq."/".$nama_file1;

                if($ukuran_file1 <= 1000000){		
                    if(move_uploaded_file($tmp_file1, $path1."/".$nama_file1)){

                    }
                }
            }

            $insert = "insert into m_siswa (m_siswa_id,nis,nama_siswa,tgl_lahir,alamat,kelas
            ,m_kelas_id,biaya_spp,daftar_ulang,ppdb,pwd,tempat_lahir,isactive,createdate,createdby,tahun_ajaran,id_absen,photo,nisn,sisa_bayar)
            values ('$id_uniq','$nis','$nama','$tgl_lahir','$alamat','$kls'
            ,'$kelas','$spp','$daftar_ulang','$ppdb','123','$tempat_lahir',1,now(),'$_nama','$thn','$card_id','$pathData','$nisn','$sisa')";

            $bulan = array("Juli","Agustus","September","Oktober","November","Desember","Januari","Februari","Maret","April","Mei","Juni");
            for($i = 0; $i<count($bulan); $i++){
                $bln = $bulan[$i];
                $seq = $i +1;

                $id_uniq2 = "select uuid() as id";
                $result2 = mysqli_query($con,$id_uniq2);
                $id_uniq2 = mysqli_fetch_array($result2);
                $id_uniq2 = $id_uniq2["id"];
                if($spp == ""){
                    $spp = 0;
                }
                $inse = "insert into transaksi_spp (transaksi_spp_id,m_siswa_id,kelas,tahun_ajaran,seq,bulan,biaya_spp,bayar_spp,creatdate)
                values ('$id_uniq2','$id_uniq','$kelas','$thn',$seq,'$bln','$spp',0,null)";
                
                $result1 = mysqli_query($con,$inse);
                // echo $inse."</br>";
            }

            // isi daftar ulang
            $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
            ,nominal,kode_status,status_transaksi,createdby,createdate,m_siswa_id,kelas,tahun_ajaran,approvedby,harus_bayar)
            values (uuid(),'DAFTAR ULANG','OFFLINE',0
            ,'$daftar_ulang','WT0','Waiting','$_user',now(),'$id_uniq','$kls','$thn','$_user','$daftar_ulang') ";
            
            $result = mysqli_query($con,$upd);

            // 

            $result = mysqli_query($con,$insert);
            if($result){
                echo "<script>alert('Berhasil')</script>";
                echo "<script>window.location='../../?go=listsiswa'</script>";
            }
            // echo $insert;
        }else if($tipe == "ganti-pwd"){
            $old = $_POST['old_pwd'];
            $new = $_POST['new_pwd'];
            $cek = "select * from m_siswa where m_siswa_id = '$_user' and pwd = '$old' ";

            // echo $cek;
            $result = mysqli_query($con,$cek);
            if( mysqli_num_rows($result) > 0){
                $upd = "update m_siswa set pwd = '$new' where m_siswa_id = '$_user' ";
                $resultok = mysqli_query($con,$upd);
                if($resultok){
                    echo "<script>alert('Password berhasil di ubah')</script>";
                    echo "<script>window.location='../../?go=listsiswa'</script>";
                }
            }else{
                echo "<script>alert('Password Lama Anda salah...')</script>";
                echo "<script>window.location='../../?go=listsiswa'</script>";
            }
        }else if($tipe == "edt"){
            $nis = $_POST['nis'];
            $nama = $_POST['nama'];
            $tempat_lahir = $_POST['tempat_lahir'];
            $tgl_lahir = $_POST['tgl_lahir'];
            $kelas = $_POST['kelas'];
            $alamat = $_POST['alamat'];
            $thn = $_POST['tahun_ajaran'];
            $card_id = $_POST['card_id'];
            $id = $_POST['id'];
            $tunggakan = $_POST['tunggakan'];
            $ppdb = $_POST['ppdb'];
            $daftar_ulang = $_POST['daftar_ulang'];
            $spp = $_POST['spp'];
            $nisn = $_POST['nisn'];

            $cl = "select * from m_kelas where m_kelas_id = '$kelas' ";
            $resultok = mysqli_query($con,$cl);
            $dtx = mysqli_fetch_array($resultok);
            $kls = $dtx['nama_kelas'];
            $password = $_POST['password'];
            $pwd = "";
            if(strlen($password) > 0){

                $pwd = " , pwd = '$password'";
            }

            $upd = "update m_siswa set tempat_lahir = '$tempat_lahir',tgl_lahir = '$tgl_lahir',kelas = '$kls', m_kelas_id = '$kelas',
            nama_siswa = '$nama',id_absen = '$card_id', ppdb = '$ppdb',sisa_bayar = '$tunggakan'
            , daftar_ulang = '$daftar_ulang',nisn = '$nisn', alamat = '$alamat', tahun_ajaran = '$thn'
            $pwd
            where m_siswa_id = '$id'";

            $res = mysqli_query($con,$upd) ;

            


            // ppdb
            $updatePPDB = "update m_transaksi set harus_bayar = '$ppdb' where m_siswa_id = '$id' 
            and jenis_transaksi = 'PPDB'";

            $res1 = mysqli_query($con,$updatePPDB) ;

            //daftar ulang
            $sel2 = "select * from m_transaksi where m_siswa_id = '$id' 
                    and jenis_transaksi = 'DAFTAR ULANG' and tahun_ajaran = '$thn'";
            $res2 = mysqli_query($con,$sel2);
            if(mysqli_num_rows($res2) == 0 && $daftar_ulang > 0){
                $insertDU = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                ,nominal,kode_status,status_transaksi,createdby,createdate,m_siswa_id,kelas,tahun_ajaran,approvedby,harus_bayar)
                values (uuid(),'DAFTAR ULANG','OFFLINE',0
                ,0,'WT0','Waiting','SISTEM',now(),'$id','$kls','$thn','SISTEM','$daftar_ulang') ";

                mysqli_query($con,$insertDU);
            }
            $updateDU = "update m_transaksi set harus_bayar = '$daftar_ulang' where m_siswa_id = '$id' 
            and jenis_transaksi = 'DAFTAR ULANG' and tahun_ajaran = '$thn'";
            $res3  = mysqli_query($con,$updateDU);

            // SPP
            $selspp = "select * from m_transaksi where m_siswa_id = '$id' 
                    and jenis_transaksi = 'BAYAR SPP' and tahun_ajaran = '$thn'";
            $resspp = mysqli_query($con,$selspp);
            if(mysqli_num_rows($resspp) == 0){
                $updateSPP = "update transaksi_spp set biaya_spp = '$spp' where m_siswa_id = '$id' 
                and tahun_ajaran = '$thn'";
                $res4  = mysqli_query($con,$updateSPP);   

                $upd0 = "update m_siswa set biaya_spp = '$spp'
                where m_siswa_id = '$id'";
                $res = mysqli_query($con,$upd0) ;
            }

            echo "200";
        }
    }
    if(isset($_GET['findOne'])){
        $id = $_GET['id'];
        $sel = "select *,'$role' as role from m_siswa where m_siswa_id = '$id'";
        echo queryJson($con,$sel);
    }
    if(isset($_POST['upload'])){
        $tahun_ = $_POST['tahun'];
        $dataList = json_decode($_POST['listData'], true);
        $c = 1;
        $ins = "";

        $del1 = "update tahun set isactive = 0";
        mysqli_query($con,$del1);

        $del1 = "update tahun set isactive = 1 where tahun_ajaran = '$tahun_' ";
        mysqli_query($con,$del1);

        $del1 = "delete from kenaikan_kelas";
        mysqli_query($con,$del1);

        for($i = 0; $i<count($dataList); $i++){
            try {
                if($dataList[$i]["NISN"]){
                    $Key_id = array_key_exists("Key_id", $dataList[$i]) ? $dataList[$i]["Key_id"] : null ;
                    $nisn = array_key_exists("NISN", $dataList[$i]) ? $dataList[$i]["NISN"] : null ;
                    $nis = $dataList[$i]["NIS"] ? $dataList[$i]["NIS"] : null ;
                    $nama_siswa = $dataList[$i]["Nama Siswa"] ? $dataList[$i]["Nama Siswa"] : null ;
                    $Kelas = $dataList[$i]["Kelas"] ? $dataList[$i]["Kelas"] : null ;
                    $tahun = $dataList[$i]["Tahun Ajaran"] ? $dataList[$i]["Tahun Ajaran"] : null ;
                    $spp = array_key_exists("Biaya SPP", $dataList[$i]) ? $dataList[$i]["Biaya SPP"] : null ;
                    $du = array_key_exists("Biaya DU", $dataList[$i]) ? $dataList[$i]["Biaya DU"] : 0 ;
                    $ppdb = array_key_exists("PPDB", $dataList[$i]) ? $dataList[$i]["PPDB"] : 0 ;
                    $sisa = array_key_exists("Tunggakan Sebelumnya", $dataList[$i]) ? $dataList[$i]["Tunggakan Sebelumnya"] : 0 ;
                    $lulus = $dataList[$i]["Lulus"] ? $dataList[$i]["Lulus"] : null ;

                    $nisn = str_replace("'", "", $nisn);
                    $nis = str_replace("'", "", $nis);

                    $ins = $ins.",('$Key_id','$nisn','$nis','$nama_siswa','$Kelas','$tahun'
                    ,'$spp','$du','$ppdb','$sisa','$lulus')";

                    // echo $ins."\n";
                }
            } catch (Exception $th) {
                echo "Periksa Kelengkapan isian ".$th;
            }
        }

        $ins = substr($ins,1);
        $masuk = "INSERT INTO kenaikan_kelas (key_id, nisn, nis, nama_siswa, kelas, tahun, spp, du, pdb, sisa, lulus)values ".$ins;

        $res = mysqli_query($con,$masuk);

        $del = "delete from m_siswa_backup";
        $res = mysqli_query($con,$del);

        $ins2 = "insert into m_siswa_backup select * from m_siswa";
        mysqli_query($con,$ins2);

        if($res){
            $sel = "select * from  kenaikan_kelas";
            $rop = mysqli_query($con,$sel);
            while($d = mysqli_fetch_array($rop)){

                $key_id = $d['key_id'];
                $kelas = $d['kelas'];
                $tahun = $d['tahun'];
                $spp = $d['spp'];
                $du = $d['du'];
                $pdb = $d['pdb'];
                $sisa = $d['sisa'];
                $lulus = $d['lulus'];
                $nisn = $d['nisn'];
                $nis = $d['nis'];
                $nama_siswa = $d['nama_siswa'];

                $sel = "select * from m_kelas where nama_kelas = '$kelas'";
                $x = mysqli_query($con,$sel);
                $x = mysqli_fetch_array($x);
                $m_kelas_id = $x['m_kelas_id'];

                $islulus = "";
                if($lulus == "Y"){
                    $islulus = " , is_lulus = 'Y' ";
                }

                if($key_id){

                    $upd = "update m_siswa set kelas = '$kelas', m_kelas_id = '$m_kelas_id'
                    ,biaya_spp = '$spp',daftar_ulang = '$du',ppdb = '$pdb',tahun_ajaran = '$tahun'
                    ,sisa_bayar = '$sisa' $islulus where m_siswa_id = '$key_id'";

                }else{
                    $key_id = generateUUID();
                    $upd = "INSERT INTO m_siswa
                    (m_siswa_id, nis, nama_siswa, tempat_lahir, tgl_lahir, alamat, kelas, m_kelas_id, 
                    biaya_spp, daftar_ulang, ppdb, isactive, pwd, createdby, 
                    createdate, tahun_ajaran, id_absen, photo, nisn, tgl_lahir_string, sisa_bayar
                    , is_lulus)
                    VALUES('$key_id', '$nis', '$nama_siswa', NULL, NULL, NULL, '$kelas', '$m_kelas_id', '$spp', '$du'
                    , '$pdb', 1, '12345', 'SYSTEM', now(), '$tahun', NULL, NULL, '$nisn', NULL, 0
                    , NULL);";
                }

                // echo $upd;
                // exit;
                $hasilUpdate = mysqli_query($con,$upd);
                if($lulus == "N"){
                    // masukin logic untuk SPP
                    $sppTrans = "insert into transaksi_spp 
                    select uuid(),'$key_id',null,'$m_kelas_id','$tahun',seq,bulan,'$spp',0,now(),null
                    from transaksi_spp
                    where m_siswa_id = '5f159d42-494d-11ed-bd94-fcb3bca9bd65' and tahun_ajaran = '2022/2023' order by seq asc";
                    $hasilUpdateSPP = mysqli_query($con,$sppTrans);

                    // masukin logic untuk DAFTAR ULANG
                    $duTrans = "insert into m_transaksi 
                    select uuid(),'$key_id','$kelas','DAFTAR ULANG','OFFLINE',0,$du,1,'WAITING','WT0'
                    ,now(),'SYSTEM','SYSTEM',null
                    ,null,'$tahun',$du,null";

                    $hasilUpdateDU = mysqli_query($con,$duTrans);

                    // logic untuk biaya daftar ulang
                    
                }
                
            }
            // $dp 
        }

        echo "200";
    }

    function generateUUID() {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // karakter "-"
            $uuid = substr($charid, 0, 8) . $hyphen
                  . substr($charid, 8, 4) . $hyphen
                  . substr($charid, 12, 4) . $hyphen
                  . substr($charid, 16, 4) . $hyphen
                  . substr($charid, 20, 12);
            return $uuid;
        }
    }
?>