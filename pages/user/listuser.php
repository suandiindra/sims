<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List User</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">List Agent</li>
    </ol>
</div>
<div class="row">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><a href="./?go=adduser">+ Data User</a></h6>
                </div>
                <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Password</th>
                        <th style="width:200px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sel = "select a.m_user_id,a.username,phone,email,pwd,b.nama as role from m_user a
                            inner join m_role b on a.m_role_id = b.m_role_id
                            where (a.isactive is null or a.isactive = 1) and phone not in ('001')";
                            $result = mysqli_query($con,$sel);
                            $i = 1;
                            while($res = mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res['phone']; ?></td>
                                <td><?php echo $res['username']; ?></td>
                                <td><?php echo $res['email']; ?></td>
                                <td><?php echo $res['role']; ?></td>
                                <td><?php echo $res['pwd']; ?></td>
                                <td style="text-align:center">
                                  <table>
                                    <tr>
                                    <td  style="padding:0"><a href="./?go=adduser&act=edit&id=<?php echo $res['m_user_id']; ?>"><button class="btn btn-danger">Edit</button></a></td>
                                    </tr>
                                  </table>
                                </td>
                            </tr>
                        <?php
                            $i += 1;
                            }
                        ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    