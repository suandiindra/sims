<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data User</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">User</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $phone = "";
        $email = "";
        $m_role_id = "";
        $role = "";
        $tipe = "add";
        $label = "Tambahkan";
        $password = "";
        $m_user_id = "";
        $isactive = "";
        $word_isactive = "";
        $note = "";
        if(isset($_GET["id"])){
            $note = "isi jika ada perubahan";
            $id = $_GET["id"];
            $key = $id;
            $sel = "select a.m_user_id,a.username,phone,email,pwd,b.nama as role,a.m_role_id,a.isactive from m_user a
            inner join m_role b on a.m_role_id = b.m_role_id where m_user_id = '$id'";
            // echo $sel;
            $result = mysqli_query($con,$sel);
            if($result){
                $tipe = "edit";
                $label = "Edit";
                $data = mysqli_fetch_array($result);
                $m_user_id = $data['m_user_id'];
                $nama_lengkap = $data['username'];
                $phone = $data['phone'];
                $email = $data['email'];
                $role = $data['role'];
                $m_role_id = $data['m_role_id'];
                $isactive = $data['isactive'];
                if($isactive == "1" || !$isactive){
                    $isactive = "1";
                    $word_isactive = "Aktif";
                }else{
                    $isactive = "0";
                    $word_isactive = "Non Aktif";
                }
                
            }
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri User diisi dan di kelola oleh Admin
                    <hr>
            </div>

            <div class="card-body">
            <Form method="POST" action="pages/user/actionuser.php">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Username/NIK</label>
                    <input type="text" required  name="phone" class="form-control" value= "<?php echo $phone ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Lengkap</label>
                    <input type="text" required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text" required  name="email" class="form-control" value= "<?php echo $email ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Password</label><br>
                    <i style="color:blue"><?php $note; ?></i>
                    <input type="text" name="pwd" class="form-control" value= "<?php echo $password ?>">
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <?php
                        $sel = "select * from m_role where isactive = 1";
                        $result1 = mysqli_query($con,$sel);
                    ?>
                   <select name="role" required id="m_role_id" class="form-control" >
                        <option value="<?php echo $m_role_id ?>"><?php echo $role ?></option>
                        <?php
                            while($res = mysqli_fetch_array($result1)){
                        ?>
                                <option value="<?php echo $res['m_role_id']?>"><?php echo $res['nama'] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                   <label>Aktivasi</label>
                   <select name="aktif" required id="m_role_id" class="form-control" >
                        <option value="<?php echo $isactive; ?>"><?php echo $word_isactive ?></option>
                        <option value="1">Aktif</option>
                        <option value="0">Non Aktif</option>
                    </select>
                </div>
                <Button class="btn btn-success">
                   <?php echo $label; ?>
                </Button>
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>