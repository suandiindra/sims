<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <div class="row" style="margin-top:-35px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
                    <b><h3>DAFTAR ULANG</h3></b>
                    <hr>
                    <div class="mb-3">
                        <label for="inputPassword5" require class="form-label">Nama Siswa</label>
                        <br>
                        <select  id="nama" onchange="test()" class=" form-control kodebrg">
                        <option value=""></option>
                        <?php
                            $del = "select * from m_siswa where isactive = 1";
                            $res = mysqli_query($con,$del);
                            while($rs = mysqli_fetch_array($res))
                            {
                        ?>
                            <option value="<?php echo $rs['m_siswa_id'] ?>"><?php echo $rs['nama_siswa'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">Tahun Ajaran</label>
                        <select name="" class="form-control" id="tahun_ajaran"  onchange="getData()">
                            <option value=""></option>
                            <?php
                                $del = "select * from tahun t order by isactive desc";
                                $res = mysqli_query($con,$del);
                                while($rs = mysqli_fetch_array($res))
                                {
                            ?>
                                <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">NIS</label>
                        <input readonly class="form-control" onchange="getData()" id="nis" type="text" value= "">
                    </div>
                    <div class="mb-3">
                        <label for="formFileDisabled" class="form-label">Kelas</label>
                        <input class="form-control" id="kelas" type="text" readonly value= "">
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">Tunggakan Daftar Ulang</label>
                        <input class="form-control" readonly type="text" id ="bulan" name="bulan">
                    </div>
                    <div class="mb-3">
                        <label for="formFileMultiple" class="form-label">Nominal</label>
                        <input class="form-control" require type="number" id="nominal" name="nominal">
                                    </div>
                    <Button class="btn btn-warning" style="margin-top:10px" onclick="konfirmasi(this)">
                        Konfirmasi
                    </Button>
            </div>
            <div class="row">
                <div class="col col-md-12">
                    <table class="table">
                        <thead>
                            <th>Tanggal Bayar</th>
                            <th>Tahun Ajaran</th>
                            <th>Kelas</th>
                            <th>Biaya</th>
                            <th>Dibayar</th>
                        </thead>
                        <tbody id="transaksiDU">
                        </tbody>
                    </table>
                </div>
            </div>
       
    </div>
    </div>
</div>

<script>
    $(".kodebrg").chosen();
    
    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        return ribuan
    }
    function getData(){
        var nis = document.getElementById("nama").value;

        console.log(nis);
        $.ajax({
            type:'POST',
            url:'pages/daftar_ulang/actiondaftarulang.php',
            data:{
              cari : true,
              m_siswa_id : nis,
              tahun_ajaran : $(`#tahun_ajaran`).val()
            },
            success:function(data){
                // console.log(data);
                // return
                var objek = $.parseJSON(data);
            
                var obj = objek.data[0]
                var tbl =  objek.dataDU
                if($(`#tahun_ajaran`).val() == ""){
                    return
                }
                // console.log(obj);
                if(data !== "0"){
                    var nama  = obj.nama_siswa;
                    var kelas = obj.kelas;
                    var cost  = obj.sisa
                    var nis   = obj.nis
                    document.getElementById("nis").value   = nis
                    document.getElementById("kelas").value = kelas
                    document.getElementById("bulan").value = duit(cost)
                }else{
                    document.getElementById("kelas").value = ""
                    document.getElementById("bulan").value = ""
                }
                // table
                console.log(tbl);
                let dom = `` 
                for(let i = 0; i<tbl.length; i++){
                    let cls = tbl[i].kode_status == 'WT0' ? "class='text-white bg-danger'" : "class='text-white bg-success'"

                    let tgl_bayar = tbl[i].kode_status == 'WT0' ? "" : tbl[i].tgl_bayar
                    dom = dom + `<tr ${cls}>
                        <td>${tgl_bayar}</td>
                        <td>${tbl[i].tahun_ajaran}</td>
                        <td>${tbl[i].kelas}</td>
                        <td>${duit(tbl[i].harus_bayar)}</td>
                        <td>${tbl[i].kode_status == "WT0" ? "0" : duit(tbl[i].nominal) }</td>
                    </tr>`
                }
                $(`#transaksiDU`).html(dom)
            }
        }); 
    }
    function duit(v){
        // return v
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        console.log(ribuan);
        
        return ribuan
    }
    function konfirmasi(){
        if(!confirm(`Yakin melanjutkan transaksi.. ?`)){
            return
        }
        var nis = document.getElementById("nama").value;
        var nominal = document.getElementById("nominal").value;
        var tahun_ajaran = document.getElementById("tahun_ajaran").value;
        var kelas = document.getElementById("kelas").value;
        $.ajax({
            type:'POST',
            url:'pages/daftar_ulang/actiondaftarulang.php',
            data:{
                bayar_du : 'true',
                id : nis,
                nominal : nominal,
                tahun_ajaran : tahun_ajaran,
                kelas : kelas
            },
            success:function(data){
                console.log(data);
                // var obj = Object.keys(data)
                if(data == "200"){
                    berhasil();
                    console.log("yessss....");
                }
            }
        }); 
    }
    function berhasil(){
        alert("Berhasil...")
        window.location='./?go=daftarulang'
    }

    function test(){
        console.log("xxxx");
        getData();
    }
</script>