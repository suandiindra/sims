<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Pembayaran Daftar Ulang Siswa</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Daftar Ulang Siswa</li>
    </ol>
</div>
      <div class="card" style="margin-top:-20px">
          <div class="card-body">
            <input type="hidden" name="randcheck" /> 
            <div class="container col-12" style="margin-top:0px">
            <div class="row" style="margin-bottom:20px">
                <div class="col col-md-3">
                    <label for="inputPassword5" class="form-label">Tgl Awal</label>
                    <input type="date" class="form-control" id="tgl1">
                </div>
                <div class="col col-md-3">
                    <label for="inputPassword5" class="form-label">Tgl Akhir</label>
                    <input type="date" class="form-control" id="tgl2">
                </div>
                <div class="col col-md-3">
                    <label for="inputPassword5" class="form-label">Tahun Ajaran</label>
                    <select style="<?php echo $disp ?>" class="form-control" id="tahun_ajaran">
                    <option value=""></option>
                    <?php
                        $del = "select * from tahun t order by isactive asc";
                        $res = mysqli_query($con,$del);
                        while($rs = mysqli_fetch_array($res))
                        {
                    ?>
                        <option value="<?php echo $rs['tahun_ajaran'] ?>"><?php echo $rs['tahun_ajaran'] ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="col col-md-3" style="margin-top:32px">
                    <Button class="btn btn-success" onclick="loadDU()">Lihat</Button>
                    <a href="./?go=add_daftarulang">
                        <Button class="btn btn-primary" style="float:right; margin-top:0px" id="adddu" >Input Data</Button>
                    </a>
                </div>
            </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-md-12">
            <div class="card">
                <div class="card-body">
                <p id="uang">Total Uang Masuk : </p>

                </div>
            </div>
        </div>
      </div>

<div class="row" style="margin-top:20px">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="dataTableHover" style="margin-top:20px">
                <thead class="thead-light">
                <tr>
                        <th>No.</th>
                        <th>NIS</th>
                        <th>Tgl Bayar</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Nominal</th>
                        <th>Tgl Bayar</th>
                        <th>Metode bayar</th>
                        <th>Tahun Ajaran</th>
                        <th>Status</th>
                        <!-- <th>Petugas</th> -->
                        <th>Action</th>
                </tr>
                </thead>
                <tbody id="dudtl">
                    <!-- didie -->
                </tbody>
            </table>


                </div>
              </div>
            </div>
          </div>
          <!-- modal -->
            <div class="modal fade" id="modalaksidu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran Daftar Ulang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="formFileMultiple" class="form-label">NAMA</label>
                        <input class="form-control" id="nama" type="text" readonly >
                        <input class="form-control" id="keys" type="hidden" >
                    </div>
                    <div class="mb-3">
                        <label for="formFileDisabled" class="form-label">TGL BAYAR</label>
                        <input class="form-control" id="tgl" type="text" readonly>
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">NOMINAL PPDB</label>
                        <input class="form-control" type="number" id ="nominal" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="editdu()">Edit</button>
                </div>
                </div>
            </div>
            </div>
        </div>
<script>
    window.addEventListener('load', loadDU(), false);
    function loadDU(){
        let role = "<?php echo $_SESSION['role']?>"
        if(role !== "Tata Usaha"){
            $(`#adddu`).css('display','none')
        }

        // var table = $('#dataTableHover').DataTable();
        // table.clear().destroy(); 
        $.ajax({
            type:'GET',
            url:`pages/daftar_ulang/actiondaftarulang.php`,
            data: {
                find : true,
                tgl1 : $(`#tgl1`).val(),
                tgl2 : $(`#tgl2`).val(),
                tahun : $(`#tahun_ajaran`).val()
            },
            success:function(data){
                console.log(data);
                // return;
                let obj = JSON.parse(data)
                obj = obj.data
                let dom = ``
                let uang = 0
                for(let i = 0; i<obj.length; i++){
                    let color = "";
                    if(obj[i].kode_status == "WT1"){
                        color = "background-color:#FF2D5B; color:white";
                    }else if(obj[i].kode_status == "WT0"){
                        color = "background-color:#D3C8BA; color:red";
                    }else{
                        color = "background-color:#7CCFC9; color:white";
                    }
                    uang = uang + parseFloat(obj[i].nominal)
                    dom = dom + `<tr style="${color}">
                        <td>${i+1}</td>
                        <td>${obj[i].nis}</td>
                        <td>${obj[i].tgl_bayar}</td>
                        <td>${obj[i].nama_siswa}</td>
                        <td>${obj[i].nama_kelas}</td>
                        <td>${duit(obj[i].nominal)}</td>
                        <td>${obj[i].tgl_bayar}</td>
                        <td>${obj[i].tipe_transaksi}</td>
                        <td>${obj[i].tahun_ajaran}</td>
                        <td>${obj[i].status_transaksi}</td>
                        <td style="text-align:center">
                            
                        </td>
                    <tr>`
                }
                $(`#uang`).html(`Total Uang Masuk : ${duit(uang)}`)
                $(`#dudtl`).html(dom)
                $('#tblkehadiran').DataTable(({ 
                        "destroy": true, //use for reinitialize datatable
                        order: [[1, 'desc']],
                }));
                console.log(obj);
            }
        })
    }
    function duit(v){
        // return v
        console.log(v);
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        
        return `${ribuan}`
    }
    function confirmationDelete(anchor){
    var conf = confirm('Apakah yakin melakukan proses ini ???');
    if(conf)
        window.location=anchor.attr("href");
    }
    function exp(){
        var sts = document.getElementById("stat").value
        var kelas = document.getElementById("kelas").value

        // console.log("./pages/daftar_ulang/exportdu.php?sts="+sts+"&kelas="+kelas+"");
        window.location="./pages/daftar_ulang/exportdu.php?sts="+sts+"&kelas="+kelas+"";
    }
    function editdu(){
        console.log('woii');
        $.ajax({
            type:'POST',
            url:`pages/daftar_ulang/actiondaftarulang.php`,
            data: {
                edit : true,
                id : $(`#keys`).val(),
                nominal : $(`#nominal`).val()
            },
            success:function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=daftarulang";
                }
            }
        })
    }
    function hapusdu(){
        console.log('woii');
        $.ajax({
            type:'POST',
            url:`pages/daftar_ulang/actiondaftarulang.php`,
            data: {
                hapus : true,
                id : $(`#keys`).val(),
                nominal : $(`#nominal`).val()
            },
            success:function(data){
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=daftarulang";
                }
            }
        })
    }
    function aksi(id,nama,tgl,nominal){
        console.log(id,nama,tgl,nominal);
        $(`#nama`).val(nama)
        $(`#keys`).val(id)
        $(`#tgl`).val(tgl)
        $(`#nominal`).val(nominal)
        $(`#modalaksidu`).modal('show')
    }
</script>