<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <?php 
        $idx = $_SESSION['m_user_id'];
        $sel = "select case when b.m_transaksi_id is null then daftar_ulang else 0 end as cost,a.tahun_ajaran from m_siswa a
        left join m_transaksi b on a.m_siswa_id = b.m_siswa_id
        and a.tahun_ajaran = b.tahun_ajaran 
        and kode_status = 'WT2' and jenis_transaksi = 'DAFTAR ULANG'
        where a.m_siswa_id = '$idx'";

        // echo $sel;
        $da = mysqli_query($con,$sel);
        $rs = mysqli_fetch_array($da);
    ?>
    <div class="row" style="margin-top:-35px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi Data Siswa
                    <div style="float:right; margin-bottom:10px">
                     <a href="./?go=listdaftarulang"><button class="btn btn-danger">Riwayat Daftar Ulang</button></a>
                    </div>
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                        <table class="col-md-12" style="margin-top:10px; padding:20px">
                            <tr >
                            <form action="pages/daftar_ulang/actiondaftarulang.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="tp" value="trf-du">
                                <td>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">NIS</label>
                                        <input class="form-control" type="text" readonly value= "<?php  echo $_SESSION['nis']; ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">NAMA</label>
                                        <input class="form-control" type="text" readonly value= "<?php  echo $_SESSION['nama']; ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Kelas</label>
                                        <input class="form-control" type="text" readonly value= "<?php  echo $_SESSION['kelas']; ?>">
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFileDisabled" class="form-label">Tahun Ajaran</label>
                                        <input class="form-control" type="text" name = "tahun_ajaran" readonly value= "<?php  echo $rs['tahun_ajaran']; ?>">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFile" class="form-label">Tunggakan Daftar Ulang</label>
                                    <input class="form-control" readonly type="number" name="biaya" value="<?php echo ($rs['cost']) ?>">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFileMultiple" class="form-label">Nominal Pembayaran</label>
                                    <input class="form-control" require type="number" name="nominal">
                                    </div>
                                    <div class="mb-3">
                                    <label for="formFileDisabled" class="form-label">Upload Bukti Transfer</label>
                                    <input class="form-control" require type="file" id="formFileDisabled" name="bukti">
                                    </div>
                                </td>
                            
                            </tr>
                            
                        </table>
                        <Button class="btn btn-warning" style="margin-top:10px">
                            Konfirmasi
                        </Button>
                        </form>
                    </col>
                </Row>
            </div>
            

            <!-- <table class="table align-items-center table-flush table-hover" id="dataTableHover" style="margin-top:20px">
                <thead class="thead-light">
                <tr>
                    <th>No</th>
                    <th>Bulan</th>
                    <th>Biaya SPP</th>
                    <th>Jumlah Bayar</th>
                    <th>Tgl Bayar</th>
                </tr>
                </thead>
                <tbody id="partialdo">
                    <?php
                        $sel = "select * from transaksi_spp where m_siswa_id = '$_user'";
                        $result = mysqli_query($con,$sel);
                        $i = 1;
                        while($res = mysqli_fetch_array($result)){
                        $color = "";
                        if($res['bayar_spp'] == "0"){
                            $color = "background-color:#BF202A; color:white";
                        }else{
                            $color = "background-color:#106DCA; color:white";
                        }
                    ?>
                    <tr style="<?php echo $color; ?>">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['bulan']; ?></td>
                        <td><?php echo number_format($res['biaya_spp']); ?></td>
                        <td><?php echo number_format($res['bayar_spp']); ?></td>
                        <td><?php echo $res['createdate']; ?></td>
                    </tr>
                    <?php
                        $i += 1;
                        }
                    ?>
                </tbody>
            </table> -->
            </div>
        </div>
    </div>
    </div>



</div>