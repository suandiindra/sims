<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    $tipe = "";
    $id_uniq = "select uuid() as id";
    $result = mysqli_query($con,$id_uniq);
    $id_uniq = mysqli_fetch_array($result);
    $id = $id_uniq["id"];   
    $obj = new stdClass();


    $_user = $_SESSION["m_user_id"];
    $_nama = $_SESSION['nama'];
    // $_kelas = $_SESSION['kelas'];
    if(isset($_GET['find'])){
        $period1 = $_GET['tgl1'];
        $period2 = $_GET['tgl2'];
        $tahun = $_GET['tahun'];
        $where = "";

        if(strlen($period1) > 0){
            $where = " and date_format(a.createdate, '%Y-%m-%d') between '$period1' and '$period2' ";
        }else{
            $where = " and date_format(a.createdate, '%Y-%m'), date_format(now() , '%Y-%m') ";
        }

        if(strlen($tahun) == 0){
            $selc = "select * from tahun where isactive = 1";
            $tp = mysqli_query($con,$selc);
            $pp = mysqli_fetch_array($tp);
            $thn = $pp['tahun_ajaran'];
            $where = $where." and a.tahun_ajaran = '$thn'";
        }else{
            $where  = $where."and a.tahun_ajaran = '$tahun'";
            
        }
        $sel = "select username, m_transaksi_id, bukti,nis,nama_siswa,c.nama_kelas as nama_kelas,bulan,tipe_transaksi,
        status_transaksi,nominal,DATE_FORMAT(a.createdate, '%d-%M-%Y') tgl_bayar,kode_status 
        ,a.approvedby,a.tahun_ajaran
        from m_transaksi a
        inner join m_siswa b on a.m_siswa_id = b.m_siswa_id
        inner join m_kelas c on c.m_kelas_id = b.m_kelas_id
        left join m_user d on d.m_user_id = a.approvedby where 1=1 
        and a.jenis_transaksi = 'DAFTAR ULANG' and  a.kode_status <> 'WT0'
        $where order by a.createdate desc";

        // echo $sel;
        echo queryJson($con,$sel);

    }
    if(isset($_POST['tp'])){
        $nominal = $_POST['nominal'];
        $tahun_ajaran = $_POST['tahun_ajaran'];
        $nama_file1 = $_FILES['bukti']['name'];
        $ukuran_file1 = $_FILES['bukti']['size'];
        $tipe_file1 = $_FILES['bukti']['type'];
        $tmp_file1 = $_FILES['bukti']['tmp_name'];
        $path1 = "../../asset/bukti_du/".$id."/".$nama_file1;

        // echo $tahun_ajaran.$nominal;
        if( is_dir($path1) === false )
        {
            mkdir("../../asset/bukti_du/".$id."/");
        }
        $pathData = "asset/bukti_du/".$id."/".$nama_file1;
        
        if($ukuran_file1 <= 1000000){		
            if(move_uploaded_file($tmp_file1, $path1)){
                $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                ,nominal,kode_status,status_transaksi,createdby,createdate,bukti,m_siswa_id,kelas,tahun_ajaran)
                values ('$id','DAFTAR ULANG','ONLINE',0
                ,'$nominal','WT1','Menunggu Konfirmasi','$_user',now(),'$pathData','$_user','$_kelas','$tahun_ajaran') ";
                // echo $upd;
                $result = mysqli_query($con,$upd);
                if($result){
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=daftarulang'</script>";
                }
            }
        }
    }else if(isset($_GET['id'])){
        $id = $_GET['id'];
        $act = $_GET['act'];
        if($act == "1"){
            $cek_ = "select * from m_user where m_user_id = '$_user'";
            $result = mysqli_query($con,$cek_);
            if(mysqli_num_rows($result) > 0){
                $cek2 = "select * from m_transaksi where m_transaksi_id = '$id' and kode_status = 'WT1'";
                $result1 = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result1) > 0){
                    $upd = "update m_transaksi set status_transaksi = 'Approved', kode_status = 'WT2',approvedby = '$_user'
                    ,approvedate = now(),tahun_ajaran = '2020/2021' where m_transaksi_id = '$id'";

                    // echo $upd;
                    $result2 = mysqli_query($con,$upd);
                    if($result2){
                        echo "<script>alert('Transaksi Berhasil...')</script>";
                        echo "<script>window.location='../../?go=daftarulang'</script>";
                    }
                }

            }else{
                echo "<script>alert('Tidak diizinkan lagi...')</script>";
                echo "<script>window.location='../../?go=daftarulang'</script>";
            }
        }else{
            $cek_ = "select * from m_user where m_user_id = '$_user'";
            $result = mysqli_query($con,$cek_);
            if(mysqli_num_rows($result) > 0){
                $cek2 = "select * from m_transaksi where m_transaksi_id = '$id' and kode_status = 'WT1'";
                $result1 = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result1) > 0){
                    $upd = "update m_transaksi set status_transaksi = 'Rejected', kode_status = 'WT0',approvedby = '$_user'
                    ,approvedate = now() where m_transaksi_id = '$id'";

                    // echo $upd;
                    $result2 = mysqli_query($con,$upd);
                    if($result2){
                        echo "<script>alert('Transaksi Berhasil...')</script>";
                        echo "<script>window.location='../../?go=daftarulang'</script>";
                    }
                }

            }else{
                echo "<script>alert('Tidak diizinkan lagi...')</script>";
                echo "<script>window.location='../../?go=daftarulang'</script>";
            }
        }
    }
    if(isset($_POST['cari'])){
            $nis = $_POST['m_siswa_id'];
            $tahun_ajaran = isset($_POST['tahun_ajaran']) ? $_POST['tahun_ajaran'] : "";

            // $sel = "select ms.*,coalesce(daftar_ulang ,0) as biaya_du 
            //         ,coalesce(daftar_ulang ,0) - coalesce(b.nominal ,0) as sisa,
            //         b.tahun_ajaran as tahun_ajaran_transaksi
            //         from m_siswa ms 
            //         left join m_transaksi b on b.m_siswa_id = ms.m_siswa_id 
            //         and kode_status = 'WT2' and jenis_transaksi = 'DAFTAR ULANG'
            //         and b.tahun_ajaran = '$tahun_ajaran'
            //         where ms.m_siswa_id = '$nis'";
            $sel = "select ms.m_siswa_id,ms.nama_siswa 
                    ,ms.nis ,ms.kelas,b.harus_bayar as biaya_du 
                    ,b.harus_bayar - nominal as sisa
                    from m_siswa ms 
                    left join 
                    (
                        select mt.m_siswa_id,sum(mt.nominal) as nominal,max(harus_bayar) as harus_bayar 
                        from m_transaksi mt where tahun_ajaran = '$tahun_ajaran'
                        and jenis_transaksi = 'DAFTAR ULANG'
                        and mt.m_siswa_id = '$nis'
                    )b on b.m_siswa_id = ms.m_siswa_id 
                    where b.m_siswa_id = '$nis'";

            // echo $sel;
            // exit;
            // echo queryJson($con,$sel);
            $data1 = json_decode(queryJson($con,$sel),true)['data'];


            $sel2 = "select b.*,date_format(b.createdate,'%d-%m-%Y') as tgl_bayar from m_siswa ms 
                    inner join m_transaksi b on b.m_siswa_id = ms.m_siswa_id 
                    and jenis_transaksi = 'DAFTAR ULANG'
                    and b.tahun_ajaran = '$tahun_ajaran'
                    where ms.m_siswa_id = '$nis'";
            $data2 = json_decode(queryJson($con,$sel2),true)['data'];


            $obj -> data = $data1;
            $obj -> dataDU = $data2;
            $data = json_encode($obj);
            // echo $sel;

            echo $data;
    }
    // cek
    if(isset($_POST['bayar_du'])){
        $nominal = $_POST['nominal'];
        $tahun_ajaran = $_POST['tahun_ajaran'];
        $id = $_POST['id'];
        $kelas = $_POST['kelas'];

        $cek = "select * from m_transaksi where jenis_transaksi = 'DAFTAR ULANG' and m_siswa_id = '$id' 
        and tahun_ajaran = '$tahun_ajaran' and kode_status = 'WT0'";

        // echo $cek;
        // exit;
        $res = mysqli_query($con,$cek);
        if(mysqli_num_rows($res) > 0){
            $dp = mysqli_fetch_array($res);
            $id = $dp['m_transaksi_id'];
            $kelas_nya = $dp['kelas'];

            $upd = "update m_transaksi set createdate = now(),status_transaksi = 'Approved'
            ,kode_status = 'WT2',nominal = '$nominal'
            ,approvedate = now(),kelas = '$kelas_nya',harus_bayar = harus_bayar 
            where m_transaksi_id = '$id'";

            // echo $upd;

            $hsl = mysqli_query($con,$upd);
            if($hsl){
                echo "200";
            }else{
                echo $upd;
                exit;
            }
            
        }else{
            $topOne = "select * from m_transaksi where jenis_transaksi = 'DAFTAR ULANG' 
            and m_siswa_id = '$id' 
            and tahun_ajaran = '$tahun_ajaran' limit 1";

            $rop = mysqli_query($con,$topOne);
            $po = mysqli_fetch_array($rop);
            $harus_bayar = $po['harus_bayar'];
            $kelas_nya = $po['kelas'];

            $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                ,nominal,kode_status,status_transaksi,createdby,createdate,m_siswa_id,kelas,tahun_ajaran
                ,approvedby,harus_bayar)
                values (uuid(),'DAFTAR ULANG','OFFLINE',0
                ,'$nominal','WT2','Approved','$_user',now(),'$id','$kelas_nya','$tahun_ajaran'
                ,'$_nama','$harus_bayar') ";
            
            // echo $upd;
            // exit;
            
            $result = mysqli_query($con,$upd);
            if($result){
                echo "200";
            }else{
                echo "500";
            }
        }
    }
    if(isset($_POST['tp'])){
        if($_POST['tp'] == "cek"){
            $nis = $_POST['nis'];
            // echo "indra suandi ".$nis;
            // $sel  = "select * from m_siswa a 
            // inner join m_kelas b on a.m_kelas_id = b.m_kelas_id 
            // where a.nis = '$nis'";

            $sel = "select case when b.m_transaksi_id is null then daftar_ulang else 0 end as cost,a.tahun_ajaran,nama_kelas,a.* 
            from m_siswa a
            left join m_transaksi b on a.m_siswa_id = b.m_siswa_id
            and kode_status = 'WT2' and jenis_transaksi = 'DAFTAR ULANG'
            inner join m_kelas c on c.m_kelas_id = a.m_kelas_id
            where a.m_siswa_id = '$nis'";
            echo $sel;
            exit;

            $res = mysqli_query($con,$sel);
            if(mysqli_num_rows($res) > 0){
                $dt = mysqli_fetch_array($res);
                $id = $dt['m_siswa_id'];
                $thn = $dt['tahun_ajaran'];

                $data ->nis = $dt['nis'];
                $data ->kelas = $dt['nama_kelas'];
                $data ->nama_siswa = $dt['nama_siswa'];
                $data ->cost = $dt['cost'];
                $data ->thn = $thn;
                $data = json_encode($data);
                echo $data;
            }else{
                echo "0";
            }
        }else if($_POST['tp'] == "du_manual"){
            $nominal = $_POST['nominal'];
            $tahun_ajaran = $_POST['tahun_ajaran'];
            $nis = $_POST['nis'];
            $kelas = $_POST['kelas'];

            $cek = "select * from m_siswa where nis = '$nis'";
            $sd = mysqli_query($con,$cek);
            $dd = mysqli_fetch_array($sd);
            $idx = $dd['m_siswa_id'];

            // echo $cek;

            $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                ,nominal,kode_status,status_transaksi,createdby,createdate,m_siswa_id,kelas,tahun_ajaran,approvedby)
                values ('$id','DAFTAR ULANG','OFFLINE',0
                ,'$nominal','WT2','Approved','$idx',now(),'$idx','$_kelas','$tahun_ajaran','$_user') ";
                // echo $upd;
            $result = mysqli_query($con,$upd);
            if($result){
               echo "200";
            }else{
                echo "500";
            }
            // echo $nominal.$tahun_ajaran.$nis.$kelas;
        }

    }
    if(isset($_POST['edit'])){
        $key = $_POST['id'];
        $nominal = $_POST['nominal'];

        $upd = "update m_transaksi set nominal = '$nominal',approvedby = '$_nama' 
        where m_transaksi_id = '$key'";

        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }
    // if(isset($_POST['hapus'])){
    //     $key = $_POST['id'];
    //     $nominal = $_POST['nominal'];

    //     $upd = "delete from m_transaksi
    //     where m_transaksi_id = '$key'";

    //     $res = mysqli_query($con,$upd);
    //     if($res){
    //         echo "200";
    //     }else{
    //         echo $upd;
    //     }
    // }

?>