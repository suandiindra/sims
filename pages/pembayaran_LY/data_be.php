<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    $obj = new stdClass();
    $_user = $_SESSION["m_user_id"];
    $_nama = $_SESSION['nama']; 

    if(isset($_POST['bayar'])){
        $id =  $_POST['id'];
        $nominal =  $_POST['nominal'];
        $catatan =  $_POST['catatan'];


        $cek = "select * from m_siswa where m_siswa_id = '$id'";
        $res = mysqli_query($con,$cek);
        $dp = mysqli_fetch_array($res);
        $harus = $dp['sisa_bayar'] ? $dp['sisa_bayar'] : 0;
        $kelas_nya = $dp['kelas'];
        $tahun_ajaran = $dp['tahun_ajaran'];

        $upd = "insert into m_transaksi (m_transaksi_id,jenis_transaksi,tipe_transaksi,bulan
                ,nominal,kode_status,status_transaksi,createdby,createdate,m_siswa_id,kelas,tahun_ajaran
                ,approvedby,harus_bayar,catatan)
                values (uuid(),'LY','OFFLINE',0
                ,'$nominal','WT2','Approved','$_nama',now(),'$id','$kelas_nya','$tahun_ajaran'
                ,'$_nama','$harus','$catatan') ";
        
        $tp = mysqli_query($con,$upd);
        if($tp){
            echo "200";
        }else{
            echo $upd;
        }
    }
    if(isset($_GET['find'])){
        $filter = "";
        if(strlen($_GET['id']) > 0){
            $id = $_GET['id'];
            
            $filter = " and ms.m_siswa_id = '$id' ";
            $sel = "select nama_siswa,ms.kelas,
            date_format(b.createdate ,'%d %M %Y %H:%i') as tgl,nominal,harus_bayar,approvedby
            ,ms.sisa_bayar ,ms.nis,b.kode_status,b.catatan
            from m_siswa ms 
            left join m_transaksi b on b.m_siswa_id = ms.m_siswa_id 
            and b.kode_status = 'WT2' and jenis_transaksi = 'LY'
            where 1=1  $filter";

            // echo $sel;?
            echo queryJSON($con,$sel);
        }else{
            $sel = "select b.nis,b.nama_siswa,mt.kelas,date_format(mt.createdate ,'%d %M %Y %H:%i') as tgl
            ,nominal,harus_bayar,approvedby 
            ,sisa_bayar as tagihan,b.kelas as kelas_skrg,m_transaksi_id,mt.kode_status,mt.catatan
            from m_transaksi mt 
            inner join m_siswa b on b.m_siswa_id = mt.m_siswa_id 
            where mt.kode_status = 'WT2' and mt.jenis_transaksi = 'LY'
            order by mt.createdate desc";

            // echo $sel;
            echo queryJSON($con,$sel);
        }
    }   
    if(isset($_POST['edit'])){
        $key = $_POST['id'];
        $nominal = $_POST['nominal'];

        $upd = "update m_transaksi set nominal = '$nominal',approvedby = '$_nama' 
        where m_transaksi_id = '$key'";

        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }
    if(isset($_POST['hapus'])){
        $key = $_POST['id'];
        $nominal = $_POST['nominal'];

        $upd = "delete from m_transaksi
        where m_transaksi_id = '$key'";

        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }

?>
