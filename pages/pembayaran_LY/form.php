<div class="container-fluid" id="container-wrapper" style="margin-top:-10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <div class="row" style="margin-top:-35px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
                    <b><h3>Pembayaran Tagihan Tahun sebelumnya</h3></b>
                    <hr>
                    <div class="mb-3">
                        <label for="inputPassword5" require class="form-label">Nama Siswa</label>
                        <br>
                        <select  id="nama" onchange="findData()" class=" form-control kodebrg">
                        <option value=""></option>
                        <?php
                            $del = "select * from m_siswa where isactive = 1";
                            $res = mysqli_query($con,$del);
                            while($rs = mysqli_fetch_array($res))
                            {
                        ?>
                            <option value="<?php echo $rs['m_siswa_id'] ?>"><?php echo $rs['nama_siswa'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">NIS</label>
                        <input readonly class="form-control" id="nis" type="text" value= "">
                    </div>
                    <div class="mb-3">
                        <label for="formFileDisabled" class="form-label">Kelas</label>
                        <input class="form-control" id="kelas" type="text" readonly value= "">
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">Tunggakan</label>
                        <input class="form-control" readonly type="text" id ="bulan" name="bulan">
                    </div>
                    <div class="mb-3">
                        <label for="formFileMultiple" class="form-label">Nominal</label>
                        <input class="form-control" require type="number" id="nominal" name="nominal">
                    </div>
                    <div class="mb-3">
                        <label for="formFileMultiple" class="form-label">Catatan Pembayaran</label>
                        <textarea class="form-control" id="catatan"></textarea>
                    </div>
                    <Button class="btn btn-warning" id="btnkonf" style="margin-top:10px" onclick="konfirmasi(this)">
                        Konfirmasi
                    </Button>
            </div>
            <!-- <div class="row">
                <div class="col col-md-12">
                    <table class="table">
                        <thead>
                            <th>Tanggal Bayar</th>
                            <th>Tahun Ajaran</th>
                            <th>Kelas</th>
                            <th>Biaya</th>
                            <th>Dibayar</th>
                        </thead>
                        <tbody id="transaksiDU">
                        </tbody>
                    </table>
                </div>
            </div> -->
       
    </div>
    </div>
</div>
<script>
    $(".kodebrg").chosen();
    function konfirmasi(v){
        if(!confirm(`Yakin melanjutkan transaksi.. ?`)){
            return
        }
        $(`#${v.id}`).html('Please wait ...')
        var nis = document.getElementById("nama").value;
        var nominal = document.getElementById("nominal").value;
        var kelas = document.getElementById("kelas").value;
        console.log(nis,nominal,kelas);
        $.ajax({
            type:'POST',
            url:'pages/pembayaran_LY/data_be.php',
            data:{
                bayar : 'true',
                id : nis,
                nominal : nominal,
                catatan : $(`#catatan`).val()
            },
            success:function(data){
                console.log(data);
                $(`#${v.id}`).html('Konfirmasi')
                // var obj = Object.keys(data)
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=ly";
                    console.log("yessss....");
                }
            }
        }); 
    }
    function findData(){
        var nis = document.getElementById("nama").value;
        console.log(nis,'xxxxxxxxxxx');
        $.ajax({
            type:'GET',
            url:'pages/pembayaran_LY/data_be.php',
            data:{
              find : true,
              id : nis
            },
            success:function(data){
                // console.log(data);
                var objek = $.parseJSON(data);
                // console.log(objek);

                var obj = objek.data[0]
                var nama  = obj.nama_siswa;
                var kelas = obj.kelas;
                var cost  = obj.sisa_bayar
                var nis   = obj.nis
                document.getElementById("nis").value   = nis
                document.getElementById("kelas").value = kelas
                document.getElementById("bulan").value = duit(cost ? cost : 0)
            }
        }); 
    }

    function duit(v){
        // return v
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        console.log(ribuan);
        
        return ribuan
    }
</script>