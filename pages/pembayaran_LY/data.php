<div class="container-fluid" id="container-wrapper" style="margin-top:10px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Pembayaran Tunggakan Tahun Sebelumnya</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Tunggakan Tahun Sebelumnya</li>
    </ol>
</div>
<!-- <div class="row">
    <div class="co cl">
        
    </div>
</div> -->
<div class="row" style="margin-top:20px">
    <div class="col-lg-12">
        <a href="./?go=inputly"><button class="btn btn-danger" id="btninputly" style="margin-bottom:10px">+ Input Data</button></a>
        <div class="card mb-4">
        <div class="table-responsive p-3">
        <table class="table align-items-center table-flush table-hover" id="tblly" style="margin-top:20px">
            <thead class="thead-light">
            <tr>
                    <th>NIS</th>
                    <th>Nama Siswa</th>
                    <th>Kelass</th>
                    <th>Nominal</th>
                    <th>Tgl Bayar</th>
                    <th>Status</th>
                    <th>Petugas</th>
                    <th>Catatan</th>
                    <th>Action</th>
            </tr>
            </thead>
            <tbody id="dtlly">
            </tbody>
        </table>
    </div>
</div>
<!-- modal -->
    <div class="modal fade" id="modalaksily" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pembayaran PPDB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label for="formFileMultiple" class="form-label">NAMA</label>
                    <input class="form-control" id="nama" type="text" readonly >
                    <input class="form-control" id="keys" type="text" >
                </div>
                <div class="mb-3">
                    <label for="formFileDisabled" class="form-label">TGL BAYAR</label>
                    <input class="form-control" id="tgl" type="text" readonly>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">NOMINAL PPDB</label>
                    <input class="form-control" type="number" id ="nominal" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="hapusLy()">Hapus</button>
                <button type="button" class="btn btn-primary" onclick="editLy()">Edit</button>
            </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', findbayar, false);
    function findbayar(){
        let role = "<?php echo $_SESSION['role']?>"

        if(role !== "Tata Usaha"){
            $(`#btninputly`).css('display','none')
        }

        var table = $('#tblly').DataTable();
        table.clear().destroy(); 
        $.ajax({
            type:'GET',
            url:'pages/pembayaran_LY/data_be.php',
            data:{
                find : true,
                kelas : "",
                id : "",
            },
            success: function(data){
                console.log(data);
                let obj = JSON.parse(data);
                obj = obj.data
                let dom = ``

                let btn = ``
                if(role == "Mgr Tata Usaha"){
                    btn = `<button class="btn btn-danger" onclick="aksi('${obj[i].m_transaksi_id}','${obj[i].nominal}','${obj[i].nama_siswa}','${obj[i].tgl}')">Aksi</button>`
                }
                for(let i = 0; i<obj.length;i++){
                    dom = dom + `<tr>
                        <td>${obj[i].nis}</td>
                        <td>${obj[i].nama_siswa}</td>
                        <td>${obj[i].kelas}</td>
                        <td>${duit(obj[i].nominal)}</td>
                        <td>${obj[i].tgl}</td>
                        <td>${obj[i].kode_status == "WT2" ? 'Approved' : 'Reject'}</td>
                        <td>${obj[i].approvedby}</td>
                        <td>${obj[i].catatan ? obj[i].catatan : ''}</td>
                        <td>${btn}</td>
                    </tr>`
                }
                $(`#dtlly`).html(dom)
                $('#tblly').DataTable(({ 
                        "destroy": true, //use for reinitialize datatable
                }));
            }
        })
    }
    function duit(v){
        console.log(v);
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join(',').split('').reverse().join('');
        
        return `${ribuan}`
    }
    function editLy(){
        console.log('woii');
        $.ajax({
            type:'POST',
            url:`pages/pembayaran_LY/data_be.php`,
            data: {
                edit : true,
                id : $(`#keys`).val(),
                nominal : $(`#nominal`).val()
            },
            success:function(data){
                console.log(data);
                if(data == "200"){
                    alert(`Berhasil`)
                    window.location = "./?go=ly"
                }
            }
        })
    }
    function hapusLy(){
        console.log('woii');
        $.ajax({
            type:'POST',
            url:`pages/pembayaran_LY/data_be.php`,
            data: {
                hapus : true,
                id : $(`#keys`).val(),
                nominal : $(`#nominal`).val()
            },
            success:function(data){
                console.log(data);
                if(data == "200"){
                    alert(`Berhasil`)
                    window.location = "./?go=ly"
                }
            }
        })
    }
    function aksi(key,nominal,nama_siswa,tgl){
        console.log(key,nominal,nama_siswa,tgl);
        $(`#nama`).val(nama_siswa)
        $(`#keys`).val(key)
        $(`#tgl`).val(tgl)
        $(`#nominal`).val(nominal)
        $(`#modalaksily`).modal('show')
    }
</script>