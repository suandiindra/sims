<?php
    include('utility/config.php');
    include("utility/fungsi.php");
    session_start();

    $sel  = "update transaksi_point set kode_status = 'WT2',status = 'On Proses'  
    where cut_off = DATE_FORMAT(now(), '%Y-%m-%d') and kode_status = 'WT1'
    and is_claim = 'N'";
    $data = mysqli_query($con,$sel);
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>RuangAdmin - Login</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">

  <style>
      .loginx{
        position: absolute;
        top:0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 100px;
        height: 100px;
        background-color: blue;
      }
  </style>
</head>
<?php
    if(isset($_POST['id'])){
      $hp     = $_POST['id'];
      $pass   = ($_POST['password']);
      $result = mysqli_query($con,"select a.m_user_id,a.username,phone,email,pwd,b.nama as role from m_user a
      inner join m_role b on a.m_role_id = b.m_role_id where phone = '$hp' and pwd = '$pass'");

      // echo "select a.m_user_id,a.username,phone,email,pwd,b.nama as role from m_user a
      // inner join m_role b on a.m_role_id = b.m_role_id where phone = '$hp' and pwd = '$pass'";

      // exit;
      if( mysqli_num_rows($result) > 0){
          $data = mysqli_fetch_array($result);
          $_SESSION['m_user_id'] = $data["m_user_id"];
          $_SESSION['nama'] = $data["username"];
          $_SESSION['role'] = $data["role"];
          $_SESSION['phone'] = $data["phone"];
          $_SESSION['email'] = $data["email"];
          echo "<script>window.location='./'</script>";
      }else{
          // cek siswa kali 
          $result = mysqli_query($con,"select * from m_siswa a inner join m_kelas b
          on a.m_kelas_id = b.m_kelas_id where a.isactive = 1 and nisn = '$hp' and pwd = '$pass'");

          
          if( mysqli_num_rows($result) > 0){
            $data = mysqli_fetch_array($result);
            // if($hp == $data["nis"]){
                $_SESSION['m_user_id'] = $data["m_siswa_id"];
                $_SESSION['nama'] = $data["nama_siswa"];
                $_SESSION['role'] = "siswa";
                $_SESSION['tempat_lahir'] = $data["tempat_lahir"];
                $_SESSION['nis'] = $data["nis"];
                $_SESSION['tgl_lahir'] = $data["tgl_lahir"];
                $_SESSION['kelas'] = $data["nama_kelas"];
                $_SESSION['alamat'] = $data["alamat"];

                $_SESSION['daftar_ulang'] = $data["daftar_ulang"];
                $_SESSION['ppdb'] = $data["ppdb"];
                $_SESSION['biaya_spp'] = $data["biaya_spp"];
                $_SESSION['m_kelas_id'] = $data["m_kelas_id"];

                // echo "cc";
                // echo "<script>alert('oke Login')</script>";
                echo "<script>window.location='./'</script>";
            // }
          }
          // echo "<script>alert('Gagal Login')</script>";
      }
    }
    

?>
<body class="bg-gradient-login" style="background-image: url('img/bg.jpg')">
  <!-- Login Content -->
  <div class="container-login" class="loginx">
    <div class="row justify-content-center">
      <div class="col-xl-5 col-lg-5 col-md-5">
        <div class="card shadow-sm my-5">
          <div class="card-body p-0">
            <div class="row"> 
                <div class="col-lg-12" >
                    <div class="login-form">
                      <div class="text-text-left col-md-4" style="background-color:; margin-top:10px; margin:auto">
                          <img src="img/logo/pta.jpg" style="width:100px"/>
                      </div>
                    </div>
                    <center style="margin-top:-30px"><p><h3>Selamat Datang</h3></p><br>
                     <p style="margin-top:-30px">SDIT Pelita Alam</p>
                    </center>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12" >
                    <div class="login-form">
                      <form class="user" action="" method="POST" style="margin-top:0px">
                                <div class="form-group">
                                  <input id = "_id" name="id" required class="form-control" id="exampleInputEmail" aria-describedby="emailHelp"
                                    placeholder="Masukan Username">
                                </div>
                                <div class="form-group">
                                  <input type="password" required class="form-control" name="password" placeholder="Masukan Password">
                                </div>

                                <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Login</button>
                              </div>   
                             <center><p style="font-size:13px; color:green">Sistem Informasi Manajemen Sekolah</p></center>
                             <center><a href="./pages/absen/absen.php"><p style="font-size:13px; color:green">Sistem Absen</p></p></center>
                      </form>
                    </div>
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Content -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
</body>
<script>
    function adminform(){
      $("#_id").toggle();
      $("#_nama").toggle(); 
      document.getElementById("_id").required = false;
      document.getElementById("_nama").required = false;
      // var txt = document.getElementById("adm").innerHTML = "Agent PT. Baraka Insan Mandiri"
      // alert(txt)
    }

    window.onload = function() {
        $.ajax({
            typxe:'GET',
            url:'pages/absen/absen_be.php',
            data:{
                scan : true,
                id : "s"
            },
            success: function(data){
                console.log("ooo",data);
            }
        })
    }
</script>
</html>