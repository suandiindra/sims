<?php
  include('utility/config.php');
  include('utility/fungsi.php');
  session_start();
  if(empty($_SESSION['m_user_id'])){
    echo "<script>window.location='login.php'</script>";
  }else{
    $_user = $_SESSION["m_user_id"];
    $_nama = $_SESSION["nama"];
  }

  $log = "select *,DATE_FORMAT(waktu,'%d-%M-%Y  %H:%i') as wt from 
  (
    select *,@rownum := @rownum + 1 AS rank from history_login
    ,(SELECT @rownum := 0) r
    where m_user_id = '$_user'
    ORDER BY waktu desc
  )dd
  where rank = 2";
  // echo $log;
  $result = mysqli_query($con,$log);
  $data = mysqli_fetch_array($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>RuangAdmin - DataTables</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">

  <!-- read excel -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script> -->

  <script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>

  
  <script>
    function startTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();

      var day = today.getDay();
      m = checkTime(m);
      s = checkTime(s);

      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = monthNames[today.getMonth() + 0]; //String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      var todayz = dd + '-' + mm + '-' + yyyy;
      todayz+ " "+ h + ":" + m ;
      var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
      if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
      return i;
    }
    </script>
</head>

<body onload="startTime()" id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html" style="background-color:#39995F">
        <div class="sidebar-brand-icon">
        </div>
        <div  class="sidebar-brand-text mx-3" style="">Yayasan Al Hasyim Sa'adatul Alam</div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item active" style="height:150px; margin-top:10px">
          <a class="nav-link" href="./">
            <img src="img/logo/sekolah.png" style="width:100px; height:100px; margin-left:30px" alt="" />
          </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Features
      </div>
      <li class="nav-item" id="menuSiswa" style="display:none">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable1" aria-expanded="true"
          aria-controls="collapseTable1">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Data Siswa</span>
        </a>
        <div id="collapseTable1" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h4 class="collapse-header">Data Siswa</h6>
            <a class="collapse-item" id="subTunggakan" href="./?go=tunggakan">Data Tunggakan</a>
            <a class="collapse-item" id="subAbsensi" href="./?go=absensi">Data Absensi</a>
            <a class="collapse-item" id="subDataSiswa" href="./?go=editsiswa&act=edit&id=<?php echo $_SESSION['m_user_id'];?>">Data Siswa</a>
          </div>
        </div>
      </li>

      <li class="nav-item" id="menuUser">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable1" aria-expanded="true"
          aria-controls="collapseTable1">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Data User</span>
        </a>
        <div id="collapseTable1" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h4 class="collapse-header">Data User</h6>
            <a class="collapse-item" id="subDataSiswa" href="./?go=listsiswa">Data Siswa</a>
            <a class="collapse-item" id="subDataAdmin" href="./?go=user">Data Admin</a>
          </div>
        </div>
      </li>
      <li class="nav-item" id="menuAbsensi">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable3" aria-expanded="true"
          aria-controls="collapseTable3">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Laporan Kehadiran</span>
        </a>
        <div id="collapseTable3" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h4 class="collapse-header">Laporan</h6>
            <a class="collapse-item" href="./?go=generate">Generate Laporan</a>
            <a class="collapse-item" href="./?go=kehadiran">Kehadiran</a>
          </div>
        </div>
      </li>
      <li class="nav-item" id="menuLaporanTU">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable5" aria-expanded="true"
          aria-controls="collapseTable5">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Laporan TU</span>
        </a>
        <div id="collapseTable5" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h4 class="collapse-header">Laporan</h6>
            <a class="collapse-item" href="./?go=rekapitulasi">Rekapitulasi</a>
          </div>
        </div>
      </li>
      <li class="nav-item" id="menuPembayaran">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable4" aria-expanded="true"
          aria-controls="collapseTable4">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Pembayaran</span>
        </a>
        <div id="collapseTable4" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h4 class="collapse-header">pembayaran</h6>
            <a class="collapse-item" href="./?go=formspp">SPP Bulanan</a>
            <a class="collapse-item" href="./?go=daftarulang">Bayar Daftar Ulang</a>
            <a class="collapse-item" href="./?go=listppdb">Bayar PPDB</a>
            <a class="collapse-item" href="./?go=ly">Pemb Sisa Sebelumnya</a>
          </div>
        </div>
      </li>
      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link" href="./logout.php">
        <i class="fa fa-fw fa-times"></i>
          <span>Logout</span>
        </a>
      </li>
      <div class="version" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top" style="background-color:#39995F">
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="img/boy.png" style="max-width: 60px">
                <span class="ml-2 d-none d-lg-inline text-white small"><?php echo $_SESSION['nama']; ?></span>
              </a>
            </li>
          </ul>
        </nav>
        <?php
        include('route.php')
        ?>
        <!---Container Fluid-->
      </div>

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script>
            </span>
          </div>
        </div>
      </footer>
      <!-- Footer -->
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <!-- js function -->
  <!-- <script src="js/utility/callAPI.js"></script> -->
  <!-- <script src="js/apexchart/summary.js"></script> -->




  <!-- Page level custom scripts -->
  <script>
    let role_ = "<?php echo isset($_GET['role']) ? $_GET['role'] : ""; ?>";
    window.addEventListener('load', loads, false);

    function loads(){
      refreshMenu()
      let role = "<?php echo $_SESSION['role']; ?>"

      if(role == "Tata Usaha"){
        $(`#menuPembayaran`).css('display','block')
        $(`#menuLaporanTU`).css('display','block')
      }else if(role == "siswa"){
        $(`#menuSiswa`).css('display','block')
      }else if(role == "Kesiswaan"){
        $(`#menuUser`).css('display','block')
        $(`#SubDatasiswa`).css('display','block')
        $(`#menuAbsensi`).css('display','block')
      }else if(role == "Administrator" || role == "Admin"){
        $(`#menuUser`).css('display','block')
        $(`#menuPembayaran`).css('display','block')
        $(`#menuAbsensi`).css('display','block')
        $(`#SubDatasiswa`).css('display','block')
        $(`#subDataAdmin`).css('display','block')
        $(`#subLY`).css('display','block')
        $(`#menuLaporanTU`).css('display','block')  
      }else if(role == "Mgr Tata Usaha"){
        $(`#menuLaporanTU`).css('display','block')
        $(`#SubDatasiswa`).css('display','block')
        $(`#menuUser`).css('display','block')
        $(`#menuPembayaran`).css('display','block')
      }
      console.log(role);
    }
    function refreshMenu(){
      $(`#menuUser`).css('display','none')
      $(`#menuPembayaran`).css('display','none')
      $(`#menuAbsensi`).css('display','none')
      $(`#SubDatasiswa`).css('display','none')
      $(`#subDataAdmin`).css('display','none')
      $(`#menuLaporanTU`).css('display','none')
      $(`#subLY`).css('display','none')

    }
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
      $('#dataTableHover2').DataTable(); // ID From dataTable with Hover
    });
  </script>
</body>

</html>